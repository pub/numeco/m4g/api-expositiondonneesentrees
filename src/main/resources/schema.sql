CREATE TABLE IF NOT EXISTS en_donnees_entrees
(
    id                        int8         NOT NULL,
    date_creation             timestamp    NULL,
    date_lot                  date         NULL,
    nom_organisation          varchar(255) NULL,
    nom_lot                   varchar(255) NULL,
    nbr_applications          int8         NULL,
    nbr_data_center           int8         NULL,
    nbr_equipements_physiques int8         NULL,
    nbr_equipements_virtuels  int8         NULL,
    nbr_messageries           int8         NULL,
    CONSTRAINT en_donnees_entrees_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_data_center
(
    id                   int8         NOT NULL,
    date_creation        timestamp    NULL,
    date_lot             date         NULL,
    nom_lot              varchar(255) NULL,
    nom_organisation     varchar(255) NULL,
    nom_source_donnee    varchar(255) NULL,
    localisation         varchar(255) NULL,
    nom_court_datacenter varchar(255) NULL,
    nom_entite           varchar(255) NULL,
    nom_long_datacenter  varchar(255) NULL,
    pue                  float8       NULL,
    CONSTRAINT en_data_center_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_equipement_physique
(
    id                        int8         NOT NULL,
    date_creation             timestamp    NULL,
    date_lot                  date         NULL,
    nom_lot                   varchar(255) NULL,
    nom_organisation          varchar(255) NULL,
    nom_source_donnee         varchar(255) NULL,
    conso_elec_annuelle       float8       NULL,
    date_achat                date         NULL,
    date_retrait              date         NULL,
    duree_vie_defaut          float8       NULL,
    go_telecharge             float4       NULL,
    modele                    varchar(255) NULL,
    nb_coeur                  varchar(255) NULL,
    nb_jour_utilise_an        float8       NULL,
    nom_court_datacenter      varchar(255) NULL,
    nom_entite                varchar(255) NULL,
    nom_equipement_physique   varchar(255) NULL,
    pays_utilisation          varchar(255) NULL,
    quantite                  float8       NULL,
    serveur                   bool         NOT NULL,
    statut                    varchar(255) NULL,
    "type"                    varchar(255) NULL,
    utilisateur               varchar(255) NULL,
    ref_equipement_par_defaut varchar(255) NULL,
    ref_equipement_retenu     varchar(255) NULL,
    nb_equipements_virtuels     varchar(255) NULL,
    nb_total_vcpu     varchar(255) NULL,
    CONSTRAINT en_equipement_physique_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_equipement_virtuel
(
    id                      int8         NOT NULL,
    date_creation           timestamp    NULL,
    date_lot                date         NULL,
    nom_lot                 varchar(255) NULL,
    nom_organisation        varchar(255) NULL,
    nom_source_donnee       varchar(255) NULL,
    "cluster"               varchar(255) NULL,
    nom_entite              varchar(255) NULL,
    nom_equipement_physique varchar(255) NULL,
    nom_vm                  varchar(255) NULL,
    vcpu                    int4         NULL,
    nb_applications         varchar(255) NULL,
    CONSTRAINT en_equipement_virtuel_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_application
(
    id                      int8         NOT NULL,
    date_creation           timestamp    NULL,
    date_lot                date         NULL,
    nom_lot                 varchar(255) NULL,
    nom_organisation        varchar(255) NULL,
    nom_source_donnee       varchar(255) NULL,
    domaine                 varchar(255) NULL,
    nom_application         varchar(255) NULL,
    nom_entite              varchar(255) NULL,
    nom_vm                  varchar(255) NULL,
    nom_equipement_physique varchar(255) NULL,
    sous_domaine            varchar(255) NULL,
    type_environnement      varchar(255) NULL,
    CONSTRAINT en_application_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_messagerie
(
    id                             int8         NOT NULL,
    date_creation                  timestamp    NULL,
    date_lot                       date         NULL,
    nom_lot                        varchar(255) NULL,
    nom_organisation               varchar(255) NULL,
    nom_source_donnee              varchar(255) NULL,
    mois_annee                     int4         NULL,
    nom_entite                     varchar(255) NULL,
    nombre_mail_emis               int4         NULL,
    nombre_mail_emisxdestinataires int4         NULL,
    volume_total_mail_emis         int4         NULL,
    CONSTRAINT en_messagerie_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_entite
(
    id                                  int8         NOT NULL,
    date_creation                       timestamp    NULL,
    date_update                         timestamp    NULL,
    date_lot                            date         NULL,
    nom_lot                             varchar(255) NULL,
    nom_organisation                    varchar(255) NULL,
    nom_source_donnee                   varchar(255) NULL,
    nom_entite                          varchar(255) NULL,
    nb_collaborateurs                   int4         NULL,
    responsable_entite                  varchar(255) NULL,
    responsable_numerique_durable       varchar(255) NULL,
    CONSTRAINT en_entite_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE IF NOT EXISTS seq_en_entite INCREMENT BY 1000;

ALTER TABLE IF EXISTS en_donnees_entrees ADD COLUMN IF NOT EXISTS statut_traitement varchar(255);
ALTER TABLE IF EXISTS en_data_center ADD COLUMN IF NOT EXISTS statut_traitement varchar(255);
ALTER TABLE IF EXISTS en_equipement_physique ADD COLUMN IF NOT EXISTS statut_traitement varchar(255);
ALTER TABLE IF EXISTS en_equipement_virtuel ADD COLUMN IF NOT EXISTS statut_traitement varchar(255);
ALTER TABLE IF EXISTS en_application ADD COLUMN IF NOT EXISTS statut_traitement varchar(255);
ALTER TABLE IF EXISTS en_messagerie ADD COLUMN IF NOT EXISTS statut_traitement varchar(255);

ALTER TABLE IF EXISTS en_donnees_entrees ADD COLUMN IF NOT EXISTS date_update timestamp;
ALTER TABLE IF EXISTS en_data_center ADD COLUMN IF NOT EXISTS date_update timestamp;
ALTER TABLE IF EXISTS en_equipement_physique ADD COLUMN IF NOT EXISTS date_update timestamp;
ALTER TABLE IF EXISTS en_equipement_virtuel ADD COLUMN IF NOT EXISTS date_update timestamp;
ALTER TABLE IF EXISTS en_application ADD COLUMN IF NOT EXISTS date_update timestamp;
ALTER TABLE IF EXISTS en_messagerie ADD COLUMN IF NOT EXISTS date_update timestamp;

-- Taiga#1014 - Gestion du nom de lot
ALTER TABLE IF EXISTS en_data_center
    ADD COLUMN IF NOT EXISTS nom_lot varchar(255);
ALTER TABLE IF EXISTS en_equipement_physique
    ADD COLUMN IF NOT EXISTS nom_lot varchar(255);
ALTER TABLE IF EXISTS en_equipement_virtuel
    ADD COLUMN IF NOT EXISTS nom_lot varchar(255);
ALTER TABLE IF EXISTS en_application
    ADD COLUMN IF NOT EXISTS nom_lot varchar(255);
ALTER TABLE IF EXISTS en_messagerie
    ADD COLUMN IF NOT EXISTS nom_lot varchar(255);
ALTER TABLE IF EXISTS en_donnees_entrees
    ADD COLUMN IF NOT EXISTS nom_lot varchar(255);

-- Taiga#937 - Gestion des sources de données
ALTER TABLE IF EXISTS en_data_center
    ADD COLUMN IF NOT EXISTS nom_source_donnee varchar(255);
ALTER TABLE IF EXISTS en_equipement_physique
    ADD COLUMN IF NOT EXISTS nom_source_donnee varchar(255);
ALTER TABLE IF EXISTS en_equipement_virtuel
    ADD COLUMN IF NOT EXISTS nom_source_donnee varchar(255);
ALTER TABLE IF EXISTS en_application
    ADD COLUMN IF NOT EXISTS nom_source_donnee varchar(255);
ALTER TABLE IF EXISTS en_messagerie
    ADD COLUMN IF NOT EXISTS nom_source_donnee varchar(255);

-- Taiga#937 - Impacts - La source de l'équipement virtuel peut être différente de celle de l'équipement physique
ALTER TABLE IF EXISTS en_equipement_virtuel
    ADD COLUMN IF NOT EXISTS nom_source_donnee_equipement_physique varchar(255);

-- Taiga#937 - Impacts - La source de l'instance d'application peut être différente de celle de l'équipement virtuel
ALTER TABLE IF EXISTS en_application
    ADD COLUMN IF NOT EXISTS nom_source_donnee_equipement_virtuel varchar(255);

-- Taiga#457 - Impacts - Ajout des colonnes conso_elec_annuelle, type_eqv, capacite_stockage et cle_repartition
ALTER TABLE IF EXISTS en_equipement_virtuel ADD COLUMN IF NOT EXISTS conso_elec_annuelle float8;
ALTER TABLE IF EXISTS en_equipement_virtuel ADD COLUMN IF NOT EXISTS type_eqv varchar(255);
ALTER TABLE IF EXISTS en_equipement_virtuel ADD COLUMN IF NOT EXISTS capacite_stockage float8;
ALTER TABLE IF EXISTS en_equipement_virtuel ADD COLUMN IF NOT EXISTS cle_repartition float8;
-- Taiga#457 - Impacts - Ajout de la colonne pour la capacité totale de stockage
ALTER TABLE IF EXISTS en_equipement_physique ADD COLUMN IF NOT EXISTS stockage_total_virtuel float8;

CREATE OR REPLACE FUNCTION rename_comlumn_if_not_needed (t_name text, old_name text, new_name text)
    RETURNS void AS
'
    BEGIN
        IF EXISTS (SELECT column_name
                       FROM information_schema.columns
                       WHERE column_name  = old_name) THEN
            EXECUTE ''ALTER TABLE IF EXISTS '' || t_name || '' RENAME COLUMN '' || old_name || '' TO '' || new_name;
        END IF;
    END;
' LANGUAGE plpgsql;;

-- Taiga#1054 Renommage de nomVM en nomEquipementVirtuel
SELECT rename_comlumn_if_not_needed('en_equipement_virtuel', 'nom_vm', 'nom_equipement_virtuel');
SELECT rename_comlumn_if_not_needed('en_application', 'nom_vm', 'nom_equipement_virtuel');

-- Suppression des fonctions de mise à jour du schéma
DROP FUNCTION IF EXISTS rename_comlumn_if_not_needed;