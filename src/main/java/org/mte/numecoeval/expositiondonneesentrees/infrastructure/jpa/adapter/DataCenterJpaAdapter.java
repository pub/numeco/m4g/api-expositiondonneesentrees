package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DataCenter;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.EntreePersistencePort;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.DataCenterRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class DataCenterJpaAdapter implements EntreePersistencePort<DataCenter> {

    DataCenterRepository repository;

    EntreeEntityMapper entreeEntityMapper;

    @Override
    public void save(DataCenter entree) {
        repository.save(entreeEntityMapper.toEntity(entree));
    }

    @Override
    public void saveAll(List<DataCenter> entrees) {
        repository.saveAll(entreeEntityMapper.toEntityListDataCenter(entrees));
    }
}
