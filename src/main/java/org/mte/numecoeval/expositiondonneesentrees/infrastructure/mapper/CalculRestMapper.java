package org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DemandeCalcul;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.RapportDemandeCalcul;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.DemandeCalculRest;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.RapportDemandeCalculRest;

@Mapper(componentModel = "spring")
public interface CalculRestMapper {

    DemandeCalcul toDomain(DemandeCalculRest restDTO);

    RapportDemandeCalculRest toRest(RapportDemandeCalcul domain);
}
