package org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Application;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DataCenter;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DonneesEntree;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Entite;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementPhysique;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementVirtuel;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Messagerie;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.ApplicationEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.DataCenterEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.DonneesEntreesEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.EntiteEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.EquipementPhysiqueEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.EquipementVirtuelEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.MessagerieEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EntreeEntityMapper {

    DonneesEntreesEntity toEntity(DonneesEntree domain);
    DataCenterEntity toEntity(DataCenter domain);
    EquipementPhysiqueEntity toEntity(EquipementPhysique domain);
    @Mapping(target = "vCPU", source = "VCPU")
    EquipementVirtuelEntity toEntity(EquipementVirtuel domain);
    ApplicationEntity toEntity(Application domain);
    MessagerieEntity toEntity(Messagerie domain);
    EntiteEntity toEntity(Entite domain);
    List<DataCenterEntity> toEntityListDataCenter(List<DataCenter> domains);
    List<EquipementPhysiqueEntity> toEntityListEquipementPhysique(List<EquipementPhysique> domains);
    List<EquipementVirtuelEntity> toEntityListEquipementVirtuel(List<EquipementVirtuel> domains);
    List<ApplicationEntity> toEntityListApplication(List<Application> domains);
    List<MessagerieEntity> toEntityListMessagerie(List<Messagerie> domains);
    List<EntiteEntity> toEntityListEntite(List<Entite> domains);

    DonneesEntree toDomain(DonneesEntreesEntity entity);
    DataCenter toDomain(DataCenterEntity entity);
    EquipementPhysique toDomain(EquipementPhysiqueEntity entity);
    EquipementVirtuel toDomain(EquipementVirtuelEntity entity);
    Application toDomain(ApplicationEntity entity);
    Messagerie toDomain(MessagerieEntity entity);
    Entite toDomain(Entite entity);
    List<DataCenter> toDomainListDataCenter(List<DataCenterEntity> entities);
    List<EquipementPhysique> toDomainListEquipementPhysique(List<EquipementPhysiqueEntity> entities);
    List<EquipementVirtuel> toDomainListEquipementVirtuel(List<EquipementVirtuelEntity> entities);
    List<Application> toDomainListApplication(List<ApplicationEntity> entities);
    List<Messagerie> toDomainListMessagerie(List<MessagerieEntity> entities);
    List<Entite> toDomainListEntite(List<EntiteEntity> entities);
}
