package org.mte.numecoeval.expositiondonneesentrees.infrastructure.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.input.SoumissionCalculPort;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.DemandeCalculRest;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.RapportDemandeCalculRest;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.server.CalculsApi;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.CalculRestMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@Slf4j
public class CalculController implements CalculsApi {

    CalculRestMapper calculRestMapper;

    SoumissionCalculPort soumissionCalculPort;

    @Override
    public ResponseEntity<RapportDemandeCalculRest> soumissionPourCalcul(DemandeCalculRest demandeCalculRest) {
        log.info("Soumission de calcul {}", demandeCalculRest);
        var demandeCalcul = calculRestMapper.toDomain(demandeCalculRest);

        var soumission = soumissionCalculPort.soumissionCalcul(demandeCalcul);

        var responseBody = calculRestMapper.toRest(soumission);

        return ResponseEntity.ok(responseBody);
    }

    @Override
    public ResponseEntity<RapportDemandeCalculRest> rejeuCalcul(DemandeCalculRest demandeCalculRest) {
        log.info("Rejeu de calcul {}", demandeCalculRest);
        var demandeCalcul = calculRestMapper.toDomain(demandeCalculRest);

        var soumission = soumissionCalculPort.rejeuCalcul(demandeCalcul);

        var responseBody = calculRestMapper.toRest(soumission);

        return ResponseEntity.ok(responseBody);
    }
}
