package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository;

import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.EquipementPhysiqueEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EquipementPhysiqueRepository extends JpaRepository<EquipementPhysiqueEntity,Long> {
}
