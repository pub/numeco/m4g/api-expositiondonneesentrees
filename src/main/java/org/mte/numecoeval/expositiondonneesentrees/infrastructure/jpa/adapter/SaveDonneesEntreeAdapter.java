package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Application;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DataCenter;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DonneesEntree;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Entite;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementPhysique;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementVirtuel;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Messagerie;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.EntreePersistencePort;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.SaveDonneesEntreePort;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

@Slf4j
@AllArgsConstructor
@Service
public class SaveDonneesEntreeAdapter implements SaveDonneesEntreePort {

    private EntreePersistencePort<DonneesEntree> donneesEntreesEntreePersistencePort;
    private EntreePersistencePort<DataCenter> dataCenterEntreePersistencePort;
    private EntreePersistencePort<EquipementPhysique> equipementPhysiqueEntreePersistencePort;
    private EntreePersistencePort<EquipementVirtuel> equipementVirtuelEntreePersistencePort;
    private EntreePersistencePort<Application> applicationEntreePersistencePort;
    private EntreePersistencePort<Messagerie> messagerieEntreePersistencePort;
    private EntreePersistencePort<Entite> entiteEntreePersistencePort;



    @Override
    @CacheEvict(cacheNames="correspondanceRefEquipement", allEntries=true)
    public void save(DonneesEntree donneesEntree) {

        if(donneesEntree == null) {
            log.warn("Données null reçue");
            return;
        }

        log.info("Données reçues  : Nom Lot : {}, Date de lot : {} - Nom Organisation : {}",
                donneesEntree.getNomLot(), donneesEntree.getDateLot(), donneesEntree.getNomOrganisation()
        );
        var listEquipementsVirtuels = new ArrayList<EquipementVirtuel>();
        var listEquipementsApplications = new ArrayList<Application>();

        // Complétions des listes globales
        // Les objets sous-jacents (équipements virtuels et applications) n'arrivent que depuis les équipements physiques.
        // Ces boucles permettent de simplement conserver une seule liste unique d'objets en plus de ceux portés par DonneesEntreeDTO.
        CollectionUtils.emptyIfNull(donneesEntree.getEquipementsPhysiques()).forEach(equipementPhysique ->
                listEquipementsVirtuels.addAll(equipementPhysique.getEquipementsVirtuels())
        );
        listEquipementsVirtuels.forEach(equipementVirtuel ->
                listEquipementsApplications.addAll(equipementVirtuel.getApplications())
        );

        StopWatch globalStopWatch = StopWatch.createStarted();

        StopWatch stopWatch = StopWatch.createStarted();
        donneesEntreesEntreePersistencePort.save(
                donneesEntree
        );
        stopWatch.stop();
        log.info("Fin du traitement de l'objet DonneesEntree reçue en {} secondes",
                stopWatch.getTime(TimeUnit.SECONDS)
        );

        stopWatch = StopWatch.createStarted();
        dataCenterEntreePersistencePort.saveAll(
                ListUtils.emptyIfNull(donneesEntree.getDataCenters())
        );
        stopWatch.stop();
        log.info("Fin du traitement des {} objets DataCenter reçus en {} secondes",
                CollectionUtils.size(donneesEntree.getDataCenters()),
                stopWatch.getTime(TimeUnit.SECONDS)
        );

        stopWatch = StopWatch.createStarted();
        equipementPhysiqueEntreePersistencePort.saveAll(
                ListUtils.emptyIfNull(donneesEntree.getEquipementsPhysiques())
        );
        stopWatch.stop();
        log.info("Fin du traitement des {} objets EquipementPhysique reçus en {} secondes",
                CollectionUtils.size(donneesEntree.getEquipementsPhysiques()),
                stopWatch.getTime(TimeUnit.SECONDS)
        );

        stopWatch = StopWatch.createStarted();
        equipementVirtuelEntreePersistencePort.saveAll(
                ListUtils.emptyIfNull(listEquipementsVirtuels)
        );
        stopWatch.stop();
        log.info("Fin du traitement des {} objets EquipementVirtuel reçus en {} secondes",
                listEquipementsVirtuels.size(),
                stopWatch.getTime(TimeUnit.SECONDS)
        );

        stopWatch = StopWatch.createStarted();
        applicationEntreePersistencePort.saveAll(
                ListUtils.emptyIfNull(listEquipementsApplications)
        );
        stopWatch.stop();
        log.info("Fin du traitement des {} objets Applications reçus en {} secondes",
                listEquipementsApplications.size(),
                stopWatch.getTime(TimeUnit.SECONDS)
        );

        stopWatch = StopWatch.createStarted();
        messagerieEntreePersistencePort.saveAll(
                ListUtils.emptyIfNull(donneesEntree.getMessageries())
        );
        stopWatch.stop();
        log.info("Fin du traitement des {} objets Messagerie reçus en {} secondes",
                CollectionUtils.size(donneesEntree.getMessageries()),
                stopWatch.getTime(TimeUnit.SECONDS)
        );

        stopWatch = StopWatch.createStarted();
        entiteEntreePersistencePort.saveAll(
                ListUtils.emptyIfNull(donneesEntree.getEntites())
        );
        stopWatch.stop();
        log.info("Fin du traitement des {} objets Entites reçus en {} secondes",
                CollectionUtils.size(donneesEntree.getEntites()),
                stopWatch.getTime(TimeUnit.SECONDS)
        );

        globalStopWatch.stop();
        log.info("Fin du traitement des données reçues : Nom de Lot : {} - Date de lot : {} - Nom Organisation : {}, en {} secondes",
                donneesEntree.getNomLot(), donneesEntree.getDateLot(), donneesEntree.getNomOrganisation(),
                globalStopWatch.getTime(TimeUnit.SECONDS)
        );


    }
}
