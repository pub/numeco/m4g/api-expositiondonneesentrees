package org.mte.numecoeval.expositiondonneesentrees.infrastructure.config;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.input.ImportDonneesEntreePort;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.input.impl.ImportDonneesEntreePortImpl;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.ReferentielCorrespondanceRefEquipementServicePort;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.ReferentielTypeEquipementServicePort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
@ComponentScan(basePackages ="org.mte.numecoeval.expositiondonneesentrees.infrastructure.adapters")
public class ApplicationPortConfig {

    private ReferentielTypeEquipementServicePort referentielTypeEquipementServicePort;

    private ReferentielCorrespondanceRefEquipementServicePort referentielCorrespondanceRefEquipementServicePort;

    @Bean
    public ImportDonneesEntreePort importDonneesEntreePort(){
        return new ImportDonneesEntreePortImpl(referentielTypeEquipementServicePort, referentielCorrespondanceRefEquipementServicePort);
    }
}
