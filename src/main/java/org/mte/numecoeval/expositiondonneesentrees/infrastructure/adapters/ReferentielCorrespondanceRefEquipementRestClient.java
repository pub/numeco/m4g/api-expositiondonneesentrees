package org.mte.numecoeval.expositiondonneesentrees.infrastructure.adapters;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.expositiondonneesentrees.domain.exception.ReferentielRuntimeException;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.ReferentielCorrespondanceRefEquipement;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.ReferentielCorrespondanceRefEquipementServicePort;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.handler.RestClientResponseErrorHandler;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.CorrespondanceRefEquipementMapper;
import org.mte.numecoeval.referentiel.api.dto.CorrespondanceRefEquipementDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
@Slf4j
public class ReferentielCorrespondanceRefEquipementRestClient implements ReferentielCorrespondanceRefEquipementServicePort {


    @Value("${numecoeval.referentiel.server.url}")
    private  String referentielServeurUrl;

    @Value("${numecoeval.referentiel.endpoint.correspondancesRefEquipement}")
    private String referentielRequestUri;

    private final RestTemplate restTemplate;
    private final CorrespondanceRefEquipementMapper correspondanceRefEquipementMapper;

    public ReferentielCorrespondanceRefEquipementRestClient(RestTemplateBuilder restTemplateBuilder, CorrespondanceRefEquipementMapper correspondanceRefEquipementMapper) {
        restTemplate = restTemplateBuilder
                .errorHandler(new RestClientResponseErrorHandler(
                        "ErrCalcFonc",
                        "Aucune Correspondance d'équipement disponible"
                ))
                .build();
        this.correspondanceRefEquipementMapper = correspondanceRefEquipementMapper;
    }

    @Cacheable(value = "correspondanceRefEquipement")
    @Override
    public ReferentielCorrespondanceRefEquipement getCorrespondance(String modele) {
        try {
            CorrespondanceRefEquipementDTO dto= restTemplate
                    .getForObject(
                            referentielServeurUrl + UriComponentsBuilder.fromUriString(referentielRequestUri)
                                    .queryParam("modele", modele)
                                    .build().toUriString(),
                            CorrespondanceRefEquipementDTO.class);

            return correspondanceRefEquipementMapper.toDomain(dto);
        } catch (ReferentielRuntimeException e) {
            log.error("Une erreur est survenue lors de l'appel à l'API Réferentiel : URL : {}, Message : {}", referentielServeurUrl + referentielRequestUri, e.getMessage());
        } catch (Exception e) {
            log.error("Une erreur générale est survenue lors de l'appel à l'API Réferentiel : URL : {}, Message : {}", referentielServeurUrl + referentielRequestUri, e.getMessage());
        }
        return null;
    }

}
