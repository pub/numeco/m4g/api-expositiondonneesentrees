package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementPhysique;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.EntreePersistencePort;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.EquipementPhysiqueRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class EquipementPhysiqueJpaAdapter implements EntreePersistencePort<EquipementPhysique> {

    EquipementPhysiqueRepository repository;

    EntreeEntityMapper entreeEntityMapper;

    @Override
    public void save(EquipementPhysique entree) {
        repository.save(entreeEntityMapper.toEntity(entree));
    }

    @Override
    public void saveAll(List<EquipementPhysique> entrees) {
        repository.saveAll(entreeEntityMapper.toEntityListEquipementPhysique(entrees) );
    }
}
