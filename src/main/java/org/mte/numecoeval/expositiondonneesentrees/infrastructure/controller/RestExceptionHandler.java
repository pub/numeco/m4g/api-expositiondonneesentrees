package org.mte.numecoeval.expositiondonneesentrees.infrastructure.controller;

import org.mte.numecoeval.expositiondonneesentrees.domain.exception.ValidationException;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.ErreurRest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.OffsetDateTime;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(value = { ValidationException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErreurRest> handleValidationException(ValidationException ex, WebRequest request) {
        LOG.error("Exception de validation survenue lors de la requête {}, Exception : {}", request.getContextPath(), ex.getErreur());
        return new ResponseEntity<>(
                ErreurRest.builder()
                        .code("400")
                        .message(ex.getErreur())
                        .timestamp(OffsetDateTime.now())
                        .status(400)
                        .build(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {RuntimeException.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErreurRest> runtimeException(Exception ex, WebRequest request) {
        LOG.error("RuntimeException lors d'un traitement sur l'URI {} : {}", request.getContextPath(), ex.getMessage());
        LOG.debug("RuntimeException lors d'un traitement sur l'URI {}", request.getContextPath(), ex);
        return new ResponseEntity<>(
                ErreurRest.builder()
                        .code("500")
                        .message("Erreur interne de traitement lors du traitement de la requête")
                        .timestamp(OffsetDateTime.now())
                        .status(500)
                        .build(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErreurRest> exception(Exception ex, WebRequest request) {
        LOG.error("Exception lors d'un traitement sur l'URI {} : {}", request.getContextPath(), ex.getMessage());
        LOG.debug("Exception lors d'un traitement sur l'URI {}", request.getContextPath(), ex);
        return new ResponseEntity<>(
                ErreurRest.builder()
                        .code("500")
                        .message("Erreur interne de traitement lors du traitement de la requête")
                        .timestamp(OffsetDateTime.now())
                        .status(500)
                        .build(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
