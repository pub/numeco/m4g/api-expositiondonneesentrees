package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Messagerie;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.EntreePersistencePort;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.MessagerieRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MessagerieJpaAdapter implements EntreePersistencePort<Messagerie> {

    MessagerieRepository repository;

    EntreeEntityMapper entreeEntityMapper;

    @Override
    public void save(Messagerie entree) {
        repository.save(entreeEntityMapper.toEntity(entree));
    }

    @Override
    public void saveAll(List<Messagerie> entrees) {
        repository.saveAll(entreeEntityMapper.toEntityListMessagerie(entrees));
    }
}
