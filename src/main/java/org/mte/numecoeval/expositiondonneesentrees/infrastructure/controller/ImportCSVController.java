package org.mte.numecoeval.expositiondonneesentrees.infrastructure.controller;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.mte.numecoeval.expositiondonneesentrees.domain.exception.ValidationException;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.input.ImportDonneesEntreePort;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.SaveDonneesEntreePort;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.DonneesEntreeRest;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.RapportImportRest;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.server.ImportsApi;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.helper.CSVHelper;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.DonneesEntreeRestMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;

@RestController
@AllArgsConstructor
@SuppressWarnings("java:S107") // Obligatoire à cause de la gestion des fichiers différents au niveau contrat d'interface
public class ImportCSVController implements ImportsApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImportCSVController.class);

    DonneesEntreeRestMapper donneesEntreeMapper;

    ImportDonneesEntreePort importDonneesEntreePort;

    SaveDonneesEntreePort saveDonneesEntreePort;

    @Override
    public ResponseEntity<List<RapportImportRest>> importCSV(String nomLot, MultipartFile csvDataCenter, MultipartFile csvEquipementPhysique, MultipartFile csvEquipementVirtuel, MultipartFile csvApplication, MultipartFile csvMessagerie, MultipartFile csvEntite, LocalDate dateLot, String nomOrganisation) {
        return importInterneCSV(nomLot, dateLot, nomOrganisation, csvDataCenter, csvEquipementPhysique, csvEquipementVirtuel, csvApplication, csvMessagerie, csvEntite);
    }

    public ResponseEntity<List<RapportImportRest>> importInterneCSV(String nomLot, LocalDate dateLot, String nomOrganisation, MultipartFile csvDataCenter, MultipartFile csvEquipementPhysique, MultipartFile csvEquipementVirtuel, MultipartFile csvApplication, MultipartFile csvMessagerie, MultipartFile csvEntite) {
        return ResponseEntity.ok(
                importDonneesFromCSV(
                        nomLot, dateLot, nomOrganisation,
                        csvDataCenter,
                        csvEquipementPhysique,
                        csvEquipementVirtuel,
                        csvApplication,
                        csvMessagerie,
                        csvEntite
                )
        );
    }

    @Override
    public ResponseEntity<List<RapportImportRest>> importJson(DonneesEntreeRest donneesEntreeRest) {
        throw new ResponseStatusException(
                HttpStatus.NOT_IMPLEMENTED,
                "Cette méthode d'import n'a pas encore été implémenté."
        );
    }

    public List<RapportImportRest> importDonneesFromCSV(
            String nomLot, LocalDate dateLot, String nomOrganisation,
            MultipartFile csvDataCenter,
            MultipartFile csvEquipementPhysique,
            MultipartFile csvEquipementVirtuel,
            MultipartFile csvApplication,
            MultipartFile csvMessagerie,
            MultipartFile csvEntite) throws ValidationException {
        LOGGER.info("Reception de fichiers pour imports de données d'entrées : Nom de Lot : {}, Date de lot : {}, Nom Organisation : {}", nomLot, dateLot, nomOrganisation);

        validateRequestParametersForImportCSV(nomLot, csvDataCenter, csvEquipementPhysique, csvEquipementVirtuel, csvApplication, csvMessagerie, csvEntite);

        return importAllCsv(csvDataCenter, csvEquipementPhysique, csvEquipementVirtuel, csvApplication, csvMessagerie, csvEntite, dateLot, nomOrganisation, nomLot);
    }

    /**
     * Validation des entrées lors d'un import d'un ou plusieurs CSV.
     *
     * @param csvDataCenter         Fichier CSV des data centers
     * @param csvEquipementPhysique Fichier CSV des équipements physiques
     * @param csvEquipementVirtuel  Fichier CSV des équipements virtuels
     * @param csvApplication        Fichier CSV des applications
     * @param csvMessagerie         Fichier CSV de la messagerie
     * @param csvEntite             Fichier CSV des entités
     * @throws ResponseStatusException avec le statut 400 lorsque les entrées ne sont pas cohérentes.
     */
    private static void validateRequestParametersForImportCSV(String nomLot, MultipartFile csvDataCenter, MultipartFile csvEquipementPhysique, MultipartFile csvEquipementVirtuel, MultipartFile csvApplication, MultipartFile csvMessagerie, MultipartFile csvEntite) {
        if(CSVHelper.fileIsNullOrEmpty(csvDataCenter)
                && CSVHelper.fileIsNullOrEmpty(csvEquipementPhysique)
                && CSVHelper.fileIsNullOrEmpty(csvEquipementVirtuel)
                && CSVHelper.fileIsNullOrEmpty(csvApplication)
                && CSVHelper.fileIsNullOrEmpty(csvMessagerie)
                && CSVHelper.fileIsNullOrEmpty(csvEntite)
        ) {
            LOGGER.error("Import: Erreur contrôle : Tous les fichiers ne peuvent être vides en même temps");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Tous les fichiers ne peuvent être vides en même temps");
        }
        if(csvEquipementVirtuel != null && !csvEquipementVirtuel.isEmpty()
                && CSVHelper.fileIsNullOrEmpty(csvEquipementPhysique)
        ) {
            LOGGER.error("Import: Erreur contrôle : Le fichier des équipements virtuels ne peut pas être intégré sans les équipements physiques");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Le fichier des équipements virtuels ne peut pas être intégré sans les équipements physiques");
        }
        if(csvApplication != null && !csvApplication.isEmpty()
                && CSVHelper.fileIsNullOrEmpty(csvEquipementVirtuel)
                && CSVHelper.fileIsNullOrEmpty(csvEquipementPhysique)
        ) {
            LOGGER.error("Import: Erreur contrôle : Le fichier des applications ne peut pas être intégré sans les équipements virtuels et les équipements physiques");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Le fichier des applications ne peut pas être intégré sans les équipements virtuels et les équipements physiques");
        }
        if(StringUtils.isBlank(nomLot)) {
            LOGGER.error("Import: Erreur contrôle : Le nom du Lot ne peut être pas vide");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Le nom du Lot ne peut être pas vide");
        }
    }

    /**
     * Importe tous les fichiers CSV en paramètre.
     *
     * @param csvDataCenter         Fichier CSV des data centers
     * @param csvEquipementPhysique Fichier CSV des équipements physiques
     * @param csvEquipementVirtuel  Fichier CSV des équipements virtuels
     * @param csvApplication        Fichier CSV des applications
     * @param csvMessagerie         Fichier CSV de la messagerie
     * @param csvEntite             Fichier CSV des entités
     * @param dateLot               Date du lot associée aux fichiers
     * @param nomOrganisation       Nom de l'organisation
     * @param nomLot                Nom du lot
     * @return {@link List} des {@link RapportImportRest} correspondant à l'import
     * @throws ValidationException en cas d'absence de donner à pousser dans le système.
     */
    private List<RapportImportRest> importAllCsv(MultipartFile csvDataCenter, MultipartFile csvEquipementPhysique, MultipartFile csvEquipementVirtuel, MultipartFile csvApplication, MultipartFile csvMessagerie, MultipartFile csvEntite, LocalDate dateLot, String nomOrganisation, String nomLot) throws ValidationException {
        // Lecture & conversion
        var resultatImport = importDonneesEntreePort.importCsv(csvDataCenter, csvEquipementPhysique, csvEquipementVirtuel, csvApplication, csvMessagerie, csvEntite, dateLot, nomOrganisation, nomLot);

        saveDonneesEntreePort.save(resultatImport.getDonneesEntree());

        return resultatImport.getRapports().stream()
                .map(donneesEntreeMapper::toRestDTO)
                .toList();
    }

}
