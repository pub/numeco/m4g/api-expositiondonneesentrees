package org.mte.numecoeval.expositiondonneesentrees.infrastructure.adapters;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.expositiondonneesentrees.domain.exception.ReferentielRuntimeException;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.ReferentielTypeEquipement;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.ReferentielTypeEquipementServicePort;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.handler.RestClientResponseErrorHandler;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.TypeEquipementMapper;
import org.mte.numecoeval.referentiel.api.dto.TypeEquipementDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
@Slf4j
public class ReferentielTypeEquipementRestClient implements ReferentielTypeEquipementServicePort {


    @Value("${numecoeval.referentiel.server.url}")
    private  String referentielServeurUrl;

    @Value("${numecoeval.referentiel.endpoint.typesEquipement}")
    private String referentielRequestUri;

    private final RestTemplate restTemplate;
    private final TypeEquipementMapper typeEquipementMapper;

    public ReferentielTypeEquipementRestClient(RestTemplateBuilder restTemplateBuilder, TypeEquipementMapper typeEquipementMapper) {
        restTemplate = restTemplateBuilder
                .errorHandler(new RestClientResponseErrorHandler(
                        "ErrCalcFonc",
                        "Aucun Type Equipement disponible"
                ))
                .build();
        this.typeEquipementMapper = typeEquipementMapper;
    }

    @Override
    public List<ReferentielTypeEquipement> getAllTypesEquipement() {
        try {
            TypeEquipementDTO[] dtos= restTemplate
                    .getForObject(referentielServeurUrl + referentielRequestUri, TypeEquipementDTO[].class);

            if(dtos!=null){
                return Arrays.stream(dtos)
                        .map(typeEquipementMapper::toTypeEquipement)
                        .toList();
            }
        } catch (ReferentielRuntimeException e) {
            log.error("Une erreur est survenue lors de l'appel à l'API Réferentiel : URL : {}, Message : {}", referentielServeurUrl + referentielRequestUri, e.getMessage());
        } catch (Exception e) {
            log.error("Une erreur générale est survenue lors de l'appel à l'API Réferentiel : URL : {}, Message : {}", referentielServeurUrl + referentielRequestUri, e.getMessage());
        }
        return Collections.emptyList();
    }

}
