package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jdbc;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DemandeCalcul;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.RapportDemandeCalcul;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.input.SoumissionCalculPort;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.StatutTraitement;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class SoumissionCalculPortJdbcImpl implements SoumissionCalculPort {

    private static final String STATUT_TRAITEMENT_A_INGERER = StatutTraitement.A_INGERER.getValue();

    private static final String STATUT_TRAITEMENT_EN_ATTENTE = StatutTraitement.EN_ATTENTE.getValue();

    JdbcTemplate jdbcTemplate;

    @Transactional
    @Override
    public RapportDemandeCalcul soumissionCalcul(DemandeCalcul demandeCalcul) {
        validate(demandeCalcul);

        var rapport = new RapportDemandeCalcul();
        rapport.setDateLot(demandeCalcul.getDateLot());
        rapport.setNomOrganisation(demandeCalcul.getNomOrganisation());

        jdbcTemplate.update(getUpdateStatementForTable("en_donnees_entrees"), STATUT_TRAITEMENT_A_INGERER, demandeCalcul.getNomLot(), STATUT_TRAITEMENT_EN_ATTENTE);
        rapport.setNbrDataCenter(jdbcTemplate.update(getUpdateStatementForTable("en_data_center"), STATUT_TRAITEMENT_A_INGERER, demandeCalcul.getNomLot(), STATUT_TRAITEMENT_EN_ATTENTE));
        rapport.setNbrEquipementPhysique(jdbcTemplate.update(getUpdateStatementForTable("en_equipement_physique"), STATUT_TRAITEMENT_A_INGERER, demandeCalcul.getNomLot(), STATUT_TRAITEMENT_EN_ATTENTE));
        rapport.setNbrMessagerie(jdbcTemplate.update(getUpdateStatementForTable("en_messagerie"), STATUT_TRAITEMENT_A_INGERER, demandeCalcul.getNomLot(), STATUT_TRAITEMENT_EN_ATTENTE));
        return rapport;
    }

    @Override
    public RapportDemandeCalcul rejeuCalcul(DemandeCalcul demandeCalcul) {
        validate(demandeCalcul);

        var rapport = new RapportDemandeCalcul();
        rapport.setDateLot(demandeCalcul.getDateLot());
        rapport.setNomOrganisation(demandeCalcul.getNomOrganisation());

        jdbcTemplate.update(getUpdateForRejeuStatementForTable("en_donnees_entrees"), STATUT_TRAITEMENT_A_INGERER, demandeCalcul.getNomLot());
        rapport.setNbrDataCenter(jdbcTemplate.update(getUpdateForRejeuStatementForTable("en_data_center"), STATUT_TRAITEMENT_A_INGERER, demandeCalcul.getNomLot()));
        rapport.setNbrEquipementPhysique(jdbcTemplate.update(getUpdateForRejeuStatementForTable("en_equipement_physique"), STATUT_TRAITEMENT_A_INGERER, demandeCalcul.getNomLot()));
        rapport.setNbrEquipementVirtuel(jdbcTemplate.update(getUpdateForRejeuStatementForTable("en_equipement_virtuel"), STATUT_TRAITEMENT_A_INGERER, demandeCalcul.getNomLot()));
        rapport.setNbrApplication(jdbcTemplate.update(getUpdateForRejeuStatementForTable("en_application"), STATUT_TRAITEMENT_A_INGERER, demandeCalcul.getNomLot()));
        rapport.setNbrMessagerie(jdbcTemplate.update(getUpdateForRejeuStatementForTable("en_messagerie"), STATUT_TRAITEMENT_A_INGERER, demandeCalcul.getNomLot()));
        return rapport;
    }

    private String getUpdateStatementForTable(String table) {
        return "UPDATE "+table+" SET date_update = NOW(), statut_traitement = ? " +
                "WHERE nom_lot = ? and statut_traitement = ?";
    }

    private String getUpdateForRejeuStatementForTable(String table) {
        return "UPDATE "+table+" SET date_update = NOW(), statut_traitement = ? " +
                "WHERE nom_lot = ?";
    }
}
