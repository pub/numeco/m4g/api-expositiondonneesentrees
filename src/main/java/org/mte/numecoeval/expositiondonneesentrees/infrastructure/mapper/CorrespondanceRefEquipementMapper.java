package org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.ReferentielCorrespondanceRefEquipement;
import org.mte.numecoeval.referentiel.api.dto.CorrespondanceRefEquipementDTO;

@Mapper(componentModel = "spring")
public interface CorrespondanceRefEquipementMapper {

   ReferentielCorrespondanceRefEquipement toDomain(CorrespondanceRefEquipementDTO dto);
}
