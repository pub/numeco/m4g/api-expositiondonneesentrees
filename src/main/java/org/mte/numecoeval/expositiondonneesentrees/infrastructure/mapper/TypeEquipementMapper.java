package org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.ReferentielTypeEquipement;
import org.mte.numecoeval.referentiel.api.dto.TypeEquipementDTO;

@Mapper(componentModel = "spring")
public interface TypeEquipementMapper {

   ReferentielTypeEquipement toTypeEquipement(TypeEquipementDTO typeEquipementDTO);
}
