package org.mte.numecoeval.expositiondonneesentrees;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExpositionDonneesEntreesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExpositionDonneesEntreesApplication.class, args);
	}

}
