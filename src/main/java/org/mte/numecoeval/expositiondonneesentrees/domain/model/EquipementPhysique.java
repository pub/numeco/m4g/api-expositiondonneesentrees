package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import org.apache.commons.collections4.CollectionUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Accessors(chain = true)
public class EquipementPhysique extends AbstractEntree {
    String nomEquipementPhysique;
    String modele;
    String type;
    String statut;
    String paysDUtilisation;
    String utilisateur;
    LocalDate dateAchat;
    LocalDate dateRetrait;
    String nbCoeur;
    String nomCourtDatacenter;
    Double nbJourUtiliseAn;
    Float goTelecharge;
    Double consoElecAnnuelle;
    boolean serveur;
    Double dureeVieDefaut;
    String nomEntite;
    // référence d'équipement par défaut, propre au traitement
    String refEquipementParDefaut;
    // référence d'équipement retenu via Correspondance dans le référentiel, propre au traitement
    String refEquipementRetenu;
    Double quantite;

    List<EquipementVirtuel> equipementsVirtuels = new ArrayList<>();

    /**
     * Renvoie le nombre d'équipements virtuels rattachés à l'équipement physique.
     * @return {@link Integer} nombre d'équipements virtuels rattachés à l'équipement physique
     */
    public Integer getNbEquipementsVirtuels() {
        return CollectionUtils.size(equipementsVirtuels);
    }

    /**
     * Renvoie le nombre total de VCPU (la somme des vCPU définis dans les équipements virtuels).
     * Renvoie {@code null} si un des équipements virtuels à un nombre de VCPU à {@code null} ou 0.
     * @return {@link Integer} nombre total de VCPU ou {@code null}
     */
    public Integer getNbTotalVCPU() {
        boolean vCPUNoK = CollectionUtils.emptyIfNull(equipementsVirtuels)
                .stream()
                .anyMatch(vm -> vm.getVCPU() == null || vm.getVCPU() == 0);

        if(vCPUNoK) {
            return null;
        }

        return CollectionUtils.emptyIfNull(equipementsVirtuels)
                .stream()
                .mapToInt(EquipementVirtuel::getVCPU)
                .sum();
    }

    /**
     * Renvoie le nombre total de la capacité de stockage (la somme des capacités de stockages définis dans les équipements virtuels).
     * Renvoie {@code null} si un des équipements virtuels à la capacité de stockage à {@code null} ou 0.
     * @return {@link Double} nombre total de VCPU ou {@code null}
     */
    public Double getStockageTotalVirtuel() {
        boolean stockageOk = CollectionUtils.emptyIfNull(equipementsVirtuels)
                .stream()
                .anyMatch(vm -> vm.getCapaciteStockage() == null || vm.getCapaciteStockage() == 0);

        if(stockageOk) {
            return null;
        }

        return CollectionUtils.emptyIfNull(equipementsVirtuels)
                .stream()
                .mapToDouble(EquipementVirtuel::getCapaciteStockage)
                .sum();
    }

}
