package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Builder
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReferentielCorrespondanceRefEquipement {

    String modeleEquipementSource;
    String refEquipementCible;

}
