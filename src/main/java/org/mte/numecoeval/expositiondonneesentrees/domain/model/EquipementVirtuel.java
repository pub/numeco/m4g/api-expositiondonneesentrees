package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Accessors(chain = true)
public class EquipementVirtuel extends AbstractEntree {
    String nomEquipementVirtuel;
    String nomEquipementPhysique;
    String nomSourceDonneeEquipementPhysique;
    Integer vCPU;
    String cluster;
    String nomEntite;
    Double consoElecAnnuelle;
    String typeEqv;
    Double capaciteStockage;
    Double cleRepartition;

    List<Application> applications = new ArrayList<>();

    /**
     * Renvoie le nombre d'applications rattachées à l'équipement virtuel.
     * @return {@link Integer} nombre d'applications rattachées à l'équipement virtuel
     */
    public Integer getNbApplications() {
        return CollectionUtils.size(applications);
    }

}
