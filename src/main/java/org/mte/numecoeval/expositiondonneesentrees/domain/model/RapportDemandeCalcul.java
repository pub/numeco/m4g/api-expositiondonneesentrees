package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@SuperBuilder
@Accessors(chain = true)
public class RapportDemandeCalcul {
    String nomLot;
    LocalDate dateLot;
    String nomOrganisation;
    Integer nbrDataCenter;
    Integer nbrEquipementPhysique;
    Integer nbrEquipementVirtuel;
    Integer nbrApplication;
    Integer nbrMessagerie;
}
