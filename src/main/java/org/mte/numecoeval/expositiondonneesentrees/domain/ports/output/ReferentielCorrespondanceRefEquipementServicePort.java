package org.mte.numecoeval.expositiondonneesentrees.domain.ports.output;

import org.mte.numecoeval.expositiondonneesentrees.domain.model.ReferentielCorrespondanceRefEquipement;

public interface ReferentielCorrespondanceRefEquipementServicePort {
    ReferentielCorrespondanceRefEquipement getCorrespondance(String modele);
}
