package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ReferentielTypeEquipement {
    String type;
    boolean serveur;
    String commentaire;
    Double dureeVieDefaut;
    String source;
    // Référence de l'équipement par défaut, permet des correspondances en cas d'absence de correspondance direct.
    String refEquipementParDefaut;
}
