package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Accessors(chain = true)
public class DonneesEntree extends AbstractEntree{

    List<DataCenter> dataCenters = new ArrayList<>();

    List<EquipementPhysique> equipementsPhysiques = new ArrayList<>();

    List<Messagerie> messageries = new ArrayList<>();

    List<Entite> entites = new ArrayList<>();

}
