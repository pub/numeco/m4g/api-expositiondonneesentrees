package org.mte.numecoeval.expositiondonneesentrees.domain.ports.output;

import org.mte.numecoeval.expositiondonneesentrees.domain.model.ReferentielTypeEquipement;

import java.util.List;

public interface ReferentielTypeEquipementServicePort {
    List<ReferentielTypeEquipement> getAllTypesEquipement();
}
