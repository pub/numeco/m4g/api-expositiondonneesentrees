package org.mte.numecoeval.expositiondonneesentrees.domain.ports.input;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.tuple.Pair;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Application;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DataCenter;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Entite;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementPhysique;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementVirtuel;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Messagerie;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.RapportImport;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.ResultatImport;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.List;

@SuppressWarnings("java:S107") // Obligatoire à cause de la gestion des fichiers différents au niveau contrat d'interface
public interface ImportDonneesEntreePort {
    String EQUIPEMENT_PHYSIQUE_CSV_HEADER = "modele;quantite;nomEquipementPhysique;type;statut;paysDUtilisation;utilisateur;dateAchat;dateRetrait;nbCoeur;nomCourtDatacenter;goTelecharge;nbJourUtiliseAn;consoElecAnnuelle";
    String CSV_SEPARATOR = ";";
    String DATA_CENTER_CSV_HEADER = "nomCourtDatacenter;nomLongDatacenter;pue;localisation";

    String EQUIPEMENT_VIRTUEL_CSV_HEADER = "nomEquipementPhysique;vCPU;cluster";

    String APPLICATION_CSV_HEADER = "nomApplication;typeEnvironnement;nomEquipementPhysique;domaine;sousDomaine";
    String MESSAGERIE_CSV_HEADER = "nombreMailEmis;nombreMailEmisXDestinataires;volumeTotalMailEmis;MoisAnnee";
    String ENTITE_CSV_HEADER = "nomEntite;nbCollaborateurs;responsableEntite;responsableNumeriqueDurable";
    String MESSAGE_AVERTISSEMENT_LIGNE_INCONSISTENTE = "Fichier %s : La ligne n°%d n'est pas consistente avec les headers du fichier";
    String MESSAGE_AVERTISSEMENT_LIGNE_INCORRECTE = "Fichier %s : La ligne n°%d est incorrecte pour l'objet %s requise";
    String MESSAGE_AVERTISSEMENT_LIGNE_TYPE_EQUIPEMENT_ERREUR = "Fichier %s : La ligne n°%d est incorrecte : référence %s de type d'équipement inexistante";

    ResultatImport importCsv(MultipartFile csvDataCenter,
                             MultipartFile csvEquipementPhysique, MultipartFile csvEquipementVirtuel, MultipartFile csvApplication, MultipartFile csvMessagerie, MultipartFile csvEntite, LocalDate dateLot, String nomOrganisation, String nomLot);

    Pair<RapportImport, List<DataCenter>> importDataCenter(String nomLot, LocalDate dateLot, String nomOrganisation, MultipartFile csvDataCenter);

    Pair<RapportImport, List<EquipementPhysique>> importEquipementsPhysiques(String nomLot, LocalDate dateLot, String nomOrganisation, MultipartFile csvEquipementPhysique);

    Pair<RapportImport, List<EquipementVirtuel>> importEquipementsVirtuels(String nomLot, LocalDate dateLot, String nomOrganisation, MultipartFile csvEquipementVirtuel, List<EquipementPhysique> equipementsPhysiquesDisponibles);

    Pair<RapportImport, List<Application>> importApplications(String nomLot, LocalDate dateLot, String nomOrganisation, MultipartFile csvApplication, List<EquipementVirtuel> equipementsVirtuelsDisponibles);
    Pair<RapportImport, List<Messagerie>> importMessageries(String nomLot, LocalDate dateLot, String nomOrganisation, MultipartFile csvMessagerie);

    Pair<RapportImport, List<Entite>> importEntite(String nomLot, LocalDate dateLot, String nomOrganisation, MultipartFile csvMessagerie);

    default boolean isFieldIsMappedAtLeastOnce(CSVRecord csvRecord, String mainName, String... alternativeNames) {
        if(csvRecord.isMapped(mainName)) {
            return true;
        }

        for (String alternativeName : alternativeNames) {
            if(csvRecord.isMapped(alternativeName)) {
                return true;
            }
        }
        return false;
    }
}
