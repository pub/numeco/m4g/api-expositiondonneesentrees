package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@SuperBuilder
@NoArgsConstructor
public class RapportImport {

    String fichier;

    List<String> erreurs = new ArrayList<>();

    long nbrLignesImportees = 0;
}
