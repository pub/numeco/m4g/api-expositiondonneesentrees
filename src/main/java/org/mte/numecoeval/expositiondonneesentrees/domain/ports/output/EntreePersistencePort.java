package org.mte.numecoeval.expositiondonneesentrees.domain.ports.output;

import org.mte.numecoeval.expositiondonneesentrees.domain.model.AbstractEntree;

import java.util.List;

public interface EntreePersistencePort<T extends AbstractEntree> {
    void save(T entree);
    void saveAll(List<T> entrees);
}
