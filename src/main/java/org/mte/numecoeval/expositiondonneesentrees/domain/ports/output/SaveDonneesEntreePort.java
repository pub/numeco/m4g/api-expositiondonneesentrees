package org.mte.numecoeval.expositiondonneesentrees.domain.ports.output;

import org.mte.numecoeval.expositiondonneesentrees.domain.model.DonneesEntree;

public interface SaveDonneesEntreePort {

    /**
     * Sauvegarde de toutes les données d'entrées
     * @param donneesEntree {@link DonneesEntree} données à traiter
     */
    void save(DonneesEntree donneesEntree);

}
