package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Accessors(chain = true)
public class Messagerie extends AbstractEntree {
    Integer nombreMailEmis;
    Integer nombreMailEmisXDestinataires;
    Integer volumeTotalMailEmis;
    Integer moisAnnee;
    String nomEntite;

}
