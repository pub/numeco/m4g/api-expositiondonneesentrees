package org.mte.numecoeval.expositiondonneesentrees.domain.ports.input.impl;

import lombok.AllArgsConstructor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Application;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DataCenter;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Entite;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementPhysique;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementVirtuel;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Messagerie;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.RapportImport;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.ReferentielTypeEquipement;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.ResultatImport;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.input.ImportDonneesEntreePort;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.ReferentielCorrespondanceRefEquipementServicePort;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.ReferentielTypeEquipementServicePort;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.StatutTraitement;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.helper.CSVHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
public class ImportDonneesEntreePortImpl implements ImportDonneesEntreePort {

    private static final String LABEL_NOM_EQUIPEMENT_PHYSIQUE = "nomEquipementPhysique";
    private static final String LABEL_NOM_VM = "nomVM";
    private static final String LABEL_NOM_EQUIPEMENT_VIRTUEL = "nomEquipementVirtuel";

    private static final String[] EQUIPEMENT_PHYSIQUE_HEADER = EQUIPEMENT_PHYSIQUE_CSV_HEADER.split(CSV_SEPARATOR);

    private static final String[] DATA_CENTER_HEADER = DATA_CENTER_CSV_HEADER.split(CSV_SEPARATOR);

    private static final String[] EQUIPEMENT_VIRTUEL_HEADER = EQUIPEMENT_VIRTUEL_CSV_HEADER.split(CSV_SEPARATOR);

    private static final String[] APPLICATION_HEADER = APPLICATION_CSV_HEADER.split(CSV_SEPARATOR);
    private static final String[] MESSAGERIE_HEADER = MESSAGERIE_CSV_HEADER.split(CSV_SEPARATOR);
    private static final String[] ENTITE_HEADER = ENTITE_CSV_HEADER.split(CSV_SEPARATOR);
    private static final String HEADER_NOM_ENTITE = "nomEntite";
    private static final String HEADER_NOM_SOURCE_DONNEE = "nomSourceDonnee";


    private static final Logger LOGGER = LoggerFactory.getLogger(ImportDonneesEntreePortImpl.class);
    private static final String STATUT_TRAITEMENT_EN_ATTENTE = StatutTraitement.EN_ATTENTE.getValue();

    final ReferentielTypeEquipementServicePort referentielTypeEquipementServicePort;

    final ReferentielCorrespondanceRefEquipementServicePort referentielCorrespondanceRefEquipementServicePort;

    @Override
    public ResultatImport importCsv(MultipartFile csvDataCenter,
                                    MultipartFile csvEquipementPhysique,
                                    MultipartFile csvEquipementVirtuel,
                                    MultipartFile csvApplication,
                                    MultipartFile csvMessagerie,
                                    MultipartFile csvEntite,
                                    LocalDate dateLot, String nomOrganisation, String nomLot) {

        ResultatImport resultatImport = new ResultatImport();
        resultatImport.getDonneesEntree().setStatutTraitement(STATUT_TRAITEMENT_EN_ATTENTE);
        resultatImport.getDonneesEntree().setDateLot(dateLot);
        resultatImport.getDonneesEntree().setNomOrganisation(nomOrganisation);
        resultatImport.getDonneesEntree().setNomLot(nomLot);

        if(csvDataCenter != null) {
            Pair<RapportImport,List<DataCenter>> importDataCenter = importDataCenter(nomLot, dateLot, nomOrganisation, csvDataCenter);
            resultatImport.getRapports().add(importDataCenter.getKey());
            resultatImport.getDonneesEntree().setDataCenters(importDataCenter.getValue());
        }

        if(csvEquipementPhysique != null) {
            Pair<RapportImport,List<EquipementPhysique>> importEquipementsPhysiques = importEquipementsPhysiques(nomLot, dateLot, nomOrganisation, csvEquipementPhysique);
            resultatImport.getRapports().add(importEquipementsPhysiques.getKey());
            resultatImport.getDonneesEntree().setEquipementsPhysiques(importEquipementsPhysiques.getValue());

            // Pas d'import d'équipement Virtuel sans équipement physique
            if(csvEquipementVirtuel != null) {
                // Mise à jour automatiques des données via l'équipement physique
                Pair<RapportImport, List<EquipementVirtuel>> importEquipementVirtuel = importEquipementsVirtuels(nomLot, dateLot, nomOrganisation, csvEquipementVirtuel, resultatImport.getDonneesEntree().getEquipementsPhysiques());
                resultatImport.getRapports().add(importEquipementVirtuel.getKey());

                // Pas d'import d'application sans équipement virtuel
                if(csvApplication != null) {
                    // Mise à jour automatiques des données via l'équipement virtuel
                    Pair<RapportImport, List<Application>> importApplications = importApplications(nomLot, dateLot, nomOrganisation, csvApplication, importEquipementVirtuel.getValue());
                    resultatImport.getRapports().add(importApplications.getKey());
                }

            }
        }

        if(csvMessagerie != null) {
            Pair<RapportImport,List<Messagerie>> importMessagerie = importMessageries(nomLot, dateLot, nomOrganisation, csvMessagerie);
            resultatImport.getRapports().add(importMessagerie.getKey());
            resultatImport.getDonneesEntree().setMessageries(importMessagerie.getValue());
        }

        if(csvEntite != null) {
            Pair<RapportImport,List<Entite>> importEntites = importEntite(nomLot, dateLot, nomOrganisation, csvEntite);
            resultatImport.getRapports().add(importEntites.getKey());
            resultatImport.getDonneesEntree().setEntites(importEntites.getValue());
        }
        return resultatImport;
    }

    @Override
    public Pair<RapportImport,List<DataCenter>> importDataCenter(String nomLot, LocalDate dateLot, String nomOrganisation, MultipartFile csvDataCenter) {
        List<DataCenter> domainModels = new ArrayList<>();
        RapportImport rapportImport = new RapportImport();
        rapportImport.setFichier( csvDataCenter.getOriginalFilename() );
        try (Reader reader = new InputStreamReader(csvDataCenter.getInputStream())) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.builder()
                    .setHeader()
                    .setDelimiter(CSV_SEPARATOR)
                    .setTrim(true)
                    .setAllowMissingColumnNames(true)
                    .setSkipHeaderRecord(true)
                    .build().parse(reader);
            records.forEach(csvRecord ->
            {
                if(!Arrays.stream(DATA_CENTER_HEADER).allMatch(csvRecord::isMapped)) {
                    rapportImport.getErreurs().add(MESSAGE_AVERTISSEMENT_LIGNE_INCONSISTENTE.formatted(csvDataCenter.getOriginalFilename(), csvRecord.getRecordNumber()+1));
                }
                else {
                    domainModels.add(DataCenter.builder()
                            .statutTraitement(STATUT_TRAITEMENT_EN_ATTENTE)
                            .nomLot(nomLot)
                            .dateLot(dateLot)
                            .nomOrganisation(nomOrganisation)
                            .nomCourtDatacenter(CSVHelper.safeString(csvRecord, "nomCourtDatacenter"))
                            .nomLongDatacenter(CSVHelper.safeString(csvRecord, "nomLongDatacenter"))
                            .pue(CSVHelper.safeDouble(csvRecord, "pue"))
                            .localisation(CSVHelper.safeString(csvRecord, "localisation"))
                            .nomEntite(CSVHelper.safeString(csvRecord, HEADER_NOM_ENTITE))
                            .nomSourceDonnee(CSVHelper.safeString(csvRecord, HEADER_NOM_SOURCE_DONNEE))
                            .build());
                }
            });

        } catch (FileNotFoundException e) {
            LOGGER.error("Erreur CSV pour des DataCenters introuvable", e);
            return Pair.of(RapportImport.builder()
                            .erreurs(Collections.singletonList("Le fichier CSV des DataCenters n'est pas trouvable."))
                            .build()
                    ,Collections.emptyList()
            );
        } catch (Exception e) {
            LOGGER.error("Erreur durant la lecture d'un CSV pour des DataCenters", e);

            return Pair.of(RapportImport.builder()
                            .erreurs(Collections.singletonList("Le fichier CSV des DataCenter n'est pas lisible par le système."))
                            .build()
                    ,Collections.emptyList()
            );
        }

        rapportImport.setNbrLignesImportees(domainModels.size());

        return Pair.of(rapportImport, domainModels);
    }

    @Override
    public Pair<RapportImport,List<EquipementPhysique>> importEquipementsPhysiques(String nomLot, LocalDate dateLot, String nomOrganisation, MultipartFile csvEquipementPhysique) {
        List<EquipementPhysique> domainModels = new ArrayList<>();
        RapportImport rapportImport = new RapportImport();
        rapportImport.setFichier(csvEquipementPhysique.getOriginalFilename());
        List<ReferentielTypeEquipement> typesEquipement =  referentielTypeEquipementServicePort.getAllTypesEquipement();

        try (Reader reader = new InputStreamReader(csvEquipementPhysique.getInputStream())) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.builder()
                    .setHeader()
                    .setAllowMissingColumnNames(true)
                    .setDelimiter(CSV_SEPARATOR)
                    .setTrim(true)
                    .setSkipHeaderRecord(true)
                    .build().parse(reader);
            records.forEach(csvRecord ->
            {
                String typeEquipement = csvRecord.get("type");
                var refTypeEquipementOpt= typesEquipement.stream()
                        .filter(refType->refType.getType().equals(typeEquipement))
                        .findFirst();

                if(!Arrays.stream(EQUIPEMENT_PHYSIQUE_HEADER).allMatch(csvRecord::isMapped)) {
                    rapportImport.getErreurs().add(MESSAGE_AVERTISSEMENT_LIGNE_INCONSISTENTE.formatted(csvEquipementPhysique.getOriginalFilename(), csvRecord.getRecordNumber()+1));
                }else if (refTypeEquipementOpt.isEmpty()){
                    rapportImport.getErreurs().add(MESSAGE_AVERTISSEMENT_LIGNE_TYPE_EQUIPEMENT_ERREUR.formatted(csvEquipementPhysique.getOriginalFilename(), csvRecord.getRecordNumber()+1, csvRecord.get("type")));
                }
                else {
                    var refTypeEquipement = refTypeEquipementOpt.get();
                    EquipementPhysique equipementToAdd = EquipementPhysique.builder()
                            .statutTraitement(STATUT_TRAITEMENT_EN_ATTENTE)
                            .nomLot(nomLot)
                            .dateLot(dateLot)
                            .nomOrganisation(nomOrganisation)
                            .modele(CSVHelper.safeString(csvRecord, "modele", "refEquipement"))
                            .type(csvRecord.get("type"))
                            .quantite(CSVHelper.safeDouble(csvRecord, "quantite"))
                            .nomEquipementPhysique(CSVHelper.safeString(csvRecord, LABEL_NOM_EQUIPEMENT_PHYSIQUE, "equipement"))
                            .statut(CSVHelper.safeString(csvRecord, "statut"))
                            .paysDUtilisation(CSVHelper.safeString(csvRecord, "paysDUtilisation"))
                            .utilisateur(CSVHelper.safeString(csvRecord,"utilisateur"))
                            .dateAchat(CSVHelper.safeParseLocalDate(csvRecord, "dateAchat"))
                            .dateRetrait(CSVHelper.safeParseLocalDate(csvRecord, "dateRetrait"))
                            .nbCoeur(CSVHelper.safeString(csvRecord, "nbCoeur"))
                            .nomCourtDatacenter(CSVHelper.safeString(csvRecord, "nomCourtDatacenter", "refDatacenter"))
                            .goTelecharge(NumberUtils.toFloat(CSVHelper.safeString(csvRecord, "goTelecharge")))
                            .nbJourUtiliseAn(CSVHelper.safeDouble(csvRecord, "nbJourUtiliseAn"))
                            .consoElecAnnuelle(CSVHelper.safeDouble(csvRecord, "consoElecAnnuelle"))
                            .equipementsVirtuels(new ArrayList<>())
                            .dureeVieDefaut(refTypeEquipement.getDureeVieDefaut())
                            .serveur(refTypeEquipement.isServeur())
                            .refEquipementParDefaut(refTypeEquipement.getRefEquipementParDefaut())
                            .nomEntite(CSVHelper.safeString(csvRecord, HEADER_NOM_ENTITE))
                            .nomSourceDonnee(CSVHelper.safeString(csvRecord, HEADER_NOM_SOURCE_DONNEE))
                            .build();

                    // Ajout de la refEquipement cible via le référentiel CorrespondanceRefEquipement
                    var correspondance = referentielCorrespondanceRefEquipementServicePort.getCorrespondance(equipementToAdd.getModele());
                    if(correspondance != null) {
                        equipementToAdd.setRefEquipementRetenu(correspondance.getRefEquipementCible());
                    }

                    domainModels.add(equipementToAdd);
                }
            });

        } catch (FileNotFoundException e) {
            LOGGER.error("Erreur CSV pour des équipements physiques introuvable", e);
            return Pair.of(RapportImport.builder()
                            .erreurs(Collections.singletonList("Le fichier CSV des équipements physiques n'est pas trouvable."))
                            .build()
                    ,Collections.emptyList()
            );
        } catch (Exception e) {
            LOGGER.error("Erreur durant la lecture d'un CSV pour des équipements physiques", e);

            return Pair.of(RapportImport.builder()
                            .erreurs(Collections.singletonList("Le fichier CSV des équipements physiques n'est pas lisible par le système."))
                            .build()
                    ,Collections.emptyList()
            );
        }

        rapportImport.setNbrLignesImportees(domainModels.size());

        return Pair.of(rapportImport, domainModels);
    }

    @Override
    public Pair<RapportImport, List<EquipementVirtuel>> importEquipementsVirtuels(String nomLot, LocalDate dateLot, String nomOrganisation, MultipartFile csvEquipementVirtuel, List<EquipementPhysique> equipementsPhysiquesDisponibles) {
        List<EquipementVirtuel> domainModels = new ArrayList<>();
        RapportImport rapportImport = new RapportImport();
        rapportImport.setFichier(csvEquipementVirtuel.getOriginalFilename());

        try (Reader reader = new InputStreamReader(csvEquipementVirtuel.getInputStream())) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.builder()
                    .setHeader()
                    .setAllowMissingColumnNames(true)
                    .setDelimiter(CSV_SEPARATOR)
                    .setTrim(true)
                    .setSkipHeaderRecord(true)
                    .build().parse(reader);
            records.forEach(csvRecord ->
            {
                boolean isRecordOK = isFieldIsMappedAtLeastOnce(csvRecord, LABEL_NOM_EQUIPEMENT_VIRTUEL, LABEL_NOM_VM)
                        && Arrays.stream(EQUIPEMENT_VIRTUEL_HEADER).allMatch(csvRecord::isMapped);
                if(!isRecordOK) {
                    rapportImport.getErreurs().add(MESSAGE_AVERTISSEMENT_LIGNE_INCONSISTENTE.formatted(csvEquipementVirtuel.getOriginalFilename(), csvRecord.getRecordNumber()+1));
                }
                else {
                    EquipementVirtuel equipementVirtuel = EquipementVirtuel.builder()
                            .statutTraitement(STATUT_TRAITEMENT_EN_ATTENTE)
                            .nomLot(nomLot)
                            .dateLot(dateLot)
                            .nomOrganisation(nomOrganisation)
                            .nomEquipementVirtuel(CSVHelper.safeString(csvRecord, LABEL_NOM_EQUIPEMENT_VIRTUEL, LABEL_NOM_VM))
                            .nomEquipementPhysique(CSVHelper.safeString(csvRecord, LABEL_NOM_EQUIPEMENT_PHYSIQUE))
                            .nomSourceDonneeEquipementPhysique(CSVHelper.safeString(csvRecord, "nomSourceDonneeEquipementPhysique"))
                            .vCPU(CSVHelper.safeInteger(csvRecord, "vCPU"))
                            .cluster(CSVHelper.safeString(csvRecord, "cluster"))
                            .consoElecAnnuelle(CSVHelper.safeDouble(csvRecord, "consoElecAnnuelle"))
                            .typeEqv(CSVHelper.safeString(csvRecord, "typeEqv"))
                            .capaciteStockage(CSVHelper.safeDouble(csvRecord, "capaciteStockage"))
                            .cleRepartition(CSVHelper.safeDouble(csvRecord, "cleRepartition"))
                            .nomEntite(CSVHelper.safeString(csvRecord, HEADER_NOM_ENTITE))
                            .nomSourceDonnee(CSVHelper.safeString(csvRecord, HEADER_NOM_SOURCE_DONNEE))
                            .applications(new ArrayList<>())
                            .build();

                    var optionalEquipementPhysique = equipementsPhysiquesDisponibles.stream()
                            .filter(equipementPhysique -> equipementVirtuel.getNomEquipementPhysique().equals(equipementPhysique.getNomEquipementPhysique()))
                            .findAny();
                    if(optionalEquipementPhysique.isEmpty()) {
                        rapportImport.getErreurs().add("Fichier %s : La ligne n°%d n'est pas cohérente avec les données d'équipement fournies: équipement physique %s introuvable".formatted(csvEquipementVirtuel.getOriginalFilename(), csvRecord.getRecordNumber()+1, equipementVirtuel.getNomEquipementPhysique()));
                    }
                    else {
                        optionalEquipementPhysique.get().getEquipementsVirtuels().add(equipementVirtuel);
                        domainModels.add(equipementVirtuel);
                    }
                }
            });

        } catch (FileNotFoundException e) {
            LOGGER.error("Erreur CSV pour des équipements virtuels introuvable", e);
            return Pair.of(RapportImport.builder()
                            .erreurs(Collections.singletonList("Le fichier CSV des équipements virtuels n'est pas trouvable."))
                            .build()
                    ,Collections.emptyList()
            );
        } catch (Exception e) {
            LOGGER.error("Erreur durant la lecture d'un CSV pour des équipements virtuels", e);

            return Pair.of(RapportImport.builder()
                            .erreurs(Collections.singletonList("Le fichier CSV des équipements virtuels n'est pas lisible par le système."))
                            .build()
                    ,Collections.emptyList()
            );
        }

        rapportImport.setNbrLignesImportees(domainModels.size());

        return Pair.of(rapportImport, domainModels);

    }

    @Override
    public Pair<RapportImport, List<Application>> importApplications(String nomLot, LocalDate dateLot, String nomOrganisation, MultipartFile csvApplication, List<EquipementVirtuel> equipementsVirtuelsDisponibles) {
        List<Application> domainModels = new ArrayList<>();
        RapportImport rapportImport = new RapportImport();
        rapportImport.setFichier(csvApplication.getOriginalFilename());

        try (Reader reader = new InputStreamReader(csvApplication.getInputStream())) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.builder()
                    .setHeader()
                    .setAllowMissingColumnNames(true)
                    .setDelimiter(CSV_SEPARATOR)
                    .setTrim(true)
                    .setSkipHeaderRecord(true)
                    .build().parse(reader);
            records.forEach(csvRecord ->
            {
                boolean isRecordOK = isFieldIsMappedAtLeastOnce(csvRecord, LABEL_NOM_EQUIPEMENT_VIRTUEL, LABEL_NOM_VM)
                        && Arrays.stream(APPLICATION_HEADER).allMatch(csvRecord::isMapped);
                if(!isRecordOK) {
                    rapportImport.getErreurs().add(MESSAGE_AVERTISSEMENT_LIGNE_INCONSISTENTE.formatted(csvApplication.getOriginalFilename(), csvRecord.getRecordNumber()+1));
                }
                else {
                    Application application = Application.builder()
                            .statutTraitement(STATUT_TRAITEMENT_EN_ATTENTE)
                            .nomLot(nomLot)
                            .dateLot(dateLot)
                            .nomOrganisation(nomOrganisation)
                            .nomApplication(CSVHelper.safeString(csvRecord, "nomApplication"))
                            .typeEnvironnement(CSVHelper.safeString(csvRecord, "typeEnvironnement"))
                            .nomEquipementVirtuel(CSVHelper.safeString(csvRecord, LABEL_NOM_EQUIPEMENT_VIRTUEL, LABEL_NOM_VM))
                            .nomSourceDonneeEquipementVirtuel(CSVHelper.safeString(csvRecord, "nomSourceDonneeEquipementVirtuel"))
                            .domaine(CSVHelper.safeString(csvRecord, "domaine"))
                            .sousDomaine(CSVHelper.safeString(csvRecord, "sousDomaine"))
                            .nomEntite(CSVHelper.safeString(csvRecord, HEADER_NOM_ENTITE))
                            .nomEquipementPhysique(CSVHelper.safeString(csvRecord, LABEL_NOM_EQUIPEMENT_PHYSIQUE))
                            .nomSourceDonnee(CSVHelper.safeString(csvRecord, HEADER_NOM_SOURCE_DONNEE))
                            .build();
                    var optionalEquipementVirtuel = equipementsVirtuelsDisponibles.stream()
                            .filter(equipementVirtuel ->
                                    application.getNomEquipementVirtuel().equals(equipementVirtuel.getNomEquipementVirtuel())
                                            && equipementVirtuel.getNomEquipementPhysique().equals(application.getNomEquipementPhysique())
                            )
                            .findAny();
                    if(optionalEquipementVirtuel.isEmpty()) {
                        rapportImport.getErreurs().add("Fichier %s : La ligne n°%d n'est pas cohérente avec les données d'équipement fournies: équipement virtuel : Nom : %s, Référence équipement Physique: %s introuvable".formatted(csvApplication.getOriginalFilename(), csvRecord.getRecordNumber()+1, application.getNomEquipementVirtuel(), application.getNomEquipementPhysique()));
                    }
                    else {
                        optionalEquipementVirtuel.get().getApplications().add(application);
                        domainModels.add(application);
                    }
                }
            });

        } catch (FileNotFoundException e) {
            LOGGER.error("Erreur CSV pour des applications introuvable", e);
            return Pair.of(RapportImport.builder()
                            .erreurs(Collections.singletonList("Le fichier CSV des Applications n'est pas trouvable."))
                            .build()
                    ,Collections.emptyList()
            );
        } catch (Exception e) {
            LOGGER.error("Erreur durant la lecture d'un CSV pour des Applications", e);

            return Pair.of(RapportImport.builder()
                            .erreurs(Collections.singletonList("Le fichier CSV des Applications n'est pas lisible par le système."))
                            .build()
                    ,Collections.emptyList()
            );
        }

        rapportImport.setNbrLignesImportees(domainModels.size());

        return Pair.of(rapportImport, domainModels);
    }

    @Override
    public Pair<RapportImport, List<Messagerie>> importMessageries(String nomLot, LocalDate dateLot, String nomOrganisation, MultipartFile csvMessagerie) {
        List<Messagerie> domainModels = new ArrayList<>();
        RapportImport rapportImport = new RapportImport();
        rapportImport.setFichier(csvMessagerie.getOriginalFilename());

        try (Reader reader = new InputStreamReader(csvMessagerie.getInputStream())) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.builder()
                    .setHeader()
                    .setAllowMissingColumnNames(true)
                    .setDelimiter(CSV_SEPARATOR)
                    .setTrim(true)
                    .setSkipHeaderRecord(true)
                    .build().parse(reader);
            records.forEach(csvRecord ->
            {
                if(!Arrays.stream(MESSAGERIE_HEADER).allMatch(csvRecord::isMapped)) {
                    rapportImport.getErreurs().add(MESSAGE_AVERTISSEMENT_LIGNE_INCONSISTENTE.formatted(csvMessagerie.getOriginalFilename(), csvRecord.getRecordNumber()+1));
                }
                else {
                    domainModels.add(
                            Messagerie.builder()
                                    .statutTraitement(STATUT_TRAITEMENT_EN_ATTENTE)
                                    .nomLot(nomLot)
                                    .dateLot(dateLot)
                                    .nomOrganisation(nomOrganisation)
                                    .nombreMailEmis(CSVHelper.safeInteger(csvRecord,"nombreMailEmis"))
                                    .nombreMailEmisXDestinataires(CSVHelper.safeInteger(csvRecord,"nombreMailEmisXDestinataires"))
                                    .volumeTotalMailEmis(CSVHelper.safeInteger(csvRecord,"volumeTotalMailEmis"))
                                    .moisAnnee(CSVHelper.safeInteger(csvRecord,"MoisAnnee"))
                                    .nomEntite(CSVHelper.safeString(csvRecord, HEADER_NOM_ENTITE))
                                    .nomSourceDonnee(CSVHelper.safeString(csvRecord, HEADER_NOM_SOURCE_DONNEE))
                                    .build()
                    );
                }
            });

        } catch (FileNotFoundException e) {
            LOGGER.error("Erreur CSV pour de la messagerie introuvable", e);
            return Pair.of(RapportImport.builder()
                            .erreurs(Collections.singletonList("Le fichier CSV de la messagerie n'est pas trouvable."))
                            .build()
                    ,Collections.emptyList()
            );
        } catch (Exception e) {
            LOGGER.error("Erreur durant la lecture d'un CSV pour de la messagerie", e);

            return Pair.of(RapportImport.builder()
                            .erreurs(Collections.singletonList("Le fichier CSV de la messagerie n'est pas lisible par le système."))
                            .build()
                    ,Collections.emptyList()
            );
        }

        rapportImport.setNbrLignesImportees(domainModels.size());

        return Pair.of(rapportImport, domainModels);
    }

    @Override
    public Pair<RapportImport, List<Entite>> importEntite(String nomLot, LocalDate dateLot, String nomOrganisation, MultipartFile csvEntite) {
        List<Entite> domainModels = new ArrayList<>();
        RapportImport rapportImport = new RapportImport();
        rapportImport.setFichier(csvEntite.getOriginalFilename());

        try (Reader reader = new InputStreamReader(csvEntite.getInputStream())) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.builder()
                    .setHeader()
                    .setAllowMissingColumnNames(true)
                    .setDelimiter(CSV_SEPARATOR)
                    .setTrim(true)
                    .setSkipHeaderRecord(true)
                    .build().parse(reader);
            records.forEach(csvRecord ->
            {
                if(!Arrays.stream(ENTITE_HEADER).allMatch(csvRecord::isMapped)) {
                    rapportImport.getErreurs().add(MESSAGE_AVERTISSEMENT_LIGNE_INCONSISTENTE.formatted(csvEntite.getOriginalFilename(), csvRecord.getRecordNumber()+1));
                }
                else {
                    domainModels.add(
                            Entite.builder()
                                    .nomLot(nomLot)
                                    .dateLot(dateLot)
                                    .nomOrganisation(nomOrganisation)
                                    .nomEntite(CSVHelper.safeString(csvRecord, HEADER_NOM_ENTITE))
                                    .responsableEntite(CSVHelper.safeString(csvRecord, "responsableEntite"))
                                    .responsableNumeriqueDurable(CSVHelper.safeString(csvRecord, "responsableNumeriqueDurable"))
                                    .nbCollaborateurs(CSVHelper.safeInteger(csvRecord, "nbCollaborateurs"))
                                    .nomSourceDonnee(CSVHelper.safeString(csvRecord, HEADER_NOM_SOURCE_DONNEE))
                                    .build()
                    );
                }
            });

        } catch (FileNotFoundException e) {
            LOGGER.error("Erreur CSV pour des entités introuvable", e);
            return Pair.of(RapportImport.builder()
                            .erreurs(Collections.singletonList("Le fichier CSV des entités n'est pas trouvable."))
                            .build()
                    ,Collections.emptyList()
            );
        } catch (Exception e) {
            LOGGER.error("Erreur durant la lecture d'un CSV pour les entités", e);

            return Pair.of(RapportImport.builder()
                            .erreurs(Collections.singletonList("Le fichier CSV des entités n'est pas lisible par le système."))
                            .build()
                    ,Collections.emptyList()
            );
        }

        rapportImport.setNbrLignesImportees(domainModels.size());

        return Pair.of(rapportImport, domainModels);
    }
}
