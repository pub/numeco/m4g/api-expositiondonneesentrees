package org.mte.numecoeval.expositiondonneesentrees.config;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.common.ConsoleNotifier;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class WiremockServerConfig {

    @Value("${numecoeval.referentiel.server.url}")
    private  String referentielServeurUrl;

    @Bean
    public WireMockServer serveurWiremock() {
        log.info("Démarrage du serveur Wiremock sur le port 19090");
        var wireMockServer = new WireMockServer(
                WireMockConfiguration.options()
                        .port(19090)
                        .notifier(new ConsoleNotifier(true)));
        wireMockServer.start();
        return wireMockServer;
    }

}
