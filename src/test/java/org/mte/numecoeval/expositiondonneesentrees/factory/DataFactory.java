package org.mte.numecoeval.expositiondonneesentrees.factory;

import org.mte.numecoeval.expositiondonneesentrees.domain.model.Application;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DataCenter;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DonneesEntree;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementPhysique;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementVirtuel;
import org.mte.numecoeval.referentiel.api.dto.TypeEquipementDTO;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DataFactory {

    static public DonneesEntree donneesEntree() {
        DataCenter dataCenter01 = dataCenter("datacenter_01", "France_datacenter_01", 1.0d, "France");
        return DonneesEntree.builder()
                .dataCenters(Collections.singletonList(dataCenter01))
                .equipementsPhysiques(Arrays.asList(
                        equipementPhysique(null, "Apple Iphone 11-Thierry", "Apple Iphone 11", "Téléphone", 1.0d, "France", "", 4.0f, LocalDate.now().minusYears(2), LocalDate.now().plusYears(2)),
                        equipementPhysique(dataCenter01, "Xeon E-2174G:DC_Villeperdue_01", "Xeon E-2174G", "Serveur", 1.0d, "France", "", null, LocalDate.now().minusYears(2), LocalDate.now().plusYears(2)))
                )
                .build();
    }

    static public DataCenter dataCenter(String nomCourtDataCenter, String nomLongDataCenter, Double pue, String localisation) {

        return DataCenter.builder()
                .nomCourtDatacenter(nomCourtDataCenter)
                .nomLongDatacenter(nomLongDataCenter)
                .pue(pue)
                .localisation(localisation)
                .build();

    }

    static public EquipementPhysique equipementPhysique(DataCenter datacenter, String equipement, String refEquipement, String type, Double quantite, String paysUtilisation, String utillisateur, Float goTelecharge, LocalDate dateAchat, LocalDate dateRetrait) {
        var builder =  EquipementPhysique.builder()
                .nomEquipementPhysique(equipement)
                .modele(refEquipement)
                .type(type)
                .quantite(quantite)
                .dateAchat(dateAchat)
                .dateRetrait(dateRetrait)
                .goTelecharge(goTelecharge)
                .utilisateur(utillisateur)
                .paysDUtilisation(paysUtilisation)
                .nomCourtDatacenter(datacenter != null ? datacenter.getNomCourtDatacenter() : null);

        if("Serveur".equals(type)) {
            builder.equipementsVirtuels(Collections.singletonList(
                    equipementVirtuel(equipement, "VM1", 4, "Cluster1"))
            );
        }

        return builder.build();
    }

    static public EquipementVirtuel equipementVirtuel(String nomEquipementPhysique, String nomEquipementVirtuel, Integer vCPU, String cluster) {
        var builder = EquipementVirtuel.builder()
                .nomEquipementPhysique(nomEquipementPhysique)
                .nomEquipementVirtuel(nomEquipementVirtuel)
                .vCPU(vCPU)
                .cluster(cluster)
                .applications(Arrays.asList(
                        application(nomEquipementVirtuel, "Java", "Prod", "Back","JDBC"),
                        application(nomEquipementVirtuel, "JS", "Prod", "Front","Javascript")
                ));

        return builder.build();
    }

    static public Application application(String nomEquipementVirtuel, String nomApplication, String typeEnvironnement, String domaineApplicatif1, String domaineApplicatif2) {
        return Application.builder()
                .nomEquipementVirtuel(nomEquipementVirtuel)
                .nomApplication(nomApplication)
                .typeEnvironnement(typeEnvironnement)
                .domaine(domaineApplicatif1)
                .sousDomaine(domaineApplicatif2)
                .build();
    }

    static public List<TypeEquipementDTO> typesEquipement() {
        return Arrays.asList(
                TypeEquipementDTO.builder()
                        .type("Serveur")
                        .dureeVieDefaut(4.0d)
                        .source("Test")
                        .serveur(true)
                        .refEquipementParDefaut("serveur_par_defaut")
                        .build(),
                TypeEquipementDTO.builder()
                        .type("Smartphone")
                        .dureeVieDefaut(2.0d)
                        .source("Test")
                        .serveur(false)
                        .refEquipementParDefaut("smartphone_par_defaut")
                        .build(),
                TypeEquipementDTO.builder()
                        .type("Laptop")
                        .dureeVieDefaut(4.0d)
                        .source("Test")
                        .serveur(false)
                        .refEquipementParDefaut("laptop_par_defaut")
                        .build(),
                TypeEquipementDTO.builder()
                        .type("Ecran")
                        .dureeVieDefaut(5.0d)
                        .source("Test")
                        .serveur(false)
                        .refEquipementParDefaut("ecran_par_defaut")
                        .build(),
                TypeEquipementDTO.builder()
                        .type("Téléphone")
                        .dureeVieDefaut(5.0d)
                        .source("Test")
                        .serveur(false)
                        .refEquipementParDefaut("telephone_par_defaut")
                        .build(),
                TypeEquipementDTO.builder()
                        .type("ServeurCalcul")
                        .dureeVieDefaut(6.0d)
                        .source("Test")
                        .serveur(true)
                        .refEquipementParDefaut("serveur_par_defaut")
                        .build(),
                TypeEquipementDTO.builder()
                        .type("ServeurStockage")
                        .dureeVieDefaut(6.0d)
                        .source("Test")
                        .serveur(true)
                        .refEquipementParDefaut("serveur_par_defaut")
                        .build(),
                TypeEquipementDTO.builder()
                        .type("TelephoneMobile")
                        .dureeVieDefaut(2.5d)
                        .source("Test")
                        .serveur(false)
                        .refEquipementParDefaut("smartphone_par_defaut")
                        .build(),
                TypeEquipementDTO.builder()
                        .type("TelephoneFixe")
                        .dureeVieDefaut(10.0d)
                        .source("Test")
                        .serveur(false)
                        .refEquipementParDefaut("telephone_par_defaut")
                        .build(),
                TypeEquipementDTO.builder()
                        .type("Switch")
                        .dureeVieDefaut(6.0d)
                        .source("Test")
                        .serveur(false)
                        .build(),
                TypeEquipementDTO.builder()
                        .type("OrdinateurPortable")
                        .dureeVieDefaut(4.0d)
                        .source("Test")
                        .serveur(false)
                        .refEquipementParDefaut("laptop_par_defaut")
                        .build(),
                TypeEquipementDTO.builder()
                        .type("OrdinateurDeBureau")
                        .dureeVieDefaut(5.0d)
                        .source("Test")
                        .serveur(false)
                        .refEquipementParDefaut("ordinateur_par_defaut")
                        .build(),
                TypeEquipementDTO.builder()
                        .type("Imprimante")
                        .dureeVieDefaut(10.0d)
                        .source("Test")
                        .serveur(false)
                        .refEquipementParDefaut("imprimante_par_defaut")
                        .build()
        );
    }

}
