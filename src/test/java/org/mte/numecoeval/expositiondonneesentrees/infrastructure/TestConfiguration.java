package org.mte.numecoeval.expositiondonneesentrees.infrastructure;

import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.CorrespondanceRefEquipementMapper;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.CorrespondanceRefEquipementMapperImpl;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.TypeEquipementMapper;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.TypeEquipementMapperImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfiguration {
    @Bean
    public TypeEquipementMapper typeEquipementMapper(){return new TypeEquipementMapperImpl();
    }
    @Bean
    public CorrespondanceRefEquipementMapper correspondanceRefEquipementMapper(){return new CorrespondanceRefEquipementMapperImpl();
    }
}
