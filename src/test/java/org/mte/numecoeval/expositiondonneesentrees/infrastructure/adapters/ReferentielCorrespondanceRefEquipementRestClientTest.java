package org.mte.numecoeval.expositiondonneesentrees.infrastructure.adapters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mte.numecoeval.expositiondonneesentrees.ExpositionDonneesEntreesApplication;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.ReferentielCorrespondanceRefEquipementServicePort;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.TestConfiguration;
import org.mte.numecoeval.referentiel.api.dto.CorrespondanceRefEquipementDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@RestClientTest({ReferentielCorrespondanceRefEquipementRestClient.class})
@SpringJUnitConfig(classes ={TestConfiguration.class, ExpositionDonneesEntreesApplication.class})
class ReferentielCorrespondanceRefEquipementRestClientTest {

    @Autowired
    ReferentielCorrespondanceRefEquipementServicePort client;

    @Autowired
    private MockRestServiceServer server;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldGetResponse_whenCallGetAllTypesEquipement() throws JsonProcessingException {
        //GIVEN
        String modeleAttendu = "modele";
        CorrespondanceRefEquipementDTO correspondanceRefEquipementDTO = CorrespondanceRefEquipementDTO.builder()
                .modeleEquipementSource("modele")
                .refEquipementCible("refEquipementCible")
                .build();
       var typesDTOJson =  objectMapper.writeValueAsString(correspondanceRefEquipementDTO);
       server.expect(requestTo("http://localhost:19090/referentiel/correspondanceRefEquipement?modele=modele"))
               .andRespond(withSuccess(typesDTOJson, MediaType.APPLICATION_JSON));

        //WHEN
        var actual = client.getCorrespondance(modeleAttendu);

        //THEN
        assertEquals(modeleAttendu,actual.getModeleEquipementSource());
        assertEquals(correspondanceRefEquipementDTO.getModeleEquipementSource(),actual.getModeleEquipementSource());
        assertEquals(correspondanceRefEquipementDTO.getRefEquipementCible(),actual.getRefEquipementCible());
    }

    @Test
    void shouldReturnEmptyResult_whenServerError()throws Exception{
        server.expect(requestTo("http://localhost:19090/referentiel/correspondanceRefEquipement?modele=modele"))
                .andRespond(withServerError());
        var actual = client.getCorrespondance("modele");
        assertNull(actual);
    }
}
