package org.mte.numecoeval.expositiondonneesentrees.infrastructure.adapters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mte.numecoeval.expositiondonneesentrees.ExpositionDonneesEntreesApplication;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.ReferentielTypeEquipementServicePort;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.TestConfiguration;
import org.mte.numecoeval.referentiel.api.dto.TypeEquipementDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@RestClientTest({ReferentielTypeEquipementRestClient.class})
@SpringJUnitConfig(classes ={TestConfiguration.class, ExpositionDonneesEntreesApplication.class})
class ReferentielTypeEquipementRestClientTest {

    @Autowired
    ReferentielTypeEquipementServicePort client;
    @Autowired
    private MockRestServiceServer server;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldGetResponse_whenCallGetAllTypesEquipement() throws JsonProcessingException {
        //GIVEN
        TypeEquipementDTO type1 = TypeEquipementDTO.builder()
                .type("Serveur")
                .serveur(true)
                .dureeVieDefaut(20d)
                .build();
        TypeEquipementDTO type2 = TypeEquipementDTO.builder()
                .type("Ecran")
                .serveur(false)
                .dureeVieDefaut(5d)
                .build();
       var typesDTOJson =  objectMapper.writeValueAsString(new TypeEquipementDTO[]{type1,type2});
       server.expect(requestTo("http://localhost:19090/referentiel/typesEquipement"))
               .andRespond(withSuccess(typesDTOJson, MediaType.APPLICATION_JSON));

        //WHEN
        var actual = client.getAllTypesEquipement();

        //THEN
        assertEquals(2,actual.size());
    }

    @Test
    void shouldReturnEmptyResult_whenServerError()throws Exception{
        server.expect(requestTo("http://localhost:19090/referentiel/typesEquipement"))
                .andRespond(withServerError());
        var actual = client.getAllTypesEquipement();
        assertTrue(actual.isEmpty());
    }
}
