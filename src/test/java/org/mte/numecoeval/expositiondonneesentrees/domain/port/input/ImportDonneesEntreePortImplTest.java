package org.mte.numecoeval.expositiondonneesentrees.domain.port.input;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Application;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DataCenter;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementPhysique;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementVirtuel;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Messagerie;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.RapportImport;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.input.impl.ImportDonneesEntreePortImpl;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.ReferentielCorrespondanceRefEquipementServicePort;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.ReferentielTypeEquipementServicePort;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ImportDonneesEntreePortImplTest {
    @Mock
    MultipartFile fileToRead;
    @Mock
     ReferentielTypeEquipementServicePort referentielTypeEquipementServicePort;

    @Mock
    ReferentielCorrespondanceRefEquipementServicePort referentielCorrespondanceRefEquipementServicePort;
    ImportDonneesEntreePortImpl importDonneesEntreePort;
    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
        importDonneesEntreePort = new ImportDonneesEntreePortImpl(referentielTypeEquipementServicePort, referentielCorrespondanceRefEquipementServicePort);
    }

    @Test
    void importDataCenter_onIOException_shouldReturnReportWith1Error() throws IOException {
        Mockito.when(fileToRead.getInputStream()).thenThrow(new IOException("Test"));

        Pair<RapportImport, List<DataCenter>> resultImport = importDonneesEntreePort.importDataCenter(null, null, null, fileToRead);

        assertTrue(resultImport.getValue().isEmpty());
        assertEquals("Le fichier CSV des DataCenter n'est pas lisible par le système.", resultImport.getKey().getErreurs().get(0));
    }

    @Test
    void importDataCenter_onFileNotFoundException_shouldReturnReportWith1Error() throws IOException {
        Mockito.when(fileToRead.getInputStream()).thenThrow(new FileNotFoundException("Test"));

        Pair<RapportImport, List<DataCenter>> resultImport = importDonneesEntreePort.importDataCenter(null, null, null, fileToRead);

        assertTrue(resultImport.getValue().isEmpty());
        assertEquals("Le fichier CSV des DataCenters n'est pas trouvable.", resultImport.getKey().getErreurs().get(0));
    }

    @Test
    void importEquipementsPhysiques_onIOException_shouldReturnReportWith1Error() throws IOException {
        Mockito.when(fileToRead.getInputStream()).thenThrow(new IOException("Test"));

        Pair<RapportImport, List<EquipementPhysique>> resultImport = importDonneesEntreePort.importEquipementsPhysiques(null, null, null, fileToRead);

        assertTrue(resultImport.getValue().isEmpty());
        assertEquals("Le fichier CSV des équipements physiques n'est pas lisible par le système.", resultImport.getKey().getErreurs().get(0));
    }

    @Test
    void importEquipementsPhysiques_onFileNotFoundException_shouldReturnReportWith1Error() throws IOException {
        Mockito.when(fileToRead.getInputStream()).thenThrow(new FileNotFoundException("Test"));

        Pair<RapportImport, List<EquipementPhysique>> resultImport = importDonneesEntreePort.importEquipementsPhysiques(null, null, null, fileToRead);

        assertTrue(resultImport.getValue().isEmpty());
        assertEquals("Le fichier CSV des équipements physiques n'est pas trouvable.", resultImport.getKey().getErreurs().get(0));
    }

    @Test
    void importEquipementsVirtuels_onIOException_shouldReturnReportWith1Error() throws IOException {
        Mockito.when(fileToRead.getInputStream()).thenThrow(new IOException("Test"));

        Pair<RapportImport, List<EquipementVirtuel>> resultImport = importDonneesEntreePort.importEquipementsVirtuels(null, null, null, fileToRead, Collections.emptyList());

        assertTrue(resultImport.getValue().isEmpty());
        assertEquals("Le fichier CSV des équipements virtuels n'est pas lisible par le système.", resultImport.getKey().getErreurs().get(0));
    }

    @Test
    void importEquipementsVirtuels_onFileNotFoundException_shouldReturnReportWith1Error() throws IOException {
        Mockito.when(fileToRead.getInputStream()).thenThrow(new FileNotFoundException("Test"));

        Pair<RapportImport, List<EquipementVirtuel>> resultImport = importDonneesEntreePort.importEquipementsVirtuels(null, null, null, fileToRead, Collections.emptyList());

        assertTrue(resultImport.getValue().isEmpty());
        assertEquals("Le fichier CSV des équipements virtuels n'est pas trouvable.", resultImport.getKey().getErreurs().get(0));
    }

    @Test
    void importApplications_onIOException_shouldReturnReportWith1Error() throws IOException {
        Mockito.when(fileToRead.getInputStream()).thenThrow(new IOException("Test"));

        Pair<RapportImport, List<Application>> resultImport = importDonneesEntreePort.importApplications(null, null, null, fileToRead, Collections.emptyList());

        assertTrue(resultImport.getValue().isEmpty());
        assertEquals("Le fichier CSV des Applications n'est pas lisible par le système.", resultImport.getKey().getErreurs().get(0));
    }

    @Test
    void importApplications_onFileNotFoundException_shouldReturnReportWith1Error() throws IOException {
        Mockito.when(fileToRead.getInputStream()).thenThrow(new FileNotFoundException("Test"));

        Pair<RapportImport, List<Application>> resultImport = importDonneesEntreePort.importApplications(null, null, null, fileToRead, Collections.emptyList());

        assertTrue(resultImport.getValue().isEmpty());
        assertEquals("Le fichier CSV des Applications n'est pas trouvable.", resultImport.getKey().getErreurs().get(0));
    }

    @Test
    void importMessageries_onIOException_shouldReturnReportWith1Error() throws IOException {
        Mockito.when(fileToRead.getInputStream()).thenThrow(new IOException("Test"));

        Pair<RapportImport, List<Messagerie>> resultImport = importDonneesEntreePort.importMessageries(null, null, null, fileToRead);

        assertTrue(resultImport.getValue().isEmpty());
        assertEquals("Le fichier CSV de la messagerie n'est pas lisible par le système.", resultImport.getKey().getErreurs().get(0));
    }

    @Test
    void importMessageries_onFileNotFoundException_shouldReturnReportWith1Error() throws IOException {
        Mockito.when(fileToRead.getInputStream()).thenThrow(new FileNotFoundException("Test"));

        Pair<RapportImport, List<Messagerie>> resultImport = importDonneesEntreePort.importMessageries(null, null, null, fileToRead);

        assertTrue(resultImport.getValue().isEmpty());
        assertEquals("Le fichier CSV de la messagerie n'est pas trouvable.", resultImport.getKey().getErreurs().get(0));
    }

    @Test
    void importEntite_onIOException_shouldReturnReportWith1Error() throws IOException {
        Mockito.when(fileToRead.getInputStream()).thenThrow(new IOException("Test"));

        var resultImport = importDonneesEntreePort.importEntite(null, null, null, fileToRead);

        assertTrue(resultImport.getValue().isEmpty());
        assertEquals("Le fichier CSV des entités n'est pas lisible par le système.", resultImport.getKey().getErreurs().get(0));
    }

    @Test
    void importEntite_onFileNotFoundException_shouldReturnReportWith1Error() throws IOException {
        Mockito.when(fileToRead.getInputStream()).thenThrow(new FileNotFoundException("Test"));

        var resultImport = importDonneesEntreePort.importEntite(null, null, null, fileToRead);

        assertTrue(resultImport.getValue().isEmpty());
        assertEquals("Le fichier CSV des entités n'est pas trouvable.", resultImport.getKey().getErreurs().get(0));
    }
}
