package org.mte.numecoeval.expositiondonneesentrees.steps;

import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Et;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.DataCenterEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.DataCenterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mte.numecoeval.expositiondonneesentrees.steps.ExpositionRestStepDefinitions.FRENCH_DATE_FORMATTER;

public class DataCenterStepDefinitions {

    @Autowired
    DataCenterRepository dataCenterRepository;

    @Autowired
    ScenarioInformation scenarioInformation;

    @Alors("Les données chargées contiennent {int} objets data centers")
    public void checkCountDataCenter(int nbrDataCenter) {
        // When
        scenarioInformation.checkScenarionInformation();
        assertEquals(nbrDataCenter,
                dataCenterRepository.findAll()
                        .stream()
                        .filter(entity ->
                                scenarioInformation.getDateLot().equals(entity.getDateLot())
                                        && scenarioInformation.getNomOrganisation().equals(entity.getNomOrganisation())
                        )
                        .count()
        );
    }

    @Et("Il existe un data center {string} \\(nom complet: {string}) avec un PUE de {string} localisé en {string}")
    public void checkDataCenter(String nomCourtDatacenter, String nomLongDatacenter, String pue, String localisation) {
        var dataCenters = dataCenterRepository.findAll();

        assertTrue(dataCenters
                .stream()
                .anyMatch(dataCenter ->
                        nomCourtDatacenter.equals(dataCenter.getNomCourtDatacenter())
                                && nomLongDatacenter.equals(dataCenter.getNomLongDatacenter())
                                && Double.valueOf(pue).equals(dataCenter.getPue())
                                && localisation.equals(dataCenter.getLocalisation())
                ));
    }

    @Alors("Le statut de tous les objets DataCenter sauvegardés pour la date {string} et l'organisation {string} est {string}")
    public void checkStatutDataCenterWithDateLotAndNomOrganisation(String dateLot, String nomOrganisation, String statutAttendu) {
        var dateToSearch = LocalDate.parse(dateLot, FRENCH_DATE_FORMATTER);
        var defaultText = "pour la date " +dateToSearch + " et l'organisation " + nomOrganisation;

        assertTrue(dataCenterRepository.findAll(Example.of(
                        DataCenterEntity.builder()
                                .dateLot(dateToSearch)
                                .nomOrganisation(nomOrganisation)
                                .build()
                ))
                .stream().allMatch(dto -> statutAttendu.equals(dto.getStatutTraitement())), "Tous les data centers "+ defaultText + " n'ont pas le bon statut : " + statutAttendu);
    }

    @Alors("Le statut de tous les objets DataCenter sauvegardés pour le lot {string} est {string}")
    public void checkStatutDataCenterWithNomLot(String nomLot, String statutAttendu) {
        assertTrue(dataCenterRepository.findAll(Example.of(
                        DataCenterEntity.builder()
                                .nomLot(nomLot)
                                .build()
                ))
                .stream().allMatch(dto -> statutAttendu.equals(dto.getStatutTraitement())), "Tous les data centers pour le lot "+ nomLot + " n'ont pas le bon statut : " + statutAttendu);
    }

}
