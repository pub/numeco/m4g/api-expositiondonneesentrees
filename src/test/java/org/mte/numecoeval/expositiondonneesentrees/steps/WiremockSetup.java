package org.mte.numecoeval.expositiondonneesentrees.steps;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.fr.Soit;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.expositiondonneesentrees.config.WiremockServerConfig;
import org.mte.numecoeval.expositiondonneesentrees.factory.DataFactory;
import org.mte.numecoeval.referentiel.api.dto.CorrespondanceRefEquipementDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.util.UriComponentsBuilder;

import static com.github.tomakehurst.wiremock.client.WireMock.get;

/**
 * Cette classe contient des méthodes appelés automatiquement à l'exécution d'un scénario
 * pour la configuration du serveur Wiremock.
 * Même sans annotation de classe, le contexte Spring est bien disponible grace au contexte défini
 * dans CucumberIntegrationTest.
 * @see org.mte.numecoeval.expositiondonneesentrees.CucumberIntegrationTest pour le context Spring
 * @see WiremockServerConfig pour les configurations
 */
@Slf4j
public class WiremockSetup {

    @Autowired
    WireMockServer serveurReferentiel;

    @Value("${numecoeval.referentiel.endpoint.typesEquipement}")
    private String referentielRequestTypesEquipement;

    @Value("${numecoeval.referentiel.endpoint.correspondancesRefEquipement}")
    private String referentielRequestCorrespondanceRefEquipement;

    @Before
    public void setupStubForWiremockServer() {
        log.info("Configuration du Serveur WireMock - Ajout du Stub par défaut sur l'URI {}", referentielRequestTypesEquipement);
        serveurReferentiel.stubFor(
                get(referentielRequestTypesEquipement)
                        .willReturn(ResponseDefinitionBuilder.okForJson(DataFactory.typesEquipement()))
        );
    }

    @Soit("Les correspondances d'équipements suivantes")
    public void setupCorrespondances(DataTable dataTable) {
        log.info("Configuration du Serveur WireMock - Ajout des Stub par défaut sur l'URI {}", referentielRequestCorrespondanceRefEquipement);
        dataTable.asMaps().forEach( dataTableRow -> {
            serveurReferentiel.stubFor(
                    get(UriComponentsBuilder.fromUriString(referentielRequestCorrespondanceRefEquipement)
                            .queryParam("modele", dataTableRow.get("modeleEquipementSource").trim())
                            .build().toUri().toString())
                            .willReturn(ResponseDefinitionBuilder.okForJson(
                                    CorrespondanceRefEquipementDTO.builder()
                                            .modeleEquipementSource(dataTableRow.get("modeleEquipementSource"))
                                            .refEquipementCible(dataTableRow.get("refEquipementCible"))
                                            .build()
                            ))
            );
        });

    }

    @After
    public void resetWiremockServer() {
        log.info("Ré-initialisation du serveur Wiremock");
        serveurReferentiel.resetAll();
    }

}
