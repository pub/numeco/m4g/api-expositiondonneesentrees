package org.mte.numecoeval.expositiondonneesentrees.steps;

import io.cucumber.java.fr.Et;
import io.cucumber.java.fr.Quand;
import io.cucumber.java.fr.Soit;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.commons.lang3.StringUtils;
import org.mte.numecoeval.expositiondonneesentrees.domain.exception.ValidationException;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.server.ImportsApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ExpositionRestStepDefinitions {

    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static final DateTimeFormatter FRENCH_DATE_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public static final String NULL_VALUE = "null";

    private static final Logger LOGGER = LoggerFactory.getLogger(ExpositionRestStepDefinitions.class);

    @Autowired
    Environment environment;

    @Autowired
    ScenarioInformation scenarioInformation;

    public void setupRestAssured() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = Integer.parseInt(Objects.requireNonNull(environment.getProperty("server.port")));
    }

    @Soit("la date de lot {string} et l'organisation {string}")
    public void updateScenarioInformation(String dateLot, String nomOrganisation) {
        scenarioInformation.setDateLot(LocalDate.parse(dateLot, FRENCH_DATE_FORMATTER));
        scenarioInformation.setNomOrganisation(nomOrganisation);
    }
    public void updateScenarioInformation(String dateLot, String nomOrganisation, Response reponseDernierAppelAPI) {
        scenarioInformation.setDateLot(LocalDate.parse(dateLot, FRENCH_DATE_FORMATTER));
        scenarioInformation.setNomOrganisation(nomOrganisation);
        scenarioInformation.setReponseDernierAppelAPI(reponseDernierAppelAPI);
    }

    @Quand("pour la date {string} et l'organisation {string}, le fichier CSV {string} est envoyé")
    public void importCSVEquipementPhysiqueUniquement(String dateLot, String nomOrganisation, String fichierCsv) throws IOException, ValidationException, NoSuchMethodException {
        importCsvComplet(String.join("|", nomOrganisation, dateLot), dateLot, nomOrganisation, "inutilise.csv", fichierCsv, "inutilise.csv", "inutilise.csv", "inutilise.csv", null);
    }

    @Quand("pour la date {string} et l'organisation {string}, les fichiers dataCenters: {string}, equipementPhysique: {string} sont importés")
    public void importCSVPartielSprint2(String dateLot, String nomOrganisation, String csvDataCenters, String csvEquipementsPhysiques) throws FileNotFoundException, NoSuchMethodException {
        importCsvComplet(String.join("|", nomOrganisation, dateLot), dateLot, nomOrganisation, csvDataCenters, csvEquipementsPhysiques, "inutilise.csv", "inutilise.csv", "inutilise.csv", null);
    }

    @Quand("pour la date {string} et l'organisation {string} les fichiers dataCenters: {string}, equipementPhysique: {string}, equipementVirtuel: {string}, applications: {string} sont importés")
    public void importCsvPartielSprint4(String dateLot, String nomOrganisation, String csvDataCenters, String csvEquipementsPhysiques, String csvEquipementsVirtuels, String csvApplications) throws FileNotFoundException, NoSuchMethodException {
        importCsvComplet(String.join("|", nomOrganisation, dateLot), dateLot, nomOrganisation, csvDataCenters, csvEquipementsPhysiques, csvEquipementsVirtuels, csvApplications, "inutilise.csv", null);
    }

    private File getFile(String filepath, String nomLogiqueFichier) {
        if(StringUtils.isBlank(filepath)) {
            filepath = "ceFichierNePeutPasExister.toto";
        }
        var file = new File(filepath);
        try {
            return ResourceUtils.getFile("classpath:" + filepath);
        }
        catch (FileNotFoundException e) {
            LOGGER.info("Fichier introuvable : nom logique : {}, chemin : {}", nomLogiqueFichier, filepath);
        }

        return file;
    }

    @Quand("pour la date {string} et l'organisation {string} les fichiers dataCenters: {string}, equipementPhysique: {string}, equipementVirtuel: {string}, applications: {string}, messagerie: {string} sont importés")
    public void importCsvSprint10(String dateLot, String nomOrganisation, String csvDataCenters, String csvEquipementsPhysiques, String csvEquipementsVirtuels, String csvApplications, String csvMessageries) throws FileNotFoundException, NoSuchMethodException {
        importCsvComplet(String.join("|", nomOrganisation, dateLot), dateLot, nomOrganisation, csvDataCenters, csvEquipementsPhysiques, csvEquipementsVirtuels, csvApplications, csvMessageries, null);
    }
    @Quand("pour le lot {string}, la date {string} et l'organisation {string} les fichiers dataCenters: {string}, equipementPhysique: {string}, equipementVirtuel: {string}, applications: {string}, messagerie: {string}, entités: {string} sont importés")
    public void importCsvComplet(String nomLot, String dateLot, String nomOrganisation, String csvDataCenters, String csvEquipementsPhysiques, String csvEquipementsVirtuels, String csvApplications, String csvMessageries, String csvEntites) throws FileNotFoundException, NoSuchMethodException {
        LOGGER.info("Chargement des CSV Nom de Lot: {}, Date de Lot: {}, Nom Organisation: {}, DataCenter: {}, EquipementPhysique: {}, EquipementVirtuel: {}, Application: {}, Messagerie: {}, Entités: {}",
                nomLot, dateLot, nomOrganisation, csvDataCenters, csvEquipementsPhysiques, csvEquipementsVirtuels, csvApplications, csvMessageries, csvEntites
        );
        File fileDataCenter = getFile(csvDataCenters, "fichiers des datacenters");
        File fileEquipementsPhysiques = getFile(csvEquipementsPhysiques, "fichiers des équipements physiques");
        File fileEquipementsVirtuels = getFile(csvEquipementsVirtuels, "fichiers des équipements virtuels");
        File fileApplications = getFile(csvApplications, "fichiers des applications");
        File fileMessageries = getFile(csvMessageries, "fichier de la messagerie");
        File fileEntites = getFile(csvEntites, "fichier des entités");

        // Envoie du CSV via appel REST
        setupRestAssured();
        String requestUri = ImportsApi.class.getMethod("importCSV", String.class, MultipartFile.class, MultipartFile.class, MultipartFile.class, MultipartFile.class, MultipartFile.class, MultipartFile.class, LocalDate.class, String.class)
                .getAnnotation(RequestMapping.class).value()[0];
        var requestSpecification = given();
        requestSpecification.multiPart("nomLot", nomLot);
        requestSpecification.multiPart("dateLot", LocalDate.parse(dateLot, FRENCH_DATE_FORMATTER).toString());
        requestSpecification.multiPart("nomOrganisation", nomOrganisation);
        if(fileDataCenter.exists()) {
            requestSpecification.multiPart("csvDataCenter", fileDataCenter);
        }
        if(fileEquipementsPhysiques.exists()) {
            requestSpecification.multiPart("csvEquipementPhysique", fileEquipementsPhysiques);
        }
        if(fileEquipementsVirtuels.exists()) {
            requestSpecification.multiPart("csvEquipementVirtuel", fileEquipementsVirtuels);
        }
        if(fileApplications.exists()) {
            requestSpecification.multiPart("csvApplication", fileApplications);
        }
        if(fileMessageries.exists()) {
            requestSpecification.multiPart("csvMessagerie", fileMessageries);
        }
        if(fileEntites.exists()) {
            requestSpecification.multiPart("csvEntite", fileEntites);
        }

        updateScenarioInformation(dateLot, nomOrganisation,
                requestSpecification
                        .post(requestUri)
                        .thenReturn()
        );
    }

    @Et("La réponse de l'API est {int}")
    public void checkDerniereReponseAPI(int codeStatut) {
        LOGGER.info("Réponse de l'API : {}", scenarioInformation.getReponseDernierAppelAPI().getBody().prettyPrint());
        assertNotNull(scenarioInformation.getReponseDernierAppelAPI(), "La dernière réponse d'API ne peut être null");
        assertEquals(codeStatut, scenarioInformation.getReponseDernierAppelAPI().getStatusCode());
    }

}
