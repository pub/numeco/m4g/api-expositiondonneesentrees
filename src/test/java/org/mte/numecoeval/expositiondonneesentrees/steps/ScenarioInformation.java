package org.mte.numecoeval.expositiondonneesentrees.steps;

import io.cucumber.spring.ScenarioScope;
import io.restassured.response.Response;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Contexte globale du scénério en cours d'exécution.
 * Uniquement utilisable après un import.
 */
@Component
@ScenarioScope
@Getter
@Setter
@NoArgsConstructor
public class ScenarioInformation {

    private LocalDate dateLot;

    private String nomOrganisation;

    private Response reponseDernierAppelAPI;

    public void checkScenarionInformation() {
        assertNotNull(dateLot);
        assertNotNull(nomOrganisation);
    }
}
