package org.mte.numecoeval.expositiondonneesentrees.steps;

import io.cucumber.java.DataTableType;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Et;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.ApplicationEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.ApplicationRepository;
import org.mte.numecoeval.expositiondonneesentrees.test.DataTableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mte.numecoeval.expositiondonneesentrees.steps.ExpositionRestStepDefinitions.FORMATTER;
import static org.mte.numecoeval.expositiondonneesentrees.steps.ExpositionRestStepDefinitions.FRENCH_DATE_FORMATTER;

public class ApplicationStepDefinitions {

    @Autowired
    ApplicationRepository applicationRepository;

    @Autowired
    ScenarioInformation scenarioInformation;

    @Alors("Les données chargées contiennent {int} objets application")
    public void checkCountApplication(int nbrApplication) {
        // When
        scenarioInformation.checkScenarionInformation();
        assertEquals(nbrApplication,
                applicationRepository.findAll()
                        .stream()
                        .filter(entity ->
                                scenarioInformation.getDateLot().equals(entity.getDateLot())
                                        && scenarioInformation.getNomOrganisation().equals(entity.getNomOrganisation())
                        )
                        .count()
        );
    }

    @Alors("Les données chargées pour la date de lot {string} et le nom d'organisation {string} contiennent {int} objets application")
    public void checkCountApplication(String dateLotString, String nomOrganisation, int nbrApplication) {
        // When
        assertEquals(nbrApplication,
                applicationRepository.findAll()
                        .stream()
                        .filter(entity ->
                                LocalDate.parse(dateLotString, FORMATTER).equals(entity.getDateLot())
                                        && nomOrganisation.equals(entity.getNomOrganisation())
                        )
                        .count()
        );
    }

    @Et("Il existe une application {string} pour le type d'environnement {string} associé à la VM {string} de l'équipement {string} et avec pour domaines applicatifs {string} et {string}")
    public void checkApplication(String nomApplication, String typeEnvironnement, String nomEquipementVirtuel, String refEquipement, String domaine, String sousDomaine) {
        var matchingEntities = applicationRepository.findAll()
                .stream()
                .filter(application ->
                        scenarioInformation.getDateLot().equals(application.getDateLot())
                                && scenarioInformation.getNomOrganisation().equals(application.getNomOrganisation())
                                && nomApplication.equals(application.getNomApplication())
                                && typeEnvironnement.equals(application.getTypeEnvironnement())
                                && nomEquipementVirtuel.equals(application.getNomEquipementVirtuel())
                                && refEquipement.equals(application.getNomEquipementPhysique())
                                && domaine.equals(application.getDomaine())
                                && sousDomaine.equals(application.getSousDomaine())
                )
                .toList();

        assertFalse(matchingEntities.isEmpty());
        assertEquals(1, matchingEntities.size());
    }

    @Et("Pour la date de lot {string} et le nom d'organisation {string}, il existe une application {string} pour le type d'environnement {string} associé à la VM {string} de l'équipement {string} et avec pour domaines applicatifs {string} et {string}")
    public void checkApplication(String dateLot, String nomOrganisation, String nomApplication, String typeEnvironnement, String nomEquipementVirtuel, String refEquipement, String domaine, String sousDomaine) {
        var matchingEntities = applicationRepository.findAll(
                Example.of(
                        ApplicationEntity.builder()
                                .dateLot(LocalDate.parse(dateLot))
                                .nomOrganisation(nomOrganisation)
                                .nomApplication(nomApplication)
                                .typeEnvironnement(typeEnvironnement)
                                .nomEquipementVirtuel(nomEquipementVirtuel)
                                .nomEquipementPhysique(refEquipement)
                                .domaine(domaine)
                                .sousDomaine(sousDomaine)
                                .build()
                )

        );

        assertFalse(matchingEntities.isEmpty());
        assertEquals(1, matchingEntities.size());
    }

    @Alors("Le statut de tous les objets Application sauvegardés pour la date {string} et l'organisation {string} est {string}")
    public void checkStatutApplicationWithDateLotAndNomOrganisation(String dateLot, String nomOrganisation, String statutAttendu) {
        var dateToSearch = LocalDate.parse(dateLot, FRENCH_DATE_FORMATTER);
        var defaultText = "pour la date " +dateToSearch + " et l'organisation " + nomOrganisation;

        assertTrue(applicationRepository.findAll(Example.of(
                        ApplicationEntity.builder()
                                .dateLot(dateToSearch)
                                .nomOrganisation(nomOrganisation)
                                .build()
                ))
                .stream().allMatch(dto -> statutAttendu.equals(dto.getStatutTraitement())), "Toutes les applications "+ defaultText + " n'ont pas le bon statut : " + statutAttendu);
    }

    @Alors("Le statut de tous les objets Application sauvegardés pour le lot {string} est {string}")
    public void checkStatutApplicationWithNomLot(String nomLot, String statutAttendu) {
        assertTrue(applicationRepository.findAll(Example.of(
                        ApplicationEntity.builder()
                                .nomLot(nomLot)
                                .build()
                ))
                .stream().allMatch(dto -> statutAttendu.equals(dto.getStatutTraitement())), "Toutes les applications du lot "+ nomLot + " n'ont pas le bon statut : " + statutAttendu);
    }

    @Et("Les instances d'applications suivantes sont disponibles en base")
    public void checkDataInDatabase(List<ApplicationEntity> objectsToCheck) {

        objectsToCheck.forEach(entiteToCheck -> {
            assertTrue((long) applicationRepository.findAll(Example.of(
                            entiteToCheck
                    ))
                    .size() > 0, "L'instance d'application de nom "+ entiteToCheck.getNomApplication() + "-" + entiteToCheck.getTypeEnvironnement() + "equipement virtuel: "+ entiteToCheck.getNomEquipementVirtuel() + ", nomLot: "+ entiteToCheck.getNomLot() + " n'est pas présente à l'identique en base");
        });


    }

    @DataTableType
    public ApplicationEntity application(Map<String, String> entry) {
        return ApplicationEntity.builder()
                // Général
                .nomOrganisation(DataTableUtils.safeString(entry, "nomOrganisation"))
                .nomSourceDonnee(DataTableUtils.safeString(entry, "nomSourceDonnee"))
                .nomLot(DataTableUtils.safeString(entry, "nomLot"))
                .dateLot(DataTableUtils.safeLocalDate(entry, "dateLot"))
                .nomEntite(DataTableUtils.safeString(entry, "nomEntite"))
                .statutTraitement(DataTableUtils.safeString(entry, "statutTraitement"))
                // Spécifique
                .nomApplication(DataTableUtils.safeString(entry, "nomApplication"))
                .typeEnvironnement(DataTableUtils.safeString(entry, "typeEnvironnement"))
                .domaine(DataTableUtils.safeString(entry, "domaine"))
                .sousDomaine(DataTableUtils.safeString(entry, "sousDomaine"))
                .nomEquipementVirtuel(DataTableUtils.safeString(entry, "nomEquipementVirtuel"))
                .nomEquipementPhysique(DataTableUtils.safeString(entry, "nomEquipementPhysique"))
                .nomSourceDonneeEquipementVirtuel(DataTableUtils.safeString(entry, "nomSourceDonneeEquipementVirtuel"))
                // Spécifique Moteur
                .build();
    }
}
