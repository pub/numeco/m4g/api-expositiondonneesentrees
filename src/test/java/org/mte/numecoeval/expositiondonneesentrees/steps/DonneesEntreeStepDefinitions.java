package org.mte.numecoeval.expositiondonneesentrees.steps;

import io.cucumber.java.fr.Alors;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.DonneesEntreesEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.DonneesEntreesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mte.numecoeval.expositiondonneesentrees.steps.ExpositionRestStepDefinitions.FRENCH_DATE_FORMATTER;

public class DonneesEntreeStepDefinitions {

    @Autowired
    DonneesEntreesRepository donneesEntreesRepository;

    @Autowired
    ScenarioInformation scenarioInformation;

    @Alors("Le statut de tous les objets DonneesEntrées sauvegardés pour la date {string} et l'organisation {string} est {string}")
    public void checkStatutDonneesEntreesWithDateLotAndNomOrganisation(String dateLot, String nomOrganisation, String statutAttendu) {
        var dateToSearch = LocalDate.parse(dateLot, FRENCH_DATE_FORMATTER);
        var defaultText = "pour la date " +dateToSearch + " et l'organisation " + nomOrganisation;

        assertTrue(donneesEntreesRepository.findAll(Example.of(
                        DonneesEntreesEntity.builder()
                                .dateLot(dateToSearch)
                                .nomOrganisation(nomOrganisation)
                                .build()
                ))
                .stream().allMatch(dto -> statutAttendu.equals(dto.getStatutTraitement())), "Toutes les données d'entrées "+ defaultText + " n'ont pas le bon statut : " + statutAttendu);
    }

    @Alors("Le statut de tous les objets DonneesEntrées sauvegardés pour le lot {string} est {string}")
    public void checkStatutDonneesEntreesWithNomLot(String nomLot, String statutAttendu) {
        assertTrue(donneesEntreesRepository.findAll(Example.of(
                        DonneesEntreesEntity.builder()
                                .nomLot(nomLot)
                                .build()
                ))
                .stream().allMatch(dto -> statutAttendu.equals(dto.getStatutTraitement())), "Toutes les données d'entrées du lot "+ nomLot + " n'ont pas le bon statut : " + statutAttendu);
    }
}
