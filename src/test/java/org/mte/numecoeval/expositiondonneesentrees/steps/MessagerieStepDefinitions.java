package org.mte.numecoeval.expositiondonneesentrees.steps;

import io.cucumber.java.DataTableType;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Et;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.MessagerieEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.MessagerieRepository;
import org.mte.numecoeval.expositiondonneesentrees.test.DataTableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mte.numecoeval.expositiondonneesentrees.steps.ExpositionRestStepDefinitions.FRENCH_DATE_FORMATTER;

public class MessagerieStepDefinitions {

    @Autowired
    MessagerieRepository messagerieRepository;

    @Autowired
    ScenarioInformation scenarioInformation;

    @Alors("Les données chargées contiennent {int} objets messageries")
    public void checkCountMessagerie(int nbrMessagerie) {
        // When
        scenarioInformation.checkScenarionInformation();
        assertEquals(nbrMessagerie,
                messagerieRepository.findAll()
                        .stream()
                        .filter(entity ->
                                scenarioInformation.getDateLot().equals(entity.getDateLot())
                                        && scenarioInformation.getNomOrganisation().equals(entity.getNomOrganisation())
                        )
                        .count()
        );
    }

    @Et("Il existe une messagerie pour le mois de {int} avec {int} mail émis, {int} mails émis par destinataire pour un volume total de {int} Mo")
    public void checkMessagerie(int moisAnnee, int nombreMailEmis, int nombreMailEmisXDestinataires, int volumeTotalMailEmis) {
        var matchingEntities = messagerieRepository.findAll()
                .stream()
                .filter(messagerie ->
                        scenarioInformation.getDateLot().equals(messagerie.getDateLot())
                                && scenarioInformation.getNomOrganisation().equals(messagerie.getNomOrganisation())
                                && messagerie.getMoisAnnee().equals(moisAnnee)
                                && messagerie.getNombreMailEmis().equals(nombreMailEmis)
                                && messagerie.getNombreMailEmisXDestinataires().equals(nombreMailEmisXDestinataires)
                                && messagerie.getVolumeTotalMailEmis().equals(volumeTotalMailEmis)
                )
                .toList();

        assertFalse(matchingEntities.isEmpty());
        assertEquals(1, matchingEntities.size());
    }

    @Alors("Le statut de tous les objets Messagerie sauvegardés pour la date {string} et l'organisation {string} est {string}")
    public void checkStatutMessagerieWithDateLotAndNomOrganisation(String dateLot, String nomOrganisation, String statutAttendu) {
        var dateToSearch = LocalDate.parse(dateLot, FRENCH_DATE_FORMATTER);
        var defaultText = "pour la date " +dateToSearch + " et l'organisation " + nomOrganisation;

        assertTrue(messagerieRepository.findAll(Example.of(
                        MessagerieEntity.builder()
                                .dateLot(dateToSearch)
                                .nomOrganisation(nomOrganisation)
                                .build()
                ))
                .stream().allMatch(dto -> statutAttendu.equals(dto.getStatutTraitement())), "Tous les éléments de messageries "+ defaultText + " n'ont pas le bon statut : " + statutAttendu);
    }

    @Alors("Le statut de tous les objets Messagerie sauvegardés pour le lot {string} est {string}")
    public void checkStatutMessagerieWithNomLot(String nomLot, String statutAttendu) {
        assertTrue(messagerieRepository.findAll(Example.of(
                        MessagerieEntity.builder()
                                .nomLot(nomLot)
                                .build()
                ))
                .stream().allMatch(dto -> statutAttendu.equals(dto.getStatutTraitement())), "Tous les éléments de messageries du lot "+ nomLot + " n'ont pas le bon statut : " + statutAttendu);
    }

    @Et("Les éléments de messagerie suivants sont disponibles en base")
    public void checkDataInDatabase(List<MessagerieEntity> objectsToCheck) {

        objectsToCheck.forEach(entiteToCheck -> {
            assertTrue((long) messagerieRepository.findAll(Example.of(
                            entiteToCheck
                    ))
                    .size() > 0, "L'élément de messagerie pour le mois "+ entiteToCheck.getMoisAnnee() + ", Entité: "+ entiteToCheck.getNomEntite() + ", nomLot: "+ entiteToCheck.getNomLot() + " n'est pas présent à l'identique en base");
        });


    }

    @DataTableType
    public MessagerieEntity messagerie(Map<String, String> entry) {
        return MessagerieEntity.builder()
                // Général
                .nomOrganisation(DataTableUtils.safeString(entry, "nomOrganisation"))
                .nomSourceDonnee(DataTableUtils.safeString(entry, "nomSourceDonnee"))
                .nomLot(DataTableUtils.safeString(entry, "nomLot"))
                .dateLot(DataTableUtils.safeLocalDate(entry, "dateLot"))
                .nomEntite(DataTableUtils.safeString(entry, "nomEntite"))
                .statutTraitement(DataTableUtils.safeString(entry, "statutTraitement"))
                // Spécifique
                .moisAnnee(DataTableUtils.safeInteger(entry, "moisAnnee"))
                .nombreMailEmis(DataTableUtils.safeInteger(entry, "nombreMailEmis"))
                .nombreMailEmisXDestinataires(DataTableUtils.safeInteger(entry, "nombreMailEmisXDestinataires"))
                .volumeTotalMailEmis(DataTableUtils.safeInteger(entry, "volumeTotalMailEmis"))
                // Spécifique Moteur
                .build();
    }
}
