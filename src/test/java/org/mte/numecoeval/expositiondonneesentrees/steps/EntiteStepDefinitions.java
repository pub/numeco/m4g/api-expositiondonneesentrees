package org.mte.numecoeval.expositiondonneesentrees.steps;

import io.cucumber.java.DataTableType;
import io.cucumber.java.fr.Et;
import jakarta.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Entite;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.EntiteEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.EntiteRepository;
import org.mte.numecoeval.expositiondonneesentrees.test.DataTableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
public class EntiteStepDefinitions {

    @Autowired
    EntiteRepository entiteRepository;

    @Et("Les entités suivantes sont disponibles en base")
    public void checkDataInDatabase(List<Entite> objectsToCheck) {

        objectsToCheck.forEach(entiteToCheck -> {
            assertTrue(entiteRepository.findAll(Example.of(
                            EntiteEntity.builder()
                                    .nomLot(entiteToCheck.getNomLot())
                                    .dateLot(entiteToCheck.getDateLot())
                                    .nomOrganisation(entiteToCheck.getNomOrganisation())
                                    .nomEntite(entiteToCheck.getNomEntite())
                                    .responsableEntite(entiteToCheck.getResponsableEntite())
                                    .responsableNumeriqueDurable(entiteToCheck.getResponsableNumeriqueDurable())
                                    .nbCollaborateurs(entiteToCheck.getNbCollaborateurs())
                                    .nomSourceDonnee(entiteToCheck.getNomSourceDonnee())
                                    .build()
                    ))
                    .stream().count() > 0, "L'entité de nom "+ entiteToCheck.getNomEntite() + ", nomLot: "+ entiteToCheck.getNomLot() + " n'est pas présente à l'identique en base");
        });


    }

    @DataTableType
    public Entite entite(Map<String, String> entry) {
        return Entite.builder()
                .nomLot(StringUtils.defaultString(entry.get("nomLot"), null))
                .dateLot(DataTableUtils.safeLocalDate(entry,"dateLot"))
                .nomOrganisation(StringUtils.defaultString(entry.get("nomOrganisation")))
                .nomEntite(StringUtils.defaultString(entry.get("nomEntite")))
                .responsableEntite(StringUtils.defaultString(entry.get("responsableEntite")))
                .responsableNumeriqueDurable(StringUtils.defaultString(entry.get("responsableNumeriqueDurable")))
                .nbCollaborateurs(DataTableUtils.safeInteger(entry,"nbCollaborateurs"))
                .nomSourceDonnee(StringUtils.defaultString(entry.get("nomSourceDonnee"), null))
                .build();
    }

}
