package org.mte.numecoeval.expositiondonneesentrees.steps;

import io.cucumber.java.fr.Et;
import io.cucumber.java.fr.Quand;
import io.restassured.http.ContentType;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.DemandeCalculRest;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.RapportDemandeCalculRest;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.server.CalculsApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CalculRestStepDefinitions {

    @Autowired
    ScenarioInformation scenarioInformation;

    @Quand("pour la date {string} et l'organisation {string} on soumet une demande de calcul")
    public void soumissionCalcul(String dateLot, String nomOrganisation) throws NoSuchMethodException {
        String requestUri = CalculsApi.class.getMethod("soumissionPourCalcul", DemandeCalculRest.class)
                .getAnnotation(RequestMapping.class).value()[0];

        var requestSpecification = given().contentType(ContentType.JSON);
        requestSpecification.body(DemandeCalculRest.builder()
                .nomLot(String.join("|", nomOrganisation, dateLot))
                .build()
        );

        scenarioInformation.setReponseDernierAppelAPI(requestSpecification
                .post(requestUri)
                .thenReturn()
        );
    }

    @Quand("pour le lot {string} on soumet une demande de calcul")
    public void soumissionCalcul(String nomLot) throws NoSuchMethodException {
        String requestUri = CalculsApi.class.getMethod("soumissionPourCalcul", DemandeCalculRest.class)
                .getAnnotation(RequestMapping.class).value()[0];

        var requestSpecification = given().contentType(ContentType.JSON);
        requestSpecification.body(DemandeCalculRest.builder()
                .nomLot(nomLot)
                .build()
        );

        scenarioInformation.setReponseDernierAppelAPI(requestSpecification
                .post(requestUri)
                .thenReturn()
        );
    }

    @Quand("pour la date {string} et l'organisation {string} on soumet un rejeu de calcul")
    public void rejeuCalcul(String dateLot, String nomOrganisation) throws NoSuchMethodException {
        String requestUri = CalculsApi.class.getMethod("rejeuCalcul", DemandeCalculRest.class)
                .getAnnotation(RequestMapping.class).value()[0];

        var requestSpecification = given().contentType(ContentType.JSON);
        requestSpecification.body(DemandeCalculRest.builder()
                .nomLot(String.join("|", nomOrganisation, dateLot))
                .build()
        );

        scenarioInformation.setReponseDernierAppelAPI(requestSpecification
                .post(requestUri)
                .thenReturn());
    }

    @Quand("pour le lot {string} on soumet un rejeu de calcul")
    public void rejeuCalcul(String nomLot) throws NoSuchMethodException {
        String requestUri = CalculsApi.class.getMethod("rejeuCalcul", DemandeCalculRest.class)
                .getAnnotation(RequestMapping.class).value()[0];

        var requestSpecification = given().contentType(ContentType.JSON);
        requestSpecification.body(DemandeCalculRest.builder()
                .nomLot(nomLot)
                .build()
        );

        scenarioInformation.setReponseDernierAppelAPI(requestSpecification
                .post(requestUri)
                .thenReturn());
    }

    @Et("la réponse de l'API de soumission est {int} avec {int} data centers, {int} équipement physiques, {int} équipements virtuels, {int} applications et {int} éléments de messagerie")
    public void checkReponseSoumissionCalcul(int codeStatut, int nbrDataCenter, int nbrEquipementPhysique, int nbrEquipementVirtuel, int nbrApplication, int nbrMessagerie) {
        assertNotNull(scenarioInformation.getReponseDernierAppelAPI(), "La dernière réponse d'API ne peut être null");
        assertEquals(codeStatut, scenarioInformation.getReponseDernierAppelAPI().getStatusCode());

        var rapportDemandeCalcul = scenarioInformation.getReponseDernierAppelAPI().as(RapportDemandeCalculRest.class);
        assertEquals(nbrDataCenter, rapportDemandeCalcul.getNbrDataCenter());
        assertEquals(nbrEquipementPhysique, rapportDemandeCalcul.getNbrEquipementPhysique());
        assertEquals(nbrEquipementVirtuel, rapportDemandeCalcul.getNbrEquipementVirtuel());
        assertEquals(nbrApplication, rapportDemandeCalcul.getNbrApplication());
        assertEquals(nbrMessagerie, rapportDemandeCalcul.getNbrMessagerie());
    }
}
