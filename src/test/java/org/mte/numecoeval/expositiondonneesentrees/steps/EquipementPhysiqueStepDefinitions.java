package org.mte.numecoeval.expositiondonneesentrees.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.DataTableType;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Et;
import org.apache.commons.lang3.StringUtils;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.EquipementPhysiqueEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.EquipementPhysiqueRepository;
import org.mte.numecoeval.expositiondonneesentrees.test.DataTableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mte.numecoeval.expositiondonneesentrees.steps.ExpositionRestStepDefinitions.FORMATTER;
import static org.mte.numecoeval.expositiondonneesentrees.steps.ExpositionRestStepDefinitions.FRENCH_DATE_FORMATTER;

public class EquipementPhysiqueStepDefinitions {

    @Autowired
    EquipementPhysiqueRepository equipementPhysiqueRepository;

    @Autowired
    ScenarioInformation scenarioInformation;

    @Alors("Les données chargées contiennent {int} objets equipements")
    public void checkCountEquipement(int nbrTotalEquipement) {
        // When
        scenarioInformation.checkScenarionInformation();
        assertEquals(nbrTotalEquipement,
                equipementPhysiqueRepository.findAll()
                        .stream()
                        .filter(equipementPhysique ->
                                scenarioInformation.getDateLot().equals(equipementPhysique.getDateLot())
                                        && scenarioInformation.getNomOrganisation().equals(equipementPhysique.getNomOrganisation()))
                        .count()
        );
    }

    @Et("Il existe {string} {string} acheté le {string}, utilisé par {string} en {string} avec une consommation de data annuelle de {string} Go sur le réseau")
    public EquipementPhysiqueEntity checkEquipement(String quantite, String equipement, String dateAchat, String utilisateur, String paysDUtilisation, String goTelecharge) {

        var equipements = equipementPhysiqueRepository.findAll();

        var equipementMatch = equipements
                .stream()
                .filter(equipementPhysique ->
                        scenarioInformation.getDateLot().equals(equipementPhysique.getDateLot())
                                && scenarioInformation.getNomOrganisation().equals(equipementPhysique.getNomOrganisation())
                                && Double.valueOf(quantite).equals(equipementPhysique.getQuantite())
                                && StringUtils.equals(equipement, StringUtils.defaultString(equipementPhysique.getNomEquipementPhysique()))
                                && StringUtils.equals(utilisateur, StringUtils.defaultString(equipementPhysique.getUtilisateur()))
                                && StringUtils.equals(paysDUtilisation, StringUtils.defaultString(equipementPhysique.getPaysDUtilisation()))
                                && ((equipementPhysique.getDateAchat() == null && StringUtils.isBlank(dateAchat)) || LocalDate.parse(dateAchat, FORMATTER).equals(equipementPhysique.getDateAchat()))
                                && Float.valueOf(goTelecharge).equals(equipementPhysique.getGoTelecharge())
                ).findAny();
        assertTrue(equipementMatch.isPresent());
        var equipementPhysique = equipementMatch.get();
        assertEquals(Double.valueOf(quantite), equipementPhysique.getQuantite());
        assertEquals(equipement, equipementPhysique.getNomEquipementPhysique());
        assertEquals(utilisateur, equipementPhysique.getUtilisateur());
        assertEquals(paysDUtilisation, equipementPhysique.getPaysDUtilisation());
        assertEquals(LocalDate.parse(dateAchat, FORMATTER), equipementPhysique.getDateAchat());
        assertEquals(Float.valueOf(goTelecharge), equipementPhysique.getGoTelecharge());
        return equipementMatch.get();
    }

    @Et("Il existe {string} {string} acheté le {string}, utilisé par {string} en {string} avec une consommation de data annuelle de {string} Go sur le réseau utilisé {string} jours par an")
    public EquipementPhysiqueEntity checkEquipementAvecNbrJourUtiliseParAn(String quantite, String equipement, String dateAchat, String utilisateur, String paysDUtilisation, String goTelecharge, String nbJourUtiliseAn) {
        var equipements = checkEquipement(quantite, equipement, dateAchat, utilisateur, paysDUtilisation, goTelecharge);

        assertNotNull(equipements);
        assertEquals(Double.valueOf(nbJourUtiliseAn), equipements.getNbJourUtiliseAn());
        return equipements;
    }

    @Et("Il existe {string} {string} acheté le {string}, utilisé par {string} en {string} avec une consommation de data annuelle de {string} Go sur le réseau utilisé {string} jours par an lié au data center {string}")
    public void checkEquipementWithDataCenter(String quantite, String equipement, String dateAchat, String utilisateur, String paysDUtilisation, String goTelecharge, String nbJourUtiliseAn, String nomCourtDataCenter) {
        EquipementPhysiqueEntity equipementPhysique = checkEquipementAvecNbrJourUtiliseParAn(quantite, equipement, dateAchat, utilisateur, paysDUtilisation, goTelecharge, nbJourUtiliseAn);

        assertNotNull(equipementPhysique);
        assertEquals(nomCourtDataCenter, equipementPhysique.getNomCourtDatacenter());
    }

    @Et("Les références d'équipements par défaut sont les suivantes")
    public void checkRefEquipementParDefaut(DataTable dataTable) {
        var equipementPhysiquesSauvegardes = equipementPhysiqueRepository.findAll()
                .stream()
                .filter(equipementPhysique ->
                        scenarioInformation.getDateLot().equals(equipementPhysique.getDateLot())
                                && scenarioInformation.getNomOrganisation().equals(equipementPhysique.getNomOrganisation()))
                .toList();

        assertNotNull(equipementPhysiquesSauvegardes, "Les équipements physiques persistés ne peuvent être null");
        assertFalse(equipementPhysiquesSauvegardes.isEmpty(), "La liste d'équipements physiques envoyés ne peut être vide");

        for(var dataTableRow : dataTable.asMaps() ) {
            String nomEquipementPhysiqueAttendu = dataTableRow.get("nomEquipementPhysique");
            var equipementPhysiqueAttendu = equipementPhysiquesSauvegardes.stream().filter(dto -> nomEquipementPhysiqueAttendu.equals(dto.getNomEquipementPhysique())).findFirst().orElse(null);
            assertNotNull(equipementPhysiqueAttendu, "L'équipement de nom %s est introuvable".formatted(nomEquipementPhysiqueAttendu));
            assertEquals(dataTableRow.get("refEquipementParDefaut"), equipementPhysiqueAttendu.getRefEquipementParDefaut(), "La référence par défaut de l'équipement %s ne correspond pas".formatted(nomEquipementPhysiqueAttendu));
        }

    }

    @Et("Les références d'équipements retenus sont les suivantes")
    public void checkRefEquipementRetenu(DataTable dataTable) {
        var equipementPhysiquesSauvegardes = equipementPhysiqueRepository.findAll()
                .stream()
                .filter(equipementPhysique ->
                        scenarioInformation.getDateLot().equals(equipementPhysique.getDateLot())
                                && scenarioInformation.getNomOrganisation().equals(equipementPhysique.getNomOrganisation()))
                .toList();
        assertNotNull(equipementPhysiquesSauvegardes, "Les équipements physiques sauvegardés ne peuvent être null");

        assertFalse(equipementPhysiquesSauvegardes.isEmpty(), "La liste d'équipements physiques sauvegardés ne peut être vide");

        for(var dataTableRow : dataTable.asMaps() ) {
            String nomEquipementPhysiqueAttendu = dataTableRow.get("nomEquipementPhysique");
            var equipementPhysiqueAttendu = equipementPhysiquesSauvegardes.stream().filter(dto -> nomEquipementPhysiqueAttendu.equals(dto.getNomEquipementPhysique())).findFirst().orElse(null);
            assertNotNull(equipementPhysiqueAttendu, "L'équipement de nom %s est introuvable".formatted(nomEquipementPhysiqueAttendu));
            assertEquals(dataTableRow.get("refEquipementCible"), equipementPhysiqueAttendu.getRefEquipementRetenu(), "La référence d'équipement retenue de l'équipement %s ne correspond pas".formatted(nomEquipementPhysiqueAttendu));
        }

    }

    @Alors("Le statut de tous les objets EquipementPhysique sauvegardés pour la date {string} et l'organisation {string} est {string}")
    public void checkStatutEquipementPhysiqueWithDateLotAndNomOrganisation(String dateLot, String nomOrganisation, String statutAttendu) {
        var dateToSearch = LocalDate.parse(dateLot, FRENCH_DATE_FORMATTER);
        var defaultText = "pour la date " +dateToSearch + " et l'organisation " + nomOrganisation;

        assertTrue(equipementPhysiqueRepository.findAll(Example.of(
                        EquipementPhysiqueEntity.builder()
                                .dateLot(dateToSearch)
                                .nomOrganisation(nomOrganisation)
                                .build()
                ))
                .stream().allMatch(dto -> statutAttendu.equals(dto.getStatutTraitement())), "Tous les équipements physiques "+ defaultText + " n'ont pas le bon statut : " + statutAttendu);
    }

    @Alors("Le statut de tous les objets EquipementPhysique sauvegardés pour le lot {string} est {string}")
    public void checkStatutEquipementPhysiqueWithNomLot(String nomLot, String statutAttendu) {

        assertTrue(equipementPhysiqueRepository.findAll(Example.of(
                        EquipementPhysiqueEntity.builder()
                                .nomLot(nomLot)
                                .build()
                ))
                .stream().allMatch(dto -> statutAttendu.equals(dto.getStatutTraitement())), "Tous les équipements physiques pour le lot "+ nomLot + " n'ont pas le bon statut : " + statutAttendu);
    }

    @Et("Les équipements physiques suivants sont disponibles en base")
    public void checkDataInDatabase(List<EquipementPhysiqueEntity> objectsToCheck) {

        objectsToCheck.forEach(entiteToCheck -> {
            assertTrue((long) equipementPhysiqueRepository.findAll(Example.of(
                            entiteToCheck
                    ))
                    .size() > 0, "L'équipement physique de nom "+ entiteToCheck.getNomEquipementPhysique() + ", nomLot: "+ entiteToCheck.getNomLot() + " n'est pas présent à l'identique en base");
        });


    }

    @DataTableType
    public EquipementPhysiqueEntity equipementPhysique(Map<String, String> entry) {
        return EquipementPhysiqueEntity.builder()
                // Général
                .nomOrganisation(DataTableUtils.safeString(entry, "nomOrganisation"))
                .nomSourceDonnee(DataTableUtils.safeString(entry, "nomSourceDonnee"))
                .nomLot(DataTableUtils.safeString(entry, "nomLot"))
                .dateLot(DataTableUtils.safeLocalDate(entry, "dateLot"))
                .statutTraitement(DataTableUtils.safeString(entry, "statutTraitement"))
                .nomEntite(DataTableUtils.safeString(entry, "nomEntite"))
                // Spécifique
                .nomEquipementPhysique(DataTableUtils.safeString(entry, "nomEquipementPhysique"))
                .type(DataTableUtils.safeString(entry, "type"))
                .modele(DataTableUtils.safeString(entry, "modele"))
                .statut(DataTableUtils.safeString(entry, "statut"))
                .paysDUtilisation(DataTableUtils.safeString(entry, "paysDUtilisation"))
                .utilisateur(DataTableUtils.safeString(entry, "utilisateur"))
                .dateAchat(DataTableUtils.safeLocalDate(entry, "dateAchat"))
                .dateRetrait(DataTableUtils.safeLocalDate(entry, "dateRetrait"))
                .nbCoeur(DataTableUtils.safeString(entry, "nbCoeur"))
                .nomCourtDatacenter(DataTableUtils.safeString(entry, "nomCourtDatacenter"))
                .nbJourUtiliseAn(DataTableUtils.safeDouble(entry,"nbJourUtiliseAn"))
                .goTelecharge(DataTableUtils.safeFloat(entry,"goTelecharge"))
                .consoElecAnnuelle(DataTableUtils.safeDouble(entry,"consoElecAnnuelle"))
                .refEquipementParDefaut(DataTableUtils.safeString(entry, "refEquipementParDefaut"))
                .refEquipementRetenu(DataTableUtils.safeString(entry, "refEquipementRetenu"))
                .quantite(DataTableUtils.safeDouble(entry,"quantite"))
                .serveur(DataTableUtils.safeBoolean(entry,"serveur"))
                // Spécifique Moteur
                .nbEquipementsVirtuels(DataTableUtils.safeInteger(entry,"nbEquipementsVirtuels"))
                .nbTotalVCPU(DataTableUtils.safeInteger(entry,"nbTotalVCPU"))
                .build();
    }

}
