package org.mte.numecoeval.expositiondonneesentrees.steps;

import io.cucumber.java.DataTableType;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Et;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.EquipementVirtuelEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.EquipementVirtuelRepository;
import org.mte.numecoeval.expositiondonneesentrees.test.DataTableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mte.numecoeval.expositiondonneesentrees.steps.ExpositionRestStepDefinitions.FORMATTER;
import static org.mte.numecoeval.expositiondonneesentrees.steps.ExpositionRestStepDefinitions.FRENCH_DATE_FORMATTER;

public class EquipementVirtuelStepDefinitions {

    @Autowired
    EquipementVirtuelRepository equipementVirtuelRepository;

    @Autowired
    ScenarioInformation scenarioInformation;

    @Alors("Les données chargées contiennent {int} objets équipements virtuels")
    public void checkCountEquipementVirtuel(int nbrEquipementVirtuel) {
        // When
        scenarioInformation.checkScenarionInformation();
        assertEquals(nbrEquipementVirtuel,
                equipementVirtuelRepository.findAll()
                        .stream()
                        .filter(entity ->
                                scenarioInformation.getDateLot().equals(entity.getDateLot())
                                        && scenarioInformation.getNomOrganisation().equals(entity.getNomOrganisation())
                        )
                        .count(),
                "Il n'y a pas le même nombre d'équipement virtuels"
        );
    }

    @Alors("Les données chargées pour la date de lot {string} et le nom d'organisation {string} contiennent {int} objets équipements virtuels")
    public void checkCountEquipementVirtuel(String dateLotString, String nomOrganisation, int nbrEquipementVirtuel) {
        // When
        assertEquals(nbrEquipementVirtuel,
                equipementVirtuelRepository.findAll()
                        .stream()
                        .filter(entity ->
                                LocalDate.parse(dateLotString, FORMATTER).equals(entity.getDateLot())
                                        && nomOrganisation.equals(entity.getNomOrganisation())
                        )
                        .count(),
                "Il n'y a pas le bon nombre d'équipement virtuel pour la date de lot " + dateLotString + " et l'organisation " + nomOrganisation
        );
    }

    @Et("Il existe un équipement virtuel {string} associé au serveur {string} avec un {int} vCPU et rattaché au cluster {string}")
    public void checkEquipementVirtuel(String nomEquipementVirtuel, String nomEquipementPhysique, Integer vCPU, String cluster) {
        var matchingEntities = equipementVirtuelRepository.findAll().stream()
                .filter(equipementVirtuel ->
                        scenarioInformation.getDateLot().equals(equipementVirtuel.getDateLot())
                                && scenarioInformation.getNomOrganisation().equals(equipementVirtuel.getNomOrganisation())
                                && nomEquipementVirtuel.equals(equipementVirtuel.getNomEquipementVirtuel())
                                && nomEquipementPhysique.equals(equipementVirtuel.getNomEquipementPhysique())
                                && vCPU.equals(equipementVirtuel.getVCPU())
                                && cluster.equals(equipementVirtuel.getCluster())
                )
                .toList();

        assertFalse(matchingEntities.isEmpty());
        assertEquals(1, matchingEntities.size());
    }

    @Et("Pour la date de lot {string} et le nom d'organisation {string}, il existe un équipement virtuel {string} associé au serveur {string} avec un {int} vCPU et rattaché au cluster {string}")
    public void checkEquipementVirtuel(String dateLot, String nomOrganisation, String nomEquipementVirtuel, String nomEquipementPhysique, Integer vCPU, String cluster) {
        var matchingEntities = equipementVirtuelRepository.findAll(
                Example.of(
                        EquipementVirtuelEntity.builder()
                                .dateLot(LocalDate.parse(dateLot))
                                .nomOrganisation(nomOrganisation)
                                .nomEquipementVirtuel(nomEquipementVirtuel)
                                .nomEquipementPhysique(nomEquipementPhysique)
                                .vCPU(vCPU)
                                .cluster(cluster)
                                .build()
                )
        );

        assertFalse(matchingEntities.isEmpty());
        assertEquals(1, matchingEntities.size());
    }

    @Alors("Le statut de tous les objets EquipementVirtuel sauvegardés pour la date {string} et l'organisation {string} est {string}")
    public void checkStatutEquipementVirtuelWithDateLotAndNomOrganisation(String dateLot, String nomOrganisation, String statutAttendu) {
        var dateToSearch = LocalDate.parse(dateLot, FRENCH_DATE_FORMATTER);
        var defaultText = "pour la date " +dateToSearch + " et l'organisation " + nomOrganisation;

        assertTrue(equipementVirtuelRepository.findAll(Example.of(
                        EquipementVirtuelEntity.builder()
                                .dateLot(dateToSearch)
                                .nomOrganisation(nomOrganisation)
                                .build()
                ))
                .stream().allMatch(dto -> statutAttendu.equals(dto.getStatutTraitement())), "Tous les équipements virtuelles "+ defaultText + " n'ont pas le bon statut : " + statutAttendu);
    }

    @Alors("Le statut de tous les objets EquipementVirtuel sauvegardés pour le lot {string} est {string}")
    public void checkStatutEquipementVirtuelWithNomLot(String nomLot, String statutAttendu) {
        assertTrue(equipementVirtuelRepository.findAll(Example.of(
                        EquipementVirtuelEntity.builder()
                                .nomLot(nomLot)
                                .build()
                ))
                .stream().allMatch(dto -> statutAttendu.equals(dto.getStatutTraitement())), "Tous les équipements virtuelles du lot "+ nomLot + " n'ont pas le bon statut : " + statutAttendu);
    }

    @Et("Les équipements virtuels suivants sont disponibles en base")
    public void checkDataInDatabase(List<EquipementVirtuelEntity> objectsToCheck) {

        objectsToCheck.forEach(entiteToCheck -> {
            assertTrue((long) equipementVirtuelRepository.findAll(Example.of(
                            entiteToCheck
                    ))
                    .size() > 0, "L'équipement virtuel de nom "+ entiteToCheck.getNomEquipementVirtuel() + ", equipement physique: "+ entiteToCheck.getNomEquipementPhysique() + ", nomLot: "+ entiteToCheck.getNomLot() + " n'est pas présent à l'identique en base");
        });


    }

    @DataTableType
    public EquipementVirtuelEntity equipementVirtuel(Map<String, String> entry) {
        return EquipementVirtuelEntity.builder()
                // Général
                .nomOrganisation(DataTableUtils.safeString(entry, "nomOrganisation"))
                .nomSourceDonnee(DataTableUtils.safeString(entry, "nomSourceDonnee"))
                .nomLot(DataTableUtils.safeString(entry, "nomLot"))
                .dateLot(DataTableUtils.safeLocalDate(entry, "dateLot"))
                .nomEntite(DataTableUtils.safeString(entry, "nomEntite"))
                .statutTraitement(DataTableUtils.safeString(entry, "statutTraitement"))
                // Spécifique
                .nomEquipementVirtuel(DataTableUtils.safeString(entry, "nomEquipementVirtuel"))
                .vCPU(DataTableUtils.safeInteger(entry, "vCPU"))
                .cluster(DataTableUtils.safeString(entry, "cluster"))
                .consoElecAnnuelle(DataTableUtils.safeDouble(entry, "consoElecAnnuelle"))
                .typeEqv(DataTableUtils.safeString(entry, "typeEqv"))
                .capaciteStockage(DataTableUtils.safeDouble(entry, "capaciteStockage"))
                .cleRepartition(DataTableUtils.safeDouble(entry, "cleRepartition"))
                .nomEquipementPhysique(DataTableUtils.safeString(entry, "nomEquipementPhysique"))
                .nomSourceDonneeEquipementPhysique(DataTableUtils.safeString(entry, "nomSourceDonneeEquipementPhysique"))
                // Spécifique Moteur
                .nbApplications(DataTableUtils.safeInteger(entry,"nbApplications"))
                .build();
    }

}
