package org.mte.numecoeval.expositiondonneesentrees.pojo;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.meanbean.test.BeanTester;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DataCenter;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DonneesEntree;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementPhysique;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.RapportImport;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.ResultatImport;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.UUID;

class PojoTest {

    private BeanTester tester;

    @BeforeEach
    public void init() {
        tester = new BeanTester();
        tester.setIterations(1);
        tester.getFactoryCollection().addFactory(LocalDate.class, LocalDate::now);
        tester.getFactoryCollection().addFactory(LocalDateTime.class, LocalDateTime::now);
        tester.getFactoryCollection().addFactory(OffsetDateTime.class, OffsetDateTime::now);
        tester.getFactoryCollection().addFactory(UUID.class, UUID::randomUUID);
    }

    @ParameterizedTest
    @ValueSource(classes = {
            EquipementPhysique.class,
            DataCenter.class,
            DonneesEntree.class,
            RapportImport.class,
            ResultatImport.class,
    })
    @DisplayName("Tests des Getter/Setter des pojos")
    void getterAndSetterTest(Class classToTest) {
        tester.testBean(classToTest);
    }

    @ParameterizedTest
    @ValueSource(classes = {
            EquipementPhysique.class,
            DataCenter.class,
            DonneesEntree.class,
            RapportImport.class,
            ResultatImport.class,
    })
    @DisplayName("Tests des equals/hashCode des pojos")
    void equalsAndHashCodeTest(Class classToTest) {
        EqualsVerifier.simple().forClass(classToTest).verify();
    }

}
