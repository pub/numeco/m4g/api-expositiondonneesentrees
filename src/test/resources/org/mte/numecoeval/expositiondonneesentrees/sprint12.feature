#language: fr
#encoding: utf-8
Fonctionnalité: Fonctionnalités et anomalies du Sprint 12.
  Nouvelles Features:
  - Gestion des sources de données dans les fichiers CSV
  - Gestion du nom de Lot à l'import
  - Gestion du nom de Lot à la soumission de calcul et au rejeu

  Scénario: Feature - Taiga#1014/Taiga#937 - Ajout de la source de données et du nom de lot
    Soit Les correspondances d'équipements suivantes
      | modeleEquipementSource | refEquipementCible |
      | Serveur1               | serveurA           |
      | Serveur2               | serveurA           |
      | Serveur3               | serveurA           |
    Quand pour le lot "TEST_4-01-04-2023", la date "01-04-2023" et l'organisation "TEST_4" les fichiers dataCenters: "csv/sprint12/dataCenters.csv", equipementPhysique: "csv/sprint12/equipementPhysique.csv", equipementVirtuel: "csv/sprint12/equipementVirtuel.csv", applications: "csv/sprint12/application.csv", messagerie: "csv/sprint12/messagerie.csv", entités: "csv/sprint12/entites.csv" sont importés
    Alors La réponse de l'API est 200
    Et Les entités suivantes sont disponibles en base
      | nomLot            | nomSourceDonnee | nomOrganisation | dateLot    | nomEntite | nbCollaborateurs | responsableEntite | responsableNumeriqueDurable |
      | TEST_4-01-04-2023 | SOURCE_E        | TEST_4          | 2023-04-01 | TEST_1    | 10               | Kartik Wolfram    | Pramod Venkat               |
      | TEST_4-01-04-2023 | SOURCE_E        | TEST_4          | 2023-04-01 | TEST_2    | 100              | Hisako Dobrilo    | Felipa Zoè                  |
      | TEST_4-01-04-2023 | SOURCE_E        | TEST_4          | 2023-04-01 | TEST_3    | 100              | Selena Komang     | Selena Komang               |
      | TEST_4-01-04-2023 | SOURCE_E        | TEST_4          | 2023-04-01 | TEST_4    | 200              | Brigit Kara       | Managold Celestine          |
    Et Les équipements physiques suivants sont disponibles en base
    # Ce tableau peut contenir toutes les colonnes de l'entité EquipementPhysique + Colonnes de la table correspondante au format CamelCase.
      | nomLot            | nomOrganisation | dateLot    | nomSourceDonnee | nomEntite | quantite | nomEquipementPhysique | type    | statut | paysDUtilisation | nomCourtDatacenter | nbJourUtiliseAn | serveur |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_A        | Test_1    | 1        | ServeurCalcul_1       | Serveur | actif  | France           | DC1                | 365             | true    |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_A        | Test_1    | 1        | ServeurCalcul_2       | Serveur | actif  | France           | DC1                | 365             | true    |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_A        | Test_2    | 1        | ServeurCalcul_1       | Serveur | actif  | France           | DC1                | 365             | true    |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_A        | Test_2    | 1        | ServeurCalcul_1       | Serveur | actif  | France           | DC2                | 365             | true    |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_A        | Test_2    | 1        | ServeurCalcul_1       | Serveur | actif  | France           | DC2                | 365             | true    |
    Et Les équipements virtuels suivants sont disponibles en base
    # Ce tableau peut contenir toutes les colonnes de l'entité EquipementVirtuel + Colonnes de la table correspondante au format CamelCase.
      | nomLot            | nomOrganisation | dateLot    | nomSourceDonnee | nomEntite | nomEquipementVirtuel | nomEquipementPhysique | nomSourceDonneeEquipementPhysique | vCPU | cluster |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_B        | Test_1    | eqv-01               | ServeurCalcul_1       | SOURCE_A                          | 2    | DC1     |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_B        | Test_1    | eqv-02               | ServeurCalcul_2       | SOURCE_A                          | 2    | DC1     |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_B        | Test_2    | eqv-01               | ServeurCalcul_1       | SOURCE_A                          | 4    | DC1     |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_B        | Test_2    | eqv-02               | ServeurCalcul_1       | SOURCE_A                          | 4    | DC1     |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_B        | Test_2    | eqv-02               | ServeurCalcul_1       | SOURCE_A                          | 4    | DC2     |
    Et Les instances d'applications suivantes sont disponibles en base
    # Ce tableau peut contenir toutes les colonnes de l'entité Application + Colonnes de la table correspondante au format CamelCase.
      | nomLot            | nomOrganisation | dateLot    | nomSourceDonnee | nomEntite | nomApplication | typeEnvironnement | domaine  | sousDomaine | nomEquipementVirtuel | nomEquipementPhysique | nomSourceDonneeEquipementVirtuel |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_C        | Test_1    | app1           | Prod              | domaine1 | java        | eqv-01               | ServeurCalcul_1       | SOURCE_B                         |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_C        | Test_1    | app2           | Prod              | domaine2 | java        | eqv-02               | ServeurCalcul_2       | SOURCE_B                         |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_C        | Test_2    | app3           | Prod              | domaine3 | java        | eqv-01               | ServeurCalcul_1       | SOURCE_B                         |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_C        | Test_2    | app4           | Prod              | domaine4 | java        | eqv-02               | ServeurCalcul_1       | SOURCE_B                         |
      | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_C        | Test_2    | app5           | Prod              | domaine5 | java        | eqv-02               | ServeurCalcul_1       | SOURCE_B                         |
    Et Les éléments de messagerie suivants sont disponibles en base
    # Ce tableau peut contenir toutes les colonnes de l'entité Messagerie + Colonnes de la table correspondante au format CamelCase.
      | statutTraitement | nomLot            | nomOrganisation | dateLot    | nomSourceDonnee | nomEntite | moisAnnee | nombreMailEmis | nombreMailEmisXDestinataires | volumeTotalMailEmis |
      | EN_ATTENTE       | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_F        | Test_1    | 202201    | 100            | 20                           | 160                 |
      | EN_ATTENTE       | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_F        | Test_1    | 202202    | 200            | 20                           | 160                 |
      | EN_ATTENTE       | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_F        | Test_1    | 202208    | 800            | 20                           | 160                 |
      | EN_ATTENTE       | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_F        | Test_1    | 202209    | 1000           | 20                           | 160                 |
      | EN_ATTENTE       | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_F        | Test_1    | 202210    | 2000           | 20                           | 160                 |
      | EN_ATTENTE       | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_F        | Test_1    | 202211    | 5000           | 20                           | 160                 |
      | EN_ATTENTE       | TEST_4-01-04-2023 | TEST_4          | 2023-04-01 | SOURCE_F        | Test_1    | 202212    | 1000           | 20                           | 160                 |

  Scénario: Reprise du scénario du Sprint 11 avec le nom de lot - Après une soumission de calcul, le statut de traitement passe à A_INGERER
    Soit Les correspondances d'équipements suivantes
      | modeleEquipementSource | refEquipementCible |
      | Serveur1               | serveurA           |
      | Serveur1               | serveurA           |
      | Serveur1               | serveurA           |
    Quand pour le lot "TEST_2-01-05-2023", la date "01-05-2023" et l'organisation "TEST_2" les fichiers dataCenters: "csv/sprint8/ordre-aleatoire/dataCenters.csv", equipementPhysique: "csv/sprint8/ordre-aleatoire/equipementPhysique.csv", equipementVirtuel: "csv/sprint8/ordre-aleatoire/equipementVirtuel.csv", applications: "csv/sprint8/ordre-aleatoire/application.csv", messagerie: "csv/sprint8/ordre-aleatoire/messagerie.csv", entités: "csv/sprint12/entites.csv" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 3 objets equipements
    Et Les données chargées contiennent 1 objets data centers
    Et Les données chargées contiennent 3 objets équipements virtuels
    Et Les données chargées contiennent 12 objets application
    Et Le statut de tous les objets DonneesEntrées sauvegardés pour la date "01-05-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets DonneesEntrées sauvegardés pour le lot "TEST_2-01-05-2023" est "EN_ATTENTE"
    Et Le statut de tous les objets DataCenter sauvegardés pour la date "01-05-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets DataCenter sauvegardés pour le lot "TEST_2-01-05-2023" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementPhysique sauvegardés pour la date "01-05-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementPhysique sauvegardés pour le lot "TEST_2-01-05-2023" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementVirtuel sauvegardés pour la date "01-05-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementVirtuel sauvegardés pour le lot "TEST_2-01-05-2023" est "EN_ATTENTE"
    Et Le statut de tous les objets Application sauvegardés pour la date "01-05-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets Application sauvegardés pour le lot "TEST_2-01-05-2023" est "EN_ATTENTE"
    Et Le statut de tous les objets Messagerie sauvegardés pour la date "01-05-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets Messagerie sauvegardés pour le lot "TEST_2-01-05-2023" est "EN_ATTENTE"
    Quand pour le lot "TEST_2-01-05-2023" on soumet une demande de calcul
    Alors La réponse de l'API est 200
    Et Le statut de tous les objets DonneesEntrées sauvegardés pour la date "01-05-2023" et l'organisation "TEST_2" est "A_INGERER"
    Et Le statut de tous les objets DonneesEntrées sauvegardés pour le lot "TEST_2-01-05-2023" est "A_INGERER"
    Et Le statut de tous les objets DataCenter sauvegardés pour la date "01-05-2023" et l'organisation "TEST_2" est "A_INGERER"
    Et Le statut de tous les objets DataCenter sauvegardés pour le lot "TEST_2-01-05-2023" est "A_INGERER"
    Et Le statut de tous les objets EquipementPhysique sauvegardés pour la date "01-05-2023" et l'organisation "TEST_2" est "A_INGERER"
    Et Le statut de tous les objets EquipementPhysique sauvegardés pour le lot "TEST_2-01-05-2023" est "A_INGERER"
    Et Le statut de tous les objets EquipementVirtuel sauvegardés pour la date "01-05-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementVirtuel sauvegardés pour le lot "TEST_2-01-05-2023" est "EN_ATTENTE"
    Et Le statut de tous les objets Application sauvegardés pour la date "01-05-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets Application sauvegardés pour le lot "TEST_2-01-05-2023" est "EN_ATTENTE"
    Et Le statut de tous les objets Messagerie sauvegardés pour la date "01-05-2023" et l'organisation "TEST_2" est "A_INGERER"
    Et Le statut de tous les objets Messagerie sauvegardés pour le lot "TEST_2-01-05-2023" est "A_INGERER"

  Scénario: Reprise du scénario du Sprint 11 avec le nom de lot - Après un rejeu de calcul, le statut de traitement passe à A_INGERER
    Soit Les correspondances d'équipements suivantes
      | modeleEquipementSource | refEquipementCible |
      | Serveur1               | serveurA           |
      | Serveur2               | serveurA           |
      | Serveur3               | serveurA           |
    Quand pour le lot "TEST_3-01-05-2023", la date "02-05-2023" et l'organisation "TEST_3" les fichiers dataCenters: "csv/sprint8/ordre-aleatoire/dataCenters.csv", equipementPhysique: "csv/sprint8/ordre-aleatoire/equipementPhysique.csv", equipementVirtuel: "csv/sprint8/ordre-aleatoire/equipementVirtuel.csv", applications: "csv/sprint8/ordre-aleatoire/application.csv", messagerie: "csv/sprint8/ordre-aleatoire/messagerie.csv", entités: "csv/sprint12/entites.csv" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 3 objets equipements
    Et Les données chargées contiennent 1 objets data centers
    Et Les données chargées contiennent 3 objets équipements virtuels
    Et Les données chargées contiennent 12 objets application
    Et Le statut de tous les objets DonneesEntrées sauvegardés pour la date "02-05-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets DonneesEntrées sauvegardés pour le lot "TEST_3-01-05-2023" est "EN_ATTENTE"
    Et Le statut de tous les objets DataCenter sauvegardés pour la date "02-05-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets DataCenter sauvegardés pour le lot "TEST_3-01-05-2023" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementPhysique sauvegardés pour la date "02-05-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementPhysique sauvegardés pour le lot "TEST_3-01-05-2023" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementVirtuel sauvegardés pour la date "02-05-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementVirtuel sauvegardés pour le lot "TEST_3-02-05-2023" est "EN_ATTENTE"
    Et Le statut de tous les objets Application sauvegardés pour la date "02-05-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets Application sauvegardés pour le lot "TEST_3-02-05-2023" est "EN_ATTENTE"
    Et Le statut de tous les objets Messagerie sauvegardés pour la date "02-05-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets Messagerie sauvegardés pour le lot "TEST_3-02-05-2023" est "EN_ATTENTE"
    Quand pour le lot "TEST_3-01-05-2023" on soumet un rejeu de calcul
    Alors La réponse de l'API est 200
    Et la réponse de l'API de soumission est 200 avec 1 data centers, 3 équipement physiques, 3 équipements virtuels, 12 applications et 7 éléments de messagerie
    Et Le statut de tous les objets DonneesEntrées sauvegardés pour la date "02-05-2023" et l'organisation "TEST_3" est "A_INGERER"
    Et Le statut de tous les objets DonneesEntrées sauvegardés pour le lot "TEST_3-02-05-2023" est "A_INGERER"
    Et Le statut de tous les objets DataCenter sauvegardés pour la date "02-05-2023" et l'organisation "TEST_3" est "A_INGERER"
    Et Le statut de tous les objets DataCenter sauvegardés pour le lot "TEST_3-02-05-2023" est "A_INGERER"
    Et Le statut de tous les objets EquipementPhysique sauvegardés pour la date "02-05-2023" et l'organisation "TEST_3" est "A_INGERER"
    Et Le statut de tous les objets EquipementPhysique sauvegardés pour le lot "TEST_3-02-05-2023" est "A_INGERER"
    Et Le statut de tous les objets EquipementVirtuel sauvegardés pour la date "02-05-2023" et l'organisation "TEST_3" est "A_INGERER"
    Et Le statut de tous les objets EquipementVirtuel sauvegardés pour le lot "TEST_3-02-05-2023" est "A_INGERER"
    Et Le statut de tous les objets Application sauvegardés pour la date "02-05-2023" et l'organisation "TEST_3" est "A_INGERER"
    Et Le statut de tous les objets Application sauvegardés pour le lot "TEST_3-02-05-2023" est "A_INGERER"
    Et Le statut de tous les objets Messagerie sauvegardés pour la date "02-05-2023" et l'organisation "TEST_3" est "A_INGERER"
    Et Le statut de tous les objets Messagerie sauvegardés pour le lot "TEST_3-02-05-2023" est "A_INGERER"


  Scénario: Feature - Taiga#457 - Ajout de consoElecAnnuelle, typeEqv, capaciteStockage et cleRepartition
    Soit Les correspondances d'équipements suivantes
      | modeleEquipementSource | refEquipementCible |
      | Serveur1               | serveurA           |
      | Serveur2               | serveurA           |
      | Serveur3               | serveurA           |
    Quand pour le lot "TEST_TAIGA_457-01-04-2023", la date "01-04-2023" et l'organisation "TEST_TAIGA_457" les fichiers dataCenters: "csv/sprint12/taiga457_dataCenters.csv", equipementPhysique: "csv/sprint12/taiga457_equipementPhysique.csv", equipementVirtuel: "csv/sprint12/taiga457_equipementVirtuel.csv", applications: "inutilise.csv", messagerie: "inutilise.csv", entités: "inutilise.csv" sont importés
    Alors La réponse de l'API est 200
    Et Les équipements virtuels suivants sont disponibles en base
      | nomLot                    | nomOrganisation | dateLot    | nomSourceDonnee | nomEntite | nomEquipementVirtuel | nomEquipementPhysique | nomSourceDonneeEquipementPhysique | vCPU | cluster | consoElecAnnuelle | typeEqv   | capaciteStockage | cleRepartition |
      | TEST_TAIGA_457-01-04-2023 | TEST_TAIGA_457  | 2023-04-01 | SOURCE_B        | Test_1    | eqv-01               | ServeurCalcul_1       | SOURCE_A                          | 2    | DC1     | 100.0             | stockage  | 0.500            |                |
      | TEST_TAIGA_457-01-04-2023 | TEST_TAIGA_457  | 2023-04-01 | SOURCE_B        | Test_1    | eqv-02               | ServeurCalcul_2       | SOURCE_A                          | 2    | DC1     | 100.0             | stockage  | 0.500            |                |
      | TEST_TAIGA_457-01-04-2023 | TEST_TAIGA_457  | 2023-04-01 | SOURCE_B        | Test_2    | eqv-01               | ServeurCalcul_1       | SOURCE_A                          | 4    | DC1     | 500.0             | calcul    | 0.010            |                |
      | TEST_TAIGA_457-01-04-2023 | TEST_TAIGA_457  | 2023-04-01 | SOURCE_B        | Test_2    | eqv-02               | ServeurCalcul_1       | SOURCE_A                          | 4    | DC1     | 500.0             | calcul    | 0.010            |                |
      | TEST_TAIGA_457-01-04-2023 | TEST_TAIGA_457  | 2023-04-01 | SOURCE_B        | Test_2    | eqv-02               | ServeurCalcul_1       | SOURCE_A                          | 4    | DC2     | 0.0               | inutilise |                  | 0.0            |
      | TEST_TAIGA_457-01-04-2023 | TEST_TAIGA_457  | 2023-04-01 | SOURCE_B        | Test_2    | eqv-03               | ServeurCalcul_1       | SOURCE_A                          | 4    | DC2     | 100.0             |           |                  | 0.0            |
