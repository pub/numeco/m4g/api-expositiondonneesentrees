#language: fr
#encoding: utf-8
Fonctionnalité: Sprint 6 - Ajout de la messagerie.
  Le système NumEcoEval accepte des données aux formats CSV (import de Data Centers, d'équipements physiques, d'équipements virtuels, d'applications et de messagerie).

  Scénario: Cas Technique - Import de fichier de messagerie
    Quand pour la date "01-11-2022" et l'organisation "TECH1_SPRINT6" les fichiers dataCenters: "nonUtilise.csv", equipementPhysique: "nonUtilise.csv", equipementVirtuel: "nonUtilise.csv", applications: "nonUtilise.csv", messagerie: "csv/sprint6/messagerie_cat.csv" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 7 objets messageries
    Et Les données chargées contiennent 0 objets equipements
    Et Les données chargées contiennent 0 objets data centers
    Et Les données chargées contiennent 0 objets équipements virtuels
    Et Les données chargées contiennent 0 objets application
    Et Il existe une messagerie pour le mois de 202201 avec 100 mail émis, 20 mails émis par destinataire pour un volume total de 160 Mo
    Et Il existe une messagerie pour le mois de 202202 avec 200 mail émis, 20 mails émis par destinataire pour un volume total de 160 Mo
    Et Il existe une messagerie pour le mois de 202208 avec 800 mail émis, 20 mails émis par destinataire pour un volume total de 160 Mo
    Et Il existe une messagerie pour le mois de 202209 avec 1000 mail émis, 20 mails émis par destinataire pour un volume total de 160 Mo
    Et Il existe une messagerie pour le mois de 202210 avec 2000 mail émis, 20 mails émis par destinataire pour un volume total de 160 Mo
    Et Il existe une messagerie pour le mois de 202211 avec 5000 mail émis, 20 mails émis par destinataire pour un volume total de 160 Mo
    Et Il existe une messagerie pour le mois de 202212 avec 1000 mail émis, 20 mails émis par destinataire pour un volume total de 160 Mo

