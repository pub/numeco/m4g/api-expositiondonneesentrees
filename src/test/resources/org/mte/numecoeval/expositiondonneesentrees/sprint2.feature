#language: fr
#encoding: utf-8
Fonctionnalité: Sprint 2 - Ajout des datacenters et de l'import multiple
  Le système NumEcoEval accepte des données aux formats CSV (import de Data Centers et d'équipements physiques.

  Scénario: Multiples équipements sont correctement envoyés par CSV
    Quand pour la date "08-09-2022" et l'organisation "TEST1_SPRINT_2", les fichiers dataCenters: "csv/lot1/dataCenters.csv", equipementPhysique: "csv/lot1/equipementPhysique.csv" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 4 objets equipements
    Et Les données chargées contiennent 1 objets data centers
    Et Il existe un data center "DC_Villeperdue_01" (nom complet: "DC_Villeperdue_OpenStack_01") avec un PUE de "1.0" localisé en "France"
    Et Il existe "1" "APPLE IPHONE 11" acheté le "2020-09-01", utilisé par "Thierry" en "France" avec une consommation de data annuelle de "4" Go sur le réseau
    Et Il existe "1" "Odyssey Neo G9 49" acheté le "2020-09-01", utilisé par "Xavier" en "France" avec une consommation de data annuelle de "0" Go sur le réseau
    Et Il existe "100" "Dell 20 E2020H" acheté le "2022-09-01", utilisé par "SSG Dublin" en "Irlande" avec une consommation de data annuelle de "0" Go sur le réseau utilisé "365" jours par an
    Et Il existe "2" "Xeon E-2174G:DC_Villeperdue_01" acheté le "2018-01-01", utilisé par "Projet M4G" en "France" avec une consommation de data annuelle de "0" Go sur le réseau utilisé "365" jours par an lié au data center "DC_Villeperdue_01"

