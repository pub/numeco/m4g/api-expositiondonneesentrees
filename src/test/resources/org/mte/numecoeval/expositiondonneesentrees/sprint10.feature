#language: fr
#encoding: utf-8
Fonctionnalité: Fonctionnalités et anomalies du Sprint 10.
  Le système NumEcoEval accepte des équipements physiques sans autres fichiers (correction d'un comportement voulu).

  Scénario: Anomalie - Issue #2 - Importer un fichier d'équipement physique seul est possible
    Quand pour la date "01-02-2023" et l'organisation "ISSUE#2_TEST" les fichiers dataCenters: "", equipementPhysique: "csv/sprint9/equipementPhysique_seul.csv", equipementVirtuel: "", applications: "", messagerie: "" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 14 objets equipements
    Et Les données chargées contiennent 0 objets data centers
    Et Les données chargées contiennent 0 objets équipements virtuels
    Et Les données chargées contiennent 0 objets application

  Scénario: Feature - Taiga #862 - La refEquipementParDefaut est rempli si elle est renseignée dans le référentiel TypeEquipement
    Quand pour la date "01-02-2023" et l'organisation "TAIGA#862_SPRINT10" les fichiers dataCenters: "", equipementPhysique: "csv/sprint10/equipementPhysique_refEquipementParDefaut.csv", equipementVirtuel: "", applications: "", messagerie: "" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 13 objets equipements
    Et Les données chargées contiennent 0 objets application
    Et Les références d'équipements par défaut sont les suivantes
      | nomEquipementPhysique              | refEquipementParDefaut |
      | 2023-03-03-Serveur-99999           | serveur_par_defaut     |
      | 2023-03-03-Smartphone-99999        | smartphone_par_defaut  |
      | 2023-03-07-Laptop-10               | laptop_par_defaut      |
      | 2023-03-07-Ecran-100               | ecran_par_defaut       |
      | 2023-03-07-Telephone-707           | telephone_par_defaut   |
      | 2023-03-07-Imprimante-708          | imprimante_par_defaut  |
      | 2023-03-07-ServeurCalcul-10135     | serveur_par_defaut     |
      | 2023-03-07-ServeurStockage-10137   | serveur_par_defaut     |
      | 2023-03-07-TelephoneMobile-0       | smartphone_par_defaut  |
      | 2023-03-07-TelephoneFixe-1         | telephone_par_defaut   |
      | 2023-03-07-Switch-0                |                        |
      | 2023-03-07-OrdinateurPortable-1    | laptop_par_defaut      |
      | 2023-03-07-OrdinateurDeBureau-9941 | ordinateur_par_defaut  |

  Scénario: Feature - Taiga #319 - La référence d'équipement retenu est rempli si elle est renseignée dans le référentiel CorrespondanceRefEquipement
    Soit Les correspondances d'équipements suivantes
      | modeleEquipementSource               | refEquipementCible |
      | serveur-01                           | serveurA           |
      | smartphone-01                        | smartphoneA        |
      | smartphone-02                        | smartphoneB        |
      | laptop-24                            | laptopA-24p        |
      | laptop-27                            | laptopA-27p        |
      | professional retro landline phone-01 | telephoneFixeA     |
      | professional laser printer-01        | imprimanteA        |
      | serveur-03                           | serveurA           |
      | serveur-04                           | serveurA           |
      | switch/router-05                     | switchA            |
      | switch/router-06                     | switchA            |
      | desktop-02                           | desktopA           |
      | laptop-01                            | laptopA-24p        |
    Quand pour la date "01-02-2023" et l'organisation "TAIGA#319_SPRINT10" les fichiers dataCenters: "", equipementPhysique: "csv/sprint10/equipementPhysique_refEquipementRetenu.csv", equipementVirtuel: "", applications: "", messagerie: "" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 15 objets equipements
    Et Les données chargées contiennent 0 objets application
    Et Les références d'équipements retenus sont les suivantes
      | nomEquipementPhysique              | refEquipementCible |
      | 2023-03-03-Serveur-99999           | serveurA           |
      | 2023-03-03-Smartphone-99999        | smartphoneB        |
      | 2023-03-07-Laptop-10               | laptopA-24p        |
      | 2023-03-07-Ecran-100               | laptopA-27p        |
      | 2023-03-07-Telephone-707           | telephoneFixeA     |
      | 2023-03-07-Imprimante-708          | imprimanteA        |
      | 2023-03-07-ServeurCalcul-10135     | serveurA           |
      | 2023-03-07-ServeurStockage-10137   | serveurA           |
      | 2023-03-07-TelephoneMobile-0       | smartphoneA        |
      | 2023-03-07-TelephoneFixe-1         | telephoneFixeA     |
      | 2023-03-07-Switch-0                | switchA            |
      | 2023-03-07-Switch-1                | switchA            |
      | 2023-03-07-OrdinateurPortable-1    | laptopA-24p        |
      | 2023-03-07-OrdinateurDeBureau-9941 | desktopA           |
      | 2003-03-07-OrdinateurDeBureau-0001 |                    |


