#language: fr
#encoding: utf-8
Fonctionnalité: Sprint 8 - Ordres des colonnes sans importances,.
  Le système NumEcoEval accepte des données aux formats CSV sans importance sur l'ordre des colonnes.

  Scénario: CAT1 - Taiga #855 - Ordres des colonnes de CSV sans importances - Mêmes fichiers que le Sprint 4 et le Sprint 6 avec ordre différents.
    Quand pour la date "01-12-2023" et l'organisation "CAT1_TAIGA#855_SPRINT8" les fichiers dataCenters: "csv/sprint8/ordre-aleatoire/dataCenters.csv", equipementPhysique: "csv/sprint8/ordre-aleatoire/equipementPhysique.csv", equipementVirtuel: "csv/sprint8/ordre-aleatoire/equipementVirtuel.csv", applications: "csv/sprint8/ordre-aleatoire/application.csv", messagerie: "csv/sprint8/ordre-aleatoire/messagerie.csv" sont importés
    Alors Les données chargées contiennent 3 objets equipements
    Et Les données chargées contiennent 1 objets data centers
    Et Les données chargées contiennent 3 objets équipements virtuels
    Et Les données chargées contiennent 12 objets application
    Et La réponse de l'API est 200
    Et Il existe un data center "DC_Villeperdue_01" (nom complet: "DC_Villeperdue_OpenStack_01") avec un PUE de "1.0" localisé en "France"
    Et Il existe "1" "Serveur1 Xeon" acheté le "2020-09-01", utilisé par "SSG" en "France" avec une consommation de data annuelle de "118" Go sur le réseau
    Et Il existe "1" "Serveur2 Xeon" acheté le "2019-09-01", utilisé par "SSG" en "France" avec une consommation de data annuelle de "64" Go sur le réseau
    Et Il existe "1" "Serveur3 Xeon" acheté le "2019-09-01", utilisé par "SSG" en "France" avec une consommation de data annuelle de "20" Go sur le réseau
    Et Il existe un équipement virtuel "VM1" associé au serveur "Serveur1 Xeon" avec un 5 vCPU et rattaché au cluster "Cluster1"
    Et Il existe un équipement virtuel "VM2" associé au serveur "Serveur2 Xeon" avec un 4 vCPU et rattaché au cluster "Cluster2"
    Et Il existe un équipement virtuel "VM3" associé au serveur "Serveur3 Xeon" avec un 2 vCPU et rattaché au cluster "Cluster2"
    Et Il existe une application "M4G-api-referentiel" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-indicateurs" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-calcul" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-donneesEntrees" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-exposition" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-2" et "java"
    Et Il existe une application "M4G-superset" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-3" et "java"
    Et Il existe une application "M4G-api-referentiel" pour le type d'environnement "Dev" associé à la VM "VM2" de l'équipement "Serveur2 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-indicateurs" pour le type d'environnement "Dev" associé à la VM "VM2" de l'équipement "Serveur2 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-calcul" pour le type d'environnement "Dev" associé à la VM "VM2" de l'équipement "Serveur2 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-donneesEntrees" pour le type d'environnement "Dev" associé à la VM "VM2" de l'équipement "Serveur2 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-exposition" pour le type d'environnement "Dev" associé à la VM "VM2" de l'équipement "Serveur2 Xeon" et avec pour domaines applicatifs "datahub-m4g-2" et "java"
    Et Il existe une application "M4G-superset" pour le type d'environnement "Dev" associé à la VM "VM3" de l'équipement "Serveur3 Xeon" et avec pour domaines applicatifs "datahub-m4g-3" et "java"
