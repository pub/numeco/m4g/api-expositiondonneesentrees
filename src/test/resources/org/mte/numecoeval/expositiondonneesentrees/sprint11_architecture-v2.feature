#language: fr
#encoding: utf-8
Fonctionnalité: Fonctionnalités et anomalies du Sprint 11.
  L'architecture de NumEcoEval a été revue pour fusionner les applications:
  - api-expositiondonneesentrees: Exposition REST pour l'import de données
  - api-donneesentrees: Sauvegarde des données d'entrées
  Les scénarios suivants reprennent les scénarios des sprints précédents

  Nouvelles Features:
  - Le statut de traitement été ajouté

  Scénario: Feature - à l'ingestion, le statut de traitement est initialisé à EN_ATTENTE
    Soit Les correspondances d'équipements suivantes
      | modeleEquipementSource | refEquipementCible |
      | Serveur1               | serveurA           |
      | Serveur2               | serveurA           |
      | Serveur3               | serveurA           |
    Quand pour la date "01-03-2023" et l'organisation "TEST" les fichiers dataCenters: "csv/sprint8/ordre-aleatoire/dataCenters.csv", equipementPhysique: "csv/sprint8/ordre-aleatoire/equipementPhysique.csv", equipementVirtuel: "csv/sprint8/ordre-aleatoire/equipementVirtuel.csv", applications: "csv/sprint8/ordre-aleatoire/application.csv", messagerie: "csv/sprint8/ordre-aleatoire/messagerie.csv" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 3 objets equipements
    Et Les données chargées contiennent 1 objets data centers
    Et Les données chargées contiennent 3 objets équipements virtuels
    Et Les données chargées contiennent 12 objets application
    Et Le statut de tous les objets DonneesEntrées sauvegardés pour la date "01-03-2023" et l'organisation "TEST" est "EN_ATTENTE"
    Et Le statut de tous les objets DataCenter sauvegardés pour la date "01-03-2023" et l'organisation "TEST" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementPhysique sauvegardés pour la date "01-03-2023" et l'organisation "TEST" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementVirtuel sauvegardés pour la date "01-03-2023" et l'organisation "TEST" est "EN_ATTENTE"
    Et Le statut de tous les objets Application sauvegardés pour la date "01-03-2023" et l'organisation "TEST" est "EN_ATTENTE"
    Et Le statut de tous les objets Messagerie sauvegardés pour la date "01-03-2023" et l'organisation "TEST" est "EN_ATTENTE"

  Scénario: Feature - Après une soumission de calcul, le statut de traitement passe à A_INGERER
    Soit Les correspondances d'équipements suivantes
      | modeleEquipementSource | refEquipementCible |
      | Serveur1               | serveurA           |
      | Serveur1               | serveurA           |
      | Serveur1               | serveurA           |
    Quand pour la date "01-03-2023" et l'organisation "TEST_2" les fichiers dataCenters: "csv/sprint8/ordre-aleatoire/dataCenters.csv", equipementPhysique: "csv/sprint8/ordre-aleatoire/equipementPhysique.csv", equipementVirtuel: "csv/sprint8/ordre-aleatoire/equipementVirtuel.csv", applications: "csv/sprint8/ordre-aleatoire/application.csv", messagerie: "csv/sprint8/ordre-aleatoire/messagerie.csv" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 3 objets equipements
    Et Les données chargées contiennent 1 objets data centers
    Et Les données chargées contiennent 3 objets équipements virtuels
    Et Les données chargées contiennent 12 objets application
    Et Le statut de tous les objets DonneesEntrées sauvegardés pour la date "01-03-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets DataCenter sauvegardés pour la date "01-03-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementPhysique sauvegardés pour la date "01-03-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementVirtuel sauvegardés pour la date "01-03-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets Application sauvegardés pour la date "01-03-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets Messagerie sauvegardés pour la date "01-03-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Quand pour la date "01-03-2023" et l'organisation "TEST_2" on soumet une demande de calcul
    Alors La réponse de l'API est 200
    Et Le statut de tous les objets DonneesEntrées sauvegardés pour la date "01-03-2023" et l'organisation "TEST_2" est "A_INGERER"
    Et Le statut de tous les objets DataCenter sauvegardés pour la date "01-03-2023" et l'organisation "TEST_2" est "A_INGERER"
    Et Le statut de tous les objets EquipementPhysique sauvegardés pour la date "01-03-2023" et l'organisation "TEST_2" est "A_INGERER"
    Et Le statut de tous les objets EquipementVirtuel sauvegardés pour la date "01-03-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets Application sauvegardés pour la date "01-03-2023" et l'organisation "TEST_2" est "EN_ATTENTE"
    Et Le statut de tous les objets Messagerie sauvegardés pour la date "01-03-2023" et l'organisation "TEST_2" est "A_INGERER"

  Scénario: Feature - Après un rejeu de calcul, le statut de traitement passe à A_INGERER
    Soit Les correspondances d'équipements suivantes
      | modeleEquipementSource | refEquipementCible |
      | Serveur1               | serveurA           |
      | Serveur2               | serveurA           |
      | Serveur3               | serveurA           |
    Quand pour la date "01-03-2023" et l'organisation "TEST_3" les fichiers dataCenters: "csv/sprint8/ordre-aleatoire/dataCenters.csv", equipementPhysique: "csv/sprint8/ordre-aleatoire/equipementPhysique.csv", equipementVirtuel: "csv/sprint8/ordre-aleatoire/equipementVirtuel.csv", applications: "csv/sprint8/ordre-aleatoire/application.csv", messagerie: "csv/sprint8/ordre-aleatoire/messagerie.csv" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 3 objets equipements
    Et Les données chargées contiennent 1 objets data centers
    Et Les données chargées contiennent 3 objets équipements virtuels
    Et Les données chargées contiennent 12 objets application
    Et Le statut de tous les objets DonneesEntrées sauvegardés pour la date "01-03-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets DataCenter sauvegardés pour la date "01-03-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementPhysique sauvegardés pour la date "01-03-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementVirtuel sauvegardés pour la date "01-03-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets Application sauvegardés pour la date "01-03-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets Messagerie sauvegardés pour la date "01-03-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Quand pour la date "01-03-2023" et l'organisation "TEST_3" on soumet un rejeu de calcul
    Alors La réponse de l'API est 200
    Et la réponse de l'API de soumission est 200 avec 1 data centers, 3 équipement physiques, 3 équipements virtuels, 12 applications et 7 éléments de messagerie
    Et Le statut de tous les objets DonneesEntrées sauvegardés pour la date "01-03-2023" et l'organisation "TEST_3" est "A_INGERER"
    Et Le statut de tous les objets DataCenter sauvegardés pour la date "01-03-2023" et l'organisation "TEST_3" est "A_INGERER"
    Et Le statut de tous les objets EquipementPhysique sauvegardés pour la date "01-03-2023" et l'organisation "TEST_3" est "A_INGERER"
    Et Le statut de tous les objets EquipementVirtuel sauvegardés pour la date "01-03-2023" et l'organisation "TEST_3" est "A_INGERER"
    Et Le statut de tous les objets Application sauvegardés pour la date "01-03-2023" et l'organisation "TEST_3" est "A_INGERER"
    Et Le statut de tous les objets Messagerie sauvegardés pour la date "01-03-2023" et l'organisation "TEST_3" est "A_INGERER"

  Scénario: Feature - Taiga#922 - Il est possible de charger des données un CSV des entités
    Soit Les correspondances d'équipements suivantes
      | modeleEquipementSource | refEquipementCible |
      | Serveur1               | serveurA           |
      | Serveur2               | serveurA           |
      | Serveur3               | serveurA           |
    Quand pour le lot "TEST_4-01-04-2023", la date "01-04-2023" et l'organisation "TEST_4" les fichiers dataCenters: "csv/sprint8/ordre-aleatoire/dataCenters.csv", equipementPhysique: "csv/sprint8/ordre-aleatoire/equipementPhysique.csv", equipementVirtuel: "csv/sprint8/ordre-aleatoire/equipementVirtuel.csv", applications: "csv/sprint8/ordre-aleatoire/application.csv", messagerie: "csv/sprint8/ordre-aleatoire/messagerie.csv", entités: "csv/sprint11-v2/entites.csv" sont importés
    Alors La réponse de l'API est 200
    Et Le statut de tous les objets DonneesEntrées sauvegardés pour la date "01-04-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets DataCenter sauvegardés pour la date "01-04-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementPhysique sauvegardés pour la date "01-04-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets EquipementVirtuel sauvegardés pour la date "01-04-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets Application sauvegardés pour la date "01-04-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Le statut de tous les objets Messagerie sauvegardés pour la date "01-04-2023" et l'organisation "TEST_3" est "EN_ATTENTE"
    Et Les entités suivantes sont disponibles en base
      | nomOrganisation | dateLot    | nomEntite | nbCollaborateurs | responsableEntite | responsableNumeriqueDurable |
      | TEST_4          | 2023-04-01 | TEST_1    | 10               | Kartik Wolfram    | Pramod Venkat               |
      | TEST_4          | 2023-04-01 | TEST_2    | 100              | Hisako Dobrilo    | Felipa Zoè                  |
      | TEST_4          | 2023-04-01 | TEST_3    | 100              | Selena Komang     | Selena Komang               |
      | TEST_4          | 2023-04-01 | TEST_4    | 200              | Brigit Kara       | Managold Celestine          |

  Scénario: Feature - Taiga#922 - Il est possible de charger uniquement un CSV des entités
    Soit Les correspondances d'équipements suivantes
      | modeleEquipementSource | refEquipementCible |
      | Serveur1               | serveurA           |
      | Serveur2               | serveurA           |
      | Serveur3               | serveurA           |
    Quand pour le lot "TEST_4-01-04-2023", la date "01-04-2023" et l'organisation "TEST_4" les fichiers dataCenters: "nonExistant.csv", equipementPhysique: "nonExistant.csv", equipementVirtuel: "nonExistant.csv", applications: "nonExistant.csv", messagerie: "nonExistant.csv", entités: "csv/sprint11-v2/entites.csv" sont importés
    Alors La réponse de l'API est 200
    Et Les entités suivantes sont disponibles en base
      | nomOrganisation | dateLot    | nomEntite | nbCollaborateurs | responsableEntite | responsableNumeriqueDurable |
      | TEST_4          | 2023-04-01 | TEST_1    | 10               | Kartik Wolfram    | Pramod Venkat               |
      | TEST_4          | 2023-04-01 | TEST_2    | 100              | Hisako Dobrilo    | Felipa Zoè                  |
      | TEST_4          | 2023-04-01 | TEST_3    | 100              | Selena Komang     | Selena Komang               |
      | TEST_4          | 2023-04-01 | TEST_4    | 200              | Brigit Kara       | Managold Celestine          |

