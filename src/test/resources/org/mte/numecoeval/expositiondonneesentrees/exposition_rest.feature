#language: fr
#encoding: utf-8
Fonctionnalité: Exposition d'API pour insérer des données au format NumEcoEval
  Le système NumEcoEval accepte des données aux formats JSON et CSV.

  Scénario: 1 équipement est correctement envoyée par CSV
    Quand pour la date "01-09-2022" et l'organisation "CAF1_SPRINT_1", le fichier CSV "csv/equipementPhysique.csv" est envoyé
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 1 objets equipements
    Et Il existe "1" "APPLE IPHONE 11" acheté le "2020-09-01", utilisé par "Thierry" en "France" avec une consommation de data annuelle de "4" Go sur le réseau

  Scénario: Multiples équipements sont correctement envoyés par CSV
    Quand pour la date "02-09-2022" et l'organisation "CAF2_SPRINT_1", le fichier CSV "csv/equipementPhysique_caf2.csv" est envoyé
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 2 objets equipements
    Et Il existe "1" "Odyssey Neo G9 49" acheté le "2020-09-01", utilisé par "Xavier" en "France" avec une consommation de data annuelle de "0" Go sur le réseau
    Et Il existe "100" "Dell 20 E2020H" acheté le "2022-09-01", utilisé par "SSG Dublin" en "Irlande" avec une consommation de data annuelle de "0" Go sur le réseau

  Scénario: Fichier Vide avec uniquement l'entête envoyé résulte en une erreur 200
    Quand pour la date "03-09-2022" et l'organisation "TEST_VIDE_SPRINT_1", le fichier CSV "csv/equipementPhysique_vide.csv" est envoyé
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 0 objets equipements

  Scénario: Fichier sans contenu envoyé résulte en une erreur 400
    Quand pour la date "04-09-2022" et l'organisation "TEST_VIDE_SPRINT_1", le fichier CSV "nonExistant.csv" est envoyé
    Alors La réponse de l'API est 400
    Et Les données chargées contiennent 0 objets equipements
