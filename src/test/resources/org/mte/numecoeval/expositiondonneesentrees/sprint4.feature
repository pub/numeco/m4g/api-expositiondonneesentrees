#language: fr
#encoding: utf-8
Fonctionnalité: Sprint 4 - Ajout des équipements virtuels et des applications.
  Le système NumEcoEval accepte des données aux formats CSV (import de Data Centers, d'équipements physiques, d'équipements virtuels et d'applications).

  Scénario: CAF1 - Taiga #55/Taiga #57 - Import des 4 fichiers avec toutes les données cohérentes
    Quand pour la date "15-10-2022" et l'organisation "TAIGA55_TAIGA57_SPRINT4" les fichiers dataCenters: "csv/sprint4/dataCenters_caf1.csv", equipementPhysique: "csv/sprint4/equipementPhysique_caf1.csv", equipementVirtuel: "csv/sprint4/equipementVirtuel_caf1.csv", applications: "csv/sprint4/application_caf1.csv" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 1 objets equipements
    Et Les données chargées contiennent 1 objets data centers
    Et Les données chargées contiennent 1 objets équipements virtuels
    Et Les données chargées contiennent 6 objets application
    Et Il existe un data center "DC_Villeperdue_01" (nom complet: "DC_Villeperdue_OpenStack_01") avec un PUE de "1.0" localisé en "France"
    Et Il existe "1" "Serveur1 Xeon" acheté le "2020-09-01", utilisé par "SSG" en "France" avec une consommation de data annuelle de "118" Go sur le réseau
    Et Il existe un équipement virtuel "VM1" associé au serveur "Serveur1 Xeon" avec un 5 vCPU et rattaché au cluster "Cluster1"
    Et Il existe une application "M4G-api-referentiel" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-indicateurs" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-calcul" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-donneesEntrees" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-exposition" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-2" et "java"
    Et Il existe une application "M4G-superset" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-3" et "java"

  Scénario: CAT1 - Taiga #55/Taiga #57 - Plusieurs VMs et plusieurs applications
    Quand pour la date "15-10-2022" et l'organisation "TAIGA55_TAIGA57_PART2_SPRINT4" les fichiers dataCenters: "csv/sprint4/dataCenters_cat1.csv", equipementPhysique: "csv/sprint4/equipementPhysique_cat1.csv", equipementVirtuel: "csv/sprint4/equipementVirtuel_cat1.csv", applications: "csv/sprint4/application_cat1.csv" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 3 objets equipements
    Et Les données chargées contiennent 1 objets data centers
    Et Les données chargées contiennent 3 objets équipements virtuels
    Et Les données chargées contiennent 12 objets application
    Et Il existe un data center "DC_Villeperdue_01" (nom complet: "DC_Villeperdue_OpenStack_01") avec un PUE de "1.0" localisé en "France"
    Et Il existe "1" "Serveur1 Xeon" acheté le "2020-09-01", utilisé par "SSG" en "France" avec une consommation de data annuelle de "118" Go sur le réseau
    Et Il existe "1" "Serveur2 Xeon" acheté le "2019-09-01", utilisé par "SSG" en "France" avec une consommation de data annuelle de "64" Go sur le réseau
    Et Il existe "1" "Serveur3 Xeon" acheté le "2019-09-01", utilisé par "SSG" en "France" avec une consommation de data annuelle de "20" Go sur le réseau
    Et Il existe un équipement virtuel "VM1" associé au serveur "Serveur1 Xeon" avec un 5 vCPU et rattaché au cluster "Cluster1"
    Et Il existe un équipement virtuel "VM2" associé au serveur "Serveur2 Xeon" avec un 4 vCPU et rattaché au cluster "Cluster2"
    Et Il existe un équipement virtuel "VM3" associé au serveur "Serveur3 Xeon" avec un 2 vCPU et rattaché au cluster "Cluster2"
    Et Il existe une application "M4G-api-referentiel" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-indicateurs" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-calcul" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-donneesEntrees" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-exposition" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-2" et "java"
    Et Il existe une application "M4G-superset" pour le type d'environnement "Prod" associé à la VM "VM1" de l'équipement "Serveur1 Xeon" et avec pour domaines applicatifs "datahub-m4g-3" et "java"
    Et Il existe une application "M4G-api-referentiel" pour le type d'environnement "Dev" associé à la VM "VM2" de l'équipement "Serveur2 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-indicateurs" pour le type d'environnement "Dev" associé à la VM "VM2" de l'équipement "Serveur2 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-calcul" pour le type d'environnement "Dev" associé à la VM "VM2" de l'équipement "Serveur2 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-donneesEntrees" pour le type d'environnement "Dev" associé à la VM "VM2" de l'équipement "Serveur2 Xeon" et avec pour domaines applicatifs "datahub-m4g-1" et "java"
    Et Il existe une application "M4G-api-exposition" pour le type d'environnement "Dev" associé à la VM "VM2" de l'équipement "Serveur2 Xeon" et avec pour domaines applicatifs "datahub-m4g-2" et "java"
    Et Il existe une application "M4G-superset" pour le type d'environnement "Dev" associé à la VM "VM3" de l'équipement "Serveur3 Xeon" et avec pour domaines applicatifs "datahub-m4g-3" et "java"


  Scénario: Cas Technique - Import de 3 fichiers uniquement sans les applications
    Quand pour la date "16-10-2022" et l'organisation "TECH1_SPRINT4" les fichiers dataCenters: "csv/sprint4/dataCenters_caf1.csv", equipementPhysique: "csv/sprint4/equipementPhysique_caf1.csv", equipementVirtuel: "csv/sprint4/equipementVirtuel_caf1.csv", applications: "nonExistants" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 1 objets equipements
    Et Les données chargées contiennent 1 objets data centers
    Et Les données chargées contiennent 1 objets équipements virtuels
    Et Il existe un data center "DC_Villeperdue_01" (nom complet: "DC_Villeperdue_OpenStack_01") avec un PUE de "1.0" localisé en "France"
    Et Il existe "1" "Serveur1 Xeon" acheté le "2020-09-01", utilisé par "SSG" en "France" avec une consommation de data annuelle de "118" Go sur le réseau
    Et Il existe un équipement virtuel "VM1" associé au serveur "Serveur1 Xeon" avec un 5 vCPU et rattaché au cluster "Cluster1"


  Scénario: Cas Technique - Import d'un mauvais fichier de data center
    Quand pour la date "16-10-2022" et l'organisation "TECH2_SPRINT4" les fichiers dataCenters: "csv/sprint4/mauvaisFichier.csv", equipementPhysique: "csv/sprint4/equipementPhysique_caf1.csv", equipementVirtuel: "nonExistants", applications: "nonExistants" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 1 objets equipements
    Et Les données chargées contiennent 0 objets data centers
    Et Il existe "1" "Serveur1 Xeon" acheté le "2020-09-01", utilisé par "SSG" en "France" avec une consommation de data annuelle de "118" Go sur le réseau


  Scénario: Cas Technique - Import d'un mauvais fichier d'équipement virtuels
    Quand pour la date "16-10-2022" et l'organisation "TECH3_SPRINT4" les fichiers dataCenters: "csv/sprint4/dataCenters_caf1.csv", equipementPhysique: "csv/sprint4/equipementPhysique_caf1.csv", equipementVirtuel: "csv/sprint4/mauvaisFichier.csv", applications: "nonExistants" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 1 objets equipements
    Et Les données chargées contiennent 1 objets data centers
    Et Les données chargées contiennent 0 objets équipements virtuels
    Et Il existe un data center "DC_Villeperdue_01" (nom complet: "DC_Villeperdue_OpenStack_01") avec un PUE de "1.0" localisé en "France"
    Et Il existe "1" "Serveur1 Xeon" acheté le "2020-09-01", utilisé par "SSG" en "France" avec une consommation de data annuelle de "118" Go sur le réseau


  Scénario: Cas Technique - Import d'un mauvais fichier d'applications
    Quand pour la date "16-10-2022" et l'organisation "TECH4_SPRINT4" les fichiers dataCenters: "csv/sprint4/dataCenters_caf1.csv", equipementPhysique: "csv/sprint4/equipementPhysique_caf1.csv", equipementVirtuel: "csv/sprint4/equipementVirtuel_caf1.csv", applications: "csv/sprint4/mauvaisFichier.csv" sont importés
    Alors La réponse de l'API est 200
    Et Les données chargées contiennent 1 objets equipements
    Et Les données chargées contiennent 1 objets data centers
    Et Les données chargées contiennent 1 objets équipements virtuels
    Et Les données chargées contiennent 0 objets application
    Et Il existe un data center "DC_Villeperdue_01" (nom complet: "DC_Villeperdue_OpenStack_01") avec un PUE de "1.0" localisé en "France"
    Et Il existe "1" "Serveur1 Xeon" acheté le "2020-09-01", utilisé par "SSG" en "France" avec une consommation de data annuelle de "118" Go sur le réseau
    Et Il existe un équipement virtuel "VM1" associé au serveur "Serveur1 Xeon" avec un 5 vCPU et rattaché au cluster "Cluster1"
