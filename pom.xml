<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.mte.numecoeval</groupId>
		<artifactId>core</artifactId>
		<version>1.0.0</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<artifactId>api-expositionDonneesEntrees</artifactId>
	<version>1.0.0</version>
	<name>api-expositionDonneesEntrees</name>
	<description>API Exposition des données d'entrées - Exposition par API pour injecter des données d'entrées</description>

	<repositories>
		<repository>
			<id>gitlab-maven</id>
			<url>https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/groups/5389/-/packages/maven</url>
		</repository>
	</repositories>
	<distributionManagement>
		<repository>
			<id>gitlab-maven</id>
			<url>${env.CI_SERVER_URL}/api/v4/projects/${env.CI_PROJECT_ID}/packages/maven</url>
		</repository>
		<snapshotRepository>
			<id>gitlab-maven</id>
			<url>${env.CI_SERVER_URL}/api/v4/projects/${env.CI_PROJECT_ID}/packages/maven</url>
		</snapshotRepository>
	</distributionManagement>

	<properties>
	</properties>
	<dependencies>
		<dependency>
			<groupId>org.mte.numecoeval</groupId>
			<artifactId>common</artifactId>
			<version>1.0.0</version>
		</dependency>

		<dependency>
			<groupId>org.mte.numecoeval</groupId>
			<artifactId>rest-dto-referentiel</artifactId>
			<version>1.0.0</version>
		</dependency>

		<!-- Monitoring -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>

		<!-- REST -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-cache</artifactId>
		</dependency>

		<!-- JPA -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>

		<!-- Driver pour base de données -->
		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<scope>runtime</scope>
		</dependency>

		<!-- Nécessaire avec Spring Boot 3 pour Hibernate -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-validation</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springdoc</groupId>
			<artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
		</dependency>

		<!-- Mapping -->
		<dependency>
			<groupId>org.mapstruct</groupId>
			<artifactId>mapstruct</artifactId>
		</dependency>

		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-csv</artifactId>
		</dependency>

		<!-- Utilitaire -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-collections4</artifactId>
		</dependency>
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
		</dependency>

		<!-- Monitoring -->
		<dependency>
			<groupId>io.micrometer</groupId>
			<artifactId>micrometer-registry-prometheus</artifactId>
			<scope>runtime</scope>
		</dependency>

		<!-- Lombok -->
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<optional>true</optional>
		</dependency>

		<!-- Code généré-->
		<dependency>
			<groupId>org.openapitools</groupId>
			<artifactId>jackson-databind-nullable</artifactId>
		</dependency>

		<!-- TU/TI -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.junit.platform</groupId>
			<artifactId>junit-platform-suite</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.junit.platform</groupId>
			<artifactId>junit-platform-engine</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.meanbean</groupId>
			<artifactId>meanbean</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>nl.jqno.equalsverifier</groupId>
			<artifactId>equalsverifier</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- Tests d'API REST -->
		<dependency>
			<groupId>io.rest-assured</groupId>
			<artifactId>rest-assured</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>io.rest-assured</groupId>
			<artifactId>rest-assured-all</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- Tests avec attente minimum -->
		<dependency>
			<groupId>org.awaitility</groupId>
			<artifactId>awaitility</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- Base de données de tests -->
		<dependency>
			<groupId>io.zonky.test</groupId>
			<artifactId>embedded-database-spring-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>io.zonky.test</groupId>
			<artifactId>embedded-postgres</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- Données de tests -->
		<dependency>
			<groupId>org.instancio</groupId>
			<artifactId>instancio-junit</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- Tests fonctionnels -->
		<dependency>
			<groupId>io.cucumber</groupId>
			<artifactId>cucumber-core</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>io.cucumber</groupId>
			<artifactId>cucumber-java</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>io.cucumber</groupId>
			<artifactId>cucumber-spring</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>io.cucumber</groupId>
			<artifactId>cucumber-junit-platform-engine</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>com.github.tomakehurst</groupId>
			<artifactId>wiremock-jre8-standalone</artifactId>
			<version>2.33.2</version>
			<scope>test</scope>
		</dependency>
    </dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<excludes>
						<exclude>
							<groupId>org.projectlombok</groupId>
							<artifactId>lombok</artifactId>
						</exclude>
					</excludes>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.openapitools</groupId>
				<artifactId>openapi-generator-maven-plugin</artifactId>
				<version>${openapi-generator-version}</version>
				<executions>
					<execution>
						<id>generation_serveur</id>
						<goals>
							<goal>generate</goal>
						</goals>
						<configuration>
							<inputSpec>${project.basedir}/src/main/resources/static/openapi.yaml</inputSpec>
							<output>${project.build.directory}/generated-sources</output>
							<modelPackage>org.mte.numecoeval.expositiondonneesentrees.generated.api.model</modelPackage>
							<apiPackage>org.mte.numecoeval.expositiondonneesentrees.generated.api.server</apiPackage>
							<invokerPackage>org.mte.numecoeval.expositiondonneesentrees.generated.api.invoker</invokerPackage>
							<generateApis>true</generateApis>
							<generateModels>true</generateModels>
							<generateModelTests>false</generateModelTests>
							<generateApiTests>false</generateApiTests>
							<generateSupportingFiles>false</generateSupportingFiles>
							<generateModelDocumentation>false</generateModelDocumentation>
							<generateApiDocumentation>false</generateApiDocumentation>
							<generatorName>spring</generatorName>
							<library>spring-boot</library>
							<configOptions>
								<useTags>true</useTags>
								<skipDefaultInterface>true</skipDefaultInterface>
								<dateLibrary>java8</dateLibrary>
								<useSpringBoot3>true</useSpringBoot3>
								<sourceFolder>src/gen/java</sourceFolder>
								<serializableModel>true</serializableModel>
								<interfaceOnly>true</interfaceOnly>
								<reactive>false</reactive>
								<useBeanValidation>true</useBeanValidation>
								<performBeanValidation>true</performBeanValidation>
								<useOptional>false</useOptional>
								<serviceInterface>true</serviceInterface>
								<serviceImplementation>false</serviceImplementation>
								<booleanGetterPrefix>is</booleanGetterPrefix>
								<additionalModelTypeAnnotations>@lombok.AllArgsConstructor;@lombok.Builder;@lombok.NoArgsConstructor</additionalModelTypeAnnotations>
							</configOptions>
						</configuration>
					</execution>
				</executions>

			</plugin>
		</plugins>
	</build>

</project>
